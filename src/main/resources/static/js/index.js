
// 主页集成模块
layui.use(['mobai'], function () {
    const $ = layui.jquery, element = layui.element, mobai = layui.mobai;
    // 读取配置信息
    const setter = mobai.setter;
    // 已打开的页面列表，用于tab记忆
    setter.tabList = [];
    // 最后显示的页面，用于恢复tab后进行定位
    setter.tabPosition = undefined;
    // 默认菜单字段名
    const mp = setter.menuProp;

    const index = {
        /**
         * 初始化参数
         * @param initUrl 初始化数据路径
         */
        init: function (initUrl) {
            mobai.cacheInit();
            // 获取缓存的tab
            setter.tabList = mobai.getTempData('indexTabs') || [];
            setter.tabPosition = mobai.getTempData('tabPosition');
            mobai.req({
                url: initUrl,type: 'post',
                success: function (res) {
                    // 菜单渲染
                    if (res.data.menuList && res.data.menuList.length>0) {
                        res.data.menuList && index.menuRender(res.data.menuList);
                    }

                    // 页面初始化
                    setter.homeUrl = res.data.homePath || setter.homeUrl;

                    // 是否开启缓存，并有缓存页面
                    const recover = (setter.cacheTab && setter.tabList.length > 0);

                    // 多标签或者没有定位页面
                    if (setter.pageTabs || !setter.tabPosition){
                        const noChange = setter.tabPosition && recover;
                        index.loadView(setter.homeUrl,'<i class="layui-icon layui-icon-home"></i>',noChange);
                    }
                    // 恢复缓存tab
                    if (recover) {
                        layui.each(setter.tabList, function (i, item) {
                            index.loadView(item.menuPath,item.menuName,(item.menuPath !== setter.tabPosition));
                        });
                    }
                }
            })
        },
        // 菜单初始化
        menuRender: function (menus) {
            setter.pageTabs ? index.renderMultiModule(menus) : index.renderSingleModule(menus);
        },
        // 多模块
        renderMultiModule: function (menus) {
            let PCHeaderMenuHtml = [],MBHeaderMenuHtml = [],leftMenuHtml=[];
            layui.each(menus, function (i, item) {
                const menuKey = 'multi_module_' + item[mp.menuId];
                PCHeaderMenuHtml.push(index.renderMenu({key: menuKey,title: item[mp.title],icon: item[mp.icon],target: item[mp.target], type: 'top',href: item[mp.url],class: (i === 0 ? 'layui-this' : '')},false));
                MBHeaderMenuHtml.push(index.renderMenu({key: menuKey,title: item[mp.title],icon: item[mp.icon],target: item[mp.target], type: 'top',href: item[mp.url],class: (i === 0 ? 'layui-this' : '')},true));
                leftMenuHtml.push(index.renderLeftMenu(item[mp.children], {key: menuKey, class: (i === 0 ? 'layui-this' : 'layui-hide')}));
            })
            $('.mobai-header-menu-pc').append(PCHeaderMenuHtml.join('')); //电脑顶部菜单
            $('.mobai-header-menu-mobile .layui-nav-child').html(MBHeaderMenuHtml.join('')); //手机顶部菜单
            $('.mobai-menu-left').html(leftMenuHtml.join(''));//左侧菜单
            element.init();
        },
        // 单模块
        renderSingleModule: function (menus) {
            const leftMenuHtml = index.renderLeftMenu(menus);
            $('.mobai-header [menu-key]').remove();
            $('.mobai-menu-left').html(leftMenuHtml);
            element.init();
        },
        /**
         * 渲染左侧菜单
         * @param menus:    左侧菜单组
         * @param opt:      其他参数
         * @param opt.key:  父级模块key
         * @param opt.class: 默认显示还是隐藏的className
         */
        renderLeftMenu: function (menus, opt) {
            opt = opt || {};
            let leftMenusHtml = [];
            layui.each(menus, function (i, item) {
                const childrenHtml = index.renderChildrenMenu(item[mp.children]);
                leftMenusHtml.push(index.renderMenu({href: item[mp.url], target: item[mp.target], class: "", icon: item[mp.icon], title: item[mp.title], childrenHtml: childrenHtml},false));
            })
            leftMenusHtml = leftMenusHtml.join("");
            leftMenusHtml = index.compileMenuContainer({key: opt.key, class: opt.class, childrenHtml: leftMenusHtml},false);
            return leftMenusHtml;
        },
        // 获取子菜单
        renderChildrenMenu: function (menus) {
            let html = [];
            layui.each(menus, function (i, item) {
                if (item[mp.children] && item[mp.children].length) {
                    item.childrenHtml = index.renderChildrenMenu(item.children, true);
                }
                html.push(index.renderMenu({href: item[mp.url], target: item[mp.target],type: 'left', icon: item[mp.icon], title: item[mp.title], children: item[mp.children], class: '', childrenHtml: item.childrenHtml}, true));
            });
            html = html.join("");
            return index.compileMenuContainer({class: '',childrenHtml: html}, true);
        },
        /**
         * 菜单生成
         * @param menu              菜单对象
         * @param menu.key          菜单标识，用于切换菜单
         * @param menu.class        菜单样式
         * @param menu.href         菜单访问路径
         * @param menu.target
         * @param menu.icon         菜单图标
         * @param menu.title        菜单标题
         * @param menu.type         菜单类型：top顶部菜单 OR left左侧菜单，用于定义样式
         * @param menu.childrenHtml 子菜单html字符串
         * @param isSub             是否底级菜单
         */
        renderMenu: function (menu, isSub) {
            const menuKey = menu.key ? ('menu-key="' + menu.key + '"') : '';
            return [
                '<li ',isSub ? '' : 'class="layui-nav-item" ',menuKey,'>',
                '<a href="javascript:;" ', menu.href ? ('index-href="' + menu.href + '" ') : '', menu.target ? ('target="' + menu.target + '" ') : 'target="_self" ','>',
                // 图标
                (menu.icon ? '<i class="layui-icon left-icon ' + menu.icon + '"></i>' : ''),
                '<cite>', menu.title, '</cite>',
                '</a>',
                (menu.childrenHtml || ''),
                '</li>'
            ].join('');
        },
        /**
         * 菜单的联接
         * @param menu  菜单信息
         * @param menu.key          key标识（顶部菜单相关）
         * @param menu.class        菜单样式
         * @param menu.childrenHtml 菜单子字符串
         * @param isSub             是否是子菜单
         */
        compileMenuContainer: function (menu, isSub) {
            if (!menu.childrenHtml) return "";
            return [
                '<ul class="',isSub ? 'layui-nav-child' : ('layui-nav layui-nav-tree '+ menu.class ),'" ',menu.key ? (' nav-id="' + menu.key + '"') : '','>',
                menu.childrenHtml,
                '</ul>'
            ].join('');
        },
        /**
         * 载入新的页面
         * @param path 页面路径
         * @param name 页面名称
         * @param noChange 不切换页面（用于恢复页面，但是不打开）
         */
        loadView: function (path,name,noChange) {
            if (!path) return mobai.error('页面失败，请刷新后重试');
            const iframeHtml = '<iframe class="admin-iframe" lay-id="' + path + '" src="' + path + '" frameborder="0"></iframe>';
            if (setter.pageTabs) {  // 多标签模式
                // 选项卡是否已添加
                const tabList = $('.layui-layout-admin .mobai-body .mobai-tab-title>li');
                let flag;
                tabList.each(function () {
                    if ($(this).attr('lay-id') === path) flag = true;
                });
                if (!flag) {  // 添加选项卡
                    if (tabList.length >= setter.maxTabNum) return layer.msg('Tab窗口已达到限定数量，请先关闭部分Tab');
                    element.tabAdd('mobai-tab', {
                        id: path,
                        title: '<span class="mobai-tab-active"></span><span>' + (name || '') + (path === setter.homeUrl ? '':'</span><i class="layui-icon layui-unselect layui-tab-close">ဆ</i>'),
                        content: iframeHtml
                    });
                    if (path !== setter.homeUrl) {
                        let isHas = false; // 是否已经记录
                        for (let i = 0; i < setter.tabList.length; i++) {
                            if (setter.tabList[i].menuPath === path) {
                                isHas = true;
                                break;
                            }
                        }
                        if (!isHas) setter.tabList.push({menuPath:path,menuName:name});  // 未记录，才记录tab
                    }
                }
                if (!noChange){
                    element.tabChange('mobai-tab',path);
                    index.topMenuChange();
                }
            } else {
                const $contentDom = $('.layui-layout-admin .mobai-body .admin-iframe');
                if ($contentDom.length === 0) {//页面是否为空
                    $('.layui-layout-admin .mobai-body').html([
                        '<div class="layui-body-header">',
                        '   <span class="layui-body-header-title"></span>',
                        '   <span class="layui-breadcrumb pull-right" lay-filter="admin-body-breadcrumb" style="visibility: visible;"></span>',
                        '</div>',
                        '<div style="-webkit-overflow-scrolling: touch;">',
                        iframeHtml,
                        '</div>'
                    ].join(''));
                } else { // 已有页面打开
                    $contentDom.attr('lay-id', path).attr('src', path);
                }
                $('[lay-filter="admin-body-breadcrumb"]').html(index.getBreadcrumbHtml(path));
                setter.tabList = []// 清空缓存的页面列表
                if (path === setter.homeUrl) {
                    setter.tabPosition = undefined;
                    index.setTabTitle($(name).text() || $(menuLeft + ' [lay-href="' + setter.homeUrl + '"]').text() || '主页');
                } else {
                    setter.tabPosition = path;
                    setter.tabList.push({menuPath:path,menuName:name});
                    index.setTabTitle(name);
                }
            }
            if (setter.cacheTab) {
                index.setTabCache(true);
            }
            //if (mobai.util.pageWidth <= 768) index.flexible(); // 移动端自动收起侧导航
        },
        /** 设置tab标题 */
        setTabTitle: function (title, tabId) {
            const $header = $('.layui-layout-admin .mobai-header');
            const $bodyHeader = $('.layui-layout-admin .mobai-body>.layui-body-header');
            if (setter.pageTabs) {
                if (!tabId) tabId = $('.layui-layout-admin>.layui-body>.layui-tab>.layui-tab-title>li.layui-this').attr('lay-id');
                if (tabId) $('.layui-layout-admin>.layui-body>.layui-tab>.layui-tab-title>li[lay-id="' + tabId + '"] .title').html(title || '');
            } else if (title) {
                $('.layui-layout-admin .mobai-body .mobai-tab-title').html(title);
                $bodyHeader.addClass('show');
                $header.css('box-shadow', '0 1px 0 0 rgba(0, 0, 0, .03)');
            } else {
                $bodyHeader.removeClass('show');
                $header.css('box-shadow', '');
            }
        },
        /** 获取面包屑结构 */
        getBreadcrumbHtml: function (tabId) {
            const breadcrumb = [];
            let $href = $('.mobai-menu-left').find('[lay-href="' + tabId + '"]');
            if ($href.length > 0) breadcrumb.push($href.text().replace(/(^\s*)|(\s*$)/g, ''));
            while (true) {
                $href = $href.parent('li').parent('ul').prev('a');
                if ($href.length === 0) break;
                breadcrumb.unshift($href.text().replace(/(^\s*)|(\s*$)/g, ''));
            }
            let htmlStr = (tabId === setter.homeUrl) ? '' : ('<a index-href="' + setter.homeUrl + '">首页</a>');
            for (let i = 0; i < breadcrumb.length - 1; i++) {
                if (htmlStr) htmlStr += '<span lay-separator="">/</span>';
                htmlStr += ('<a><cite>' + breadcrumb[i] + '</cite></a>');
            }
            return htmlStr;
        },
        /** 刷新当前选项卡 */
        refresh: function () {
            if (setter.pageTabs) {
                $(".layui-tab-item.layui-show").find("iframe")[0].contentWindow.location.reload();
            } else {
                $(".layui-body").find("iframe")[0].contentWindow.location.reload();
            }
        },
        /**
         * 设置侧栏折叠和展开
         * @param isShow 是否展开
         */
        flexible: function () {
            const $layout = $('.layui-layout-body');
            // 判断主体是否包含折叠样式 admin-mini
            if ($layout.hasClass('index-mini')){
                $layout.removeClass('index-mini')
            }else if ($layout.hasClass('index-mini-m')){
                $layout.removeClass('index-mini-m')
            } else {
                $layout.addClass('index-mini');
            }
            element.init();
        },
        /** 滑动选项卡 */
        rollPage: function (d) {
            const $tabTitle = $('.layui-layout-admin .mobai-body .mobai-tab-title');
            const left = $tabTitle.scrollLeft();
            if ('left' === d) {
                $tabTitle.animate({'scrollLeft': left - 120}, 100);
            } else if ('auto' === d) {
                let autoLeft = 0;
                $tabTitle.children("li").each(function () {
                    if ($(this).hasClass('layui-this')) return false;
                    else autoLeft += $(this).outerWidth();
                });
                $tabTitle.animate({'scrollLeft': autoLeft - 120}, 100);
            } else {
                $tabTitle.animate({'scrollLeft': left + 120}, 100);
            }
        },
        /** 记忆Tab，true 记忆，否则清空 */
        setTabCache: function (isCache) {
            mobai.setTempData('indexTabs', isCache ? setter.tabList : null);
            mobai.setTempData('tabPosition', isCache ? setter.tabPosition : null);
        },
        /** 关闭选项卡操作菜单 */
        closeTabOperatorNav: function () {
            $('.layui-icon-down .layui-nav .layui-nav-child').removeClass('layui-show');
            index.setTabCache(true);
        },
        /** 关闭当前选项卡 */
        closeThisTab: function (url) {
            const $title = $('.layui-layout-admin .mobai-body .mobai-tab-title');
            if (!url) {
                url = $title.find('li.layui-this').attr('lay-id');
                $title.find('li.layui-this').find('.layui-tab-close').trigger('click');
            } else {
                $title.find('li[lay-id="' + url + '"]').find('.layui-tab-close').trigger('click');
            }
            for (let i = 0; i < setter.tabList.length; i++) {
                if (setter.tabList[i].menuPath === url) {
                    setter.tabList.splice(i, 1);
                    break;
                }
            }
            setter.tabPosition = $title.find('li.layui-this').attr('lay-id');
            index.closeTabOperatorNav();
        },
        /** 关闭其他选项卡 */
        closeOtherTab: function (url) {
            if (!url) {
                $('.layui-layout-admin .mobai-body .mobai-tab-title li:gt(0):not(.layui-this)').find('.layui-tab-close').trigger('click');
                url = $('.layui-layout-admin .mobai-body .mobai-tab-title .layui-this').attr('lay-id');
            } else {
                $('.layui-layout-admin .mobai-body .mobai-tab-title li:gt(0)').each(function () {
                    if (url !== $(this).attr('lay-id')) $(this).find('.layui-tab-close').trigger('click');
                });
            }
            for (let i = 0; i < setter.tabList.length; i++) {
                if (setter.tabList[i].menuPath === url) {
                    const menu = setter.tabList[i];
                    setter.tabList = [];
                    setter.tabList.push(menu)
                    break;
                }
            }
            setter.tabPosition = url;
            index.closeTabOperatorNav();
        },
        /** 关闭所有选项卡 */
        closeAllTab: function () {
            $('.mobai-tab-title li:gt(0)').find('.layui-tab-close').trigger('click');
            $('.mobai-tab-title li:eq(0)').trigger('click');
            setter.tabList = [];
            setter.tabPosition = undefined;
            index.closeTabOperatorNav();
        },
        /**
         * 关闭tab右键菜单
         */
        closeTabRightMenu: function () {
            $('.layui-tab-mousedown').remove();
            $('.layui-tab-make').remove();
        },
        /**
         * 开启tab右键菜单
         * @param tabId
         * @param left
         */
        openTabRightMenu: function (tabId, left) {
            index.closeTabRightMenu();
            const menuHtml = [
                '<div class="layui-unselect layui-form-select layui-form-selected layui-tab-mousedown layui-show" data-tab-id="',tabId,'" style="position:absolute;top:0px;width:80px;left:',left,'px">',
                '<dl>',
                '<dd tab-menu-close="this" ><a href="javascript:;" >关 闭 当 前</a></dd>',
                '<dd tab-menu-close="other"><a href="javascript:;" >关 闭 其 他</a></dd>',
                '<dd tab-menu-close="all"  ><a href="javascript:;" >关 闭 全 部</a></dd>',
                '</dl>',
                '</div>'
            ];
            const makeHtml = '<div class="layui-tab-make"></div>';
            $('.layui-tab .layui-tab-title').after(menuHtml.join(''));
            $('.layui-tab .layui-tab-content').after(makeHtml);
        },
        /**
         * 右侧窗口弹出弹出
         * @param param
         * @returns {*}
         */
        popupRight: function (param) {
            param.name = param.id;
            param.anim = -1;
            param.offset = 'r';
            param.move = false;
            param.fixed = true;
            if (param.area === undefined) param.area = '336px';
            if (param.title === undefined) param.title = false;
            if (param.closeBtn === undefined) param.closeBtn = false;
            if (param.shadeClose === undefined) param.shadeClose = true;
            if (param.skin === undefined) param.skin = 'layui-anim layui-anim-rl layui-layer-adminRight';
            return mobai.view(param);
        },
        /** 顶部模块切换 */
        topMenuChange: function (key) {
            $(".mobai-header-menu-pc>li[menu-key]").removeClass('layui-this');
            $('.mobai-menu-left>.layui-nav[nav-id]').removeClass('layui-this').addClass('layui-hide');
            if (key && key != setter.homeUrl){
                $(".mobai-header-menu-pc>li[menu-key='"+ key +"']").addClass('layui-this');
                $("[nav-id='" + key + "']").removeClass('layui-hide').addClass('layui-this');
            }else {
                // 有选中才切换
                const $this = $('.mobai-menu-left>ul .layui-this');
                if ($this.length >0){
                    const thisKey = $this.parents('[nav-id]').attr('nav-id');
                    index.topMenuChange(thisKey);
                }else {
                    index.topMenuChange($('.mobai-menu-left>ul[nav-id]:first').attr('nav-id'));
                }
            }
        },
        // index的事件
        events: {
            /* 退出登录 */
            logout: function () {
                mobai.req({
                    url: 'logout', type: 'post',success: function (res) {
                        window.location = 'login';
                    }
                })
            },
            /* 打开修改密码弹窗 */
            pwd: function () {
                mobai.view({
                    name: 'pwdView',
                    title: '修改密码',
                    url: 'pwdView',
                    subBtnName: 'formSubmitBtn'
                })
            },
            // 折叠侧导航
            flexible: function () {
                index.flexible();
            },
            // 刷新主体部分
            refresh: function () {
                index.refresh();
            },
            /* 左滑动tab */
            leftPage: function () {
                index.rollPage('left');
            },
            /* 右滑动tab */
            rightPage: function () {
                index.rollPage();
            },
            /* 关闭当前选项卡 */
            closeThisTab: function () {
                const url = $(this).data('url');
                index.closeThisTab(url);
            },
            /* 关闭其他选项卡 */
            closeOtherTab: function () {
                index.closeOtherTab();
            },
            /* 关闭所有选项卡 */
            closeAllTab: function () {
                index.closeAllTab();
            }
        }
    }

    {
        const $body = $('body');
        // 多标签-模块切换
        $body.on('click', '[menu-key]', function () {
            const loading = layer.load(0, {shade: false, time: 2 * 1000});
            const menuKey = $(this).attr('menu-key');
            // header
            index.topMenuChange(menuKey);
            layer.close(loading);
        });
        // 手机端点开模块
        $body.on('click', '.mobai-header-menu-mobile .layui-nav-child li', function () {
            const loading = layer.load(0, {shade: false, time: 2 * 1000});
            $('.layui-layout-body').addClass('index-mini-m');
            element.init();
            layer.close(loading);
        });
        // 菜单点击,打开新窗口
        $body.on('click', '[lay-href]', function () {
            const loading = layer.load(0, {shade: false, time: 2 * 1000});
            let href = $(this).attr('lay-href'), title = $(this).text(), target = $(this).attr('target');
            if (!href) return;
            $(this).closest(".layui-nav-tree").find(".layui-this").removeClass("layui-this");
            $(this).parent().addClass("layui-this");
            layer.close(loading);
            target === '_blank' ? window.open(href, "_blank") : index.loadView( href,title);
        });
        // 关闭页面按钮点击
        $body.on('click', '.mobai-tab-title .layui-tab-close', function () {
            const loading = layer.load(0, {shade: false, time: 2 * 1000});
            const tabId = $(this).parent().attr('lay-id');
            element.tabDelete('mobai-tab', tabId);
            index.topMenuChange();
            for (let i = 0; i < setter.tabList.length; i++) {
                if (setter.tabList[i].menuPath === tabId) {
                    setter.tabList.splice(i, 1);
                    break;
                }
            }
            setter.tabPosition = $('.mobai-body>.mobai-tab-title li.layui-this').attr('lay-id');
            index.setTabCache(true);
            layer.close(loading);
        });
        /** tab切换监听 */
        element.on('tab(mobai-tab)', function () {
            const layId = $(this).attr('lay-id');
            // 记录当前Tab位置
            if (setter.cacheTab) mobai.setTempData('tabPosition', (layId !== setter.homeUrl ? layId : undefined));
            // 移除左侧选中
            $('.mobai-menu-left>.layui-nav .layui-this').removeClass('layui-this');
            $('.mobai-menu-left>.layui-nav .layui-nav-itemed').removeClass('layui-nav-itemed');
            if ( layId === setter.homeUrl ) return ;
            $('.mobai-menu-left>ul').addClass('layui-hide');
            // 添加左侧选中
            const $a = $(".mobai-menu-left .layui-nav a[index-href='"+layId+"']");
            $a.parent().removeClass('layui-hide').addClass('layui-this');  // 选中当前
            $a.parents('.layui-nav-child').parent().addClass('layui-nav-itemed');  // 选中当前
            index.rollPage('auto');
            // 顶部模块切换
            index.topMenuChange($a.parents('.layui-nav[nav-id]').attr("nav-id"));
        });

        // 侧导航折叠状态下鼠标经过无限悬浮效果
        $(document).on('mouseenter', '.index-mini .mobai-menu-left .layui-nav-item,.index-mini .mobai-menu-left .layui-nav-item .layui-nav-child>li', function () {
            const $that = $(this), $navChild = $that.find('>.layui-nav-child');
            if ($navChild.length > 0) {
                $that.addClass('index-nav-hover');
                $navChild.css('left', $that.offset().left + $that.outerWidth());
                let top = $that.offset().top;
                if (top + $navChild.outerHeight() > mobai.util.pageHeight) {
                    top = top - $navChild.outerHeight() + $that.outerHeight();
                    if (top < 60) top = 60;
                    $navChild.addClass('show-top');
                }
                $navChild.css('top', top);
            }else if ($that.hasClass('layui-nav-item')) {
                layer.tips($that.find('cite').text(),this);
                //layer.tips({elem: $that, text: $that.find('cite').text(), direction: 2, offset: '12px'});
            }
        }).on('mouseleave', '.index-mini .mobai-menu-left .layui-nav-item,.index-mini .mobai-menu-left .layui-nav-item .layui-nav-child>li', function () {
            layer.closeAll('tips');
            const $this = $(this);
            $this.removeClass('index-nav-hover');
            const $child = $this.find('>.layui-nav-child');
            $child.removeClass('show-top ew-anim-drop-in');
            $child.css({'left': 'auto', 'top': 'auto'});
        });
    }

    // 右键菜单相关
    // 禁用网页右键
    $(".mobai-tab-title").unbind("mousedown").bind("contextmenu", function (e) {
        e.preventDefault();
        return false;
    });
    // 注册鼠标右键菜单
    $(document).on('mousedown', '.mobai-tab-title li', function (e) {
        const left = $(this).offset().left - $('.layui-tab ').offset().left + ($(this).width() / 2),
            tabId = $(this).attr('lay-id');
        if (e.which === 3) {
            index.openTabRightMenu(tabId, left);
        }
    });
    // 关闭tab右键菜单
    $(document).on('click','.mobai-left,.mobai-right,.layui-tab-make', function () {
        index.closeTabRightMenu();
    });
    // tab右键选项卡操作
    $(document).on('click', '[tab-menu-close]', function () {
        const closeType = $(this).attr('tab-menu-close'),
            tabId = $('.layui-tab-mousedown').attr('data-tab-id');
        if (closeType === 'all') {
            index.closeAllTab();
        }else if (closeType === 'this'){
            index.closeThisTab(tabId);
        }else if (closeType === 'other'){
            index.closeOtherTab(tabId);
        }
        index.closeTabRightMenu();
    });

    // 添加移动设备遮罩层,及点击事件
    if ($('.site-mobile-shade').length === 0){
        $('.layui-layout-admin').append('<div class="site-mobile-shade"></div>');
    }
    $('.site-mobile-shade').on('click',function () {
        index.flexible();
    });

    // 页脚是否显示
    if (setter.closeFooter) {
        $('body').addClass('close-footer');
    }
    // 所有index-href处理
    $(document).on('click', '*[index-href]', function () {
        const $this = $(this);
        const href = $this.attr('index-href');
        if (!href || href === '#') return;
        if (href.indexOf('javascript:') === 0) return new Function(href.substring(11))();
        const title = $this.attr('index-title') || $this.text();
        index.loadView( href, title);
    });

    // 所有index-event
    $(document).on('click', '*[index-event]', function () {
        const te = index.events[$(this).attr('index-event')];
        te && te.call(this, $(this));
    });

    index.init('index/init');
})