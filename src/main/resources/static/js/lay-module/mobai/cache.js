layui.define(['mobaiUtil'], function (exports) {
    const $ = layui.jquery, setter = layui.cache;
    const adminName = setter.adminName;

    const cache = {
        dictProp: setter.dictProp,
        menuProp: setter.menuProp,
        /**
         * 缓存数据
         * @param key 存储标识，用于取出
         * @param value 值：为null则清除对应缓存
         * @param isSession 是否是临时存储,即关闭页面就清除
         */
        setTempData: function (key, value, isSession) {
            if (key === 'loginUser' || key === 'dict' || key === 'dictTree' || key === 'deptTree' || key === 'deptScopeTree' || key === 'deptList') {
                return console.log('系统参数不允许通过此方法保存');
            }
            const opt = value ? {key: key, value: value} : {key: key, remove: true};
            isSession ? layui.sessionData(adminName, opt) : layui.data(adminName, opt);
        },
        /** 获取缓存临时数据
         * @param key 标识
         */
        getTempData: function (key) {
            if (key) {
                const tempData = layui.data(adminName);
                return tempData ? tempData[key] : undefined;
            }
        },
        // 通过页面标识获取页面缓存数据
        getViewData: function (name) {
            return layui.data('view')[name];
        },
        /**
         * 缓存页面数据
         * @param name          页面名称，用于标识是否是同一个页面
         * @param opt           页面绑定参数
         * @param opt.index     layui的弹窗index
         * @param opt.data      绑定弹窗的数据，可以用于其他页面调用
         * @param opt.status    状态，如果是true，那么关闭弹窗
         * @param isCover       是否覆盖数据，默认继承数据, 避免清空数据无法实行
         */
        setViewData: function (name, opt, isCover) {
            // 首次存页面数据 或 覆盖
            let viewData = (isCover || opt.index) ? opt : $.extend({},cache.getViewData(name), opt);
            // 页面完成，关闭页面，由于关闭弹窗回调会需要，所以本次不清空数据
            if (viewData && viewData.status && viewData.status === true) {
                layer.close(viewData.index);
            }
            layui.data('view', {key: name, value: viewData})
        },
        /**
         * 根据字典类型获取字典数据列表
         * @param type 字典类型，必填
         * @param valueHeadStr 值开头,用于过滤
         */
        getDictByType: function (type, valueHeadStr) {
            if (!cache.dict || !cache.dict[type]){
                return [];
            }
            if (valueHeadStr){
                const result = [];
                cache.dict[type].forEach(function (item) {
                    if (item[cache.dictProp.value].indexOf(valueHeadStr) === 0) {
                        result.push(item);
                    }
                });
                return result;
            }
            return cache.dict[type];
        },
        /**
         * 根据字典类型获取字典数据树
         * @param type 字典类型，必填
         */
        getDictTreeByType: function (type) {
            if (!cache.dictTree || !cache.dictTree[type]){
                return [];
            }
            return cache.dictTree[type];
        },
        /**
         * 通过字典值和字典类型获取标签,多标签可以用逗号隔开
         * @param type  字典类型
         * @param value 字典值
         * @param addStyle 是否添加字典表格样式 true
         */
        getDictLabel: function (type, value, addStyle) {
            if (cache.dict && cache.dict[type] && value){
                // 字符串的多标签,转换成数组
                if (typeof value === 'string' && value.indexOf(',') !== -1) {
                    value = value.toString().split(',');
                }
                // 数组模式的值
                if (value instanceof Array) {
                    let result = '';
                    for (let i = 0; i < value.length; i++) {
                        result += cache.getDictLabelByValue(cache.dict[type],value,addStyle);
                    }
                    return result;
                } else {
                    return cache.getDictLabelByValue(cache.dict[type],value,addStyle);
                }
            }
            return '';
        },
        // 通过字典值和字典类型获取标签,如果有父级，那么获取父级标签拼接
        getDictLabelByValue: function (dictList,value,addStyle) {
            for (let j = 0; j < dictList.length; j++) {
                if (value.toString() === dictList[j][cache.dictProp.value].toString()) {
                    if (addStyle) {
                        return ['<span class="badge '+dictList[j]['tableStyleType']+'">',
                            dictList[j][cache.dictProp.parentId].toString() === '0' ? '' : (cache.getDictLabelById(dictList,dictList[j][cache.dictProp.parentId]) + '/'),
                            dictList[j][cache.dictProp.name],
                            '</span>'].join('');
                    }else {
                        return (dictList[j][cache.dictProp.parentId].toString() === '0' ? '' : (cache.getDictLabelById(dictList,dictList[j][cache.dictProp.parentId]) + '/')) + dictList[j][cache.dictProp.name];
                    }
                }
            }
        },
        getDictLabelById: function (dictList,id) {
            if (!id || id.toString() === '0'){
                return '';
            }
            for (let j = 0; j < dictList.length; j++) {
                if (id.toString() === dictList[j][cache.dictProp.dictId].toString()) {
                    return (dictList[j][cache.dictProp.parentId].toString() === '0' ? '' : (cache.getDictLabelById(dictList,dictList[j][cache.dictProp.parentId]) + '/')) + dictList[j][cache.dictProp.name];
                }
            }
            return '';
        },
        // 根据部门id获取缓存的部门名称
        getDeptName: function (id) {
            for (let i = 0; i < cache.dept.length; i++) {
                if (cache.dept[i].deptId == id) {
                    return cache.dept[i].deptName;
                }
            }
            return '';
        },
        /**
         * 获取部门树，默认从缓存中获取全量的部门树
         * @param type      方式：1=缓存的角色权限范围部门
         */
        getDeptTree: function (type) {
            return type === 1 ? cache.deptScopeTree : cache.deptTree;
        }
    };

    cache.dict = cache.getTempData('dict');
    cache.dictTree = cache.getTempData('dictTree');
    cache.dept= cache.getTempData('dept');
    cache.deptTree= cache.getTempData('deptTree');
    cache.deptScopeTree= cache.getTempData('deptScopeTree');
    cache.loginUser= cache.getTempData('loginUser');

    exports('mobaiCache', cache);
})