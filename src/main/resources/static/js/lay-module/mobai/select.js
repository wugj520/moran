layui.define(['xmSelect','pinyin'], function (exports) {
    const $ = layui.jquery, form = layui.form;
    const xmSelect = layui.xmSelect, pinyin = layui.pinyin;

    const select = {
        /**
         * 渲染普通下拉框
         * @param opt 参数集
         * @param opt.name          指定下拉框name (必填)
         * @param opt.filter        过滤,首选过滤id,其次过滤form表单上lay-filter,最后过滤下拉框上的lay-filter
         * @param opt.data          数据
         * @param opt.prop          数据模型，例如默认：{name: 'name', value: 'value'}
         * @param opt.placeholder:  提示词
         */
        rendSelect: function (opt) {
            if (!opt.name) {return;}
            // 对象选中规则
            let elemStr = 'select[name="' + opt.name + '"]';
            if (opt.filter){
                if (document.getElementById(opt.filter)){
                    elemStr = '#' + opt.filter + ' ' + elemStr;
                }else if($('form[lay-filter="'+opt.filter+'"]').length > 0) {
                    elemStr = 'form[lay-filter="' + opt.filter + '"] ' + elemStr;
                }else {
                    elemStr = elemStr + '[lay-filter="' + opt.filter + '"]';
                }
            }
            // 数据模型
            const prop = opt.prop || {name: 'name', value: 'value'};
            // 拼接select
            let html = ('<option value="">' + (opt.placeholder || '请选择') + "</option>");
            opt.data.forEach(function (item) {
                html += ('<option value="' + item[prop.value] + '" ' + (item['selected']==='true'?'selected':'') + (item['disabled']==='true'?'disabled':'') + ">" + item[prop.name] + "</option>")
            });
            $(elemStr).html(html);
            form.render('select');
        },
        /**
         * 渲染xmSelect: <br>
         * 默认单选（隐藏图标）、开启搜索（支持拼音）、打开关闭其他打开的layui下拉框
         * @param opt 参数集
         */
        rendXmSelect: function (opt) {
            if (!opt.el || !opt.name || !opt.data) {
                console.log('缺少必备参数，怎么渲染xm下拉框？？');
                return;
            }
            const options = {
                height: '250px',
                radio: true,//是否开启单选模式
                clickClose: (opt.radio !== false),//是否点击选项后自动关闭下拉框,多选默认不自动关闭
                filterable: true,//是否开启搜索
                delay: 1000,//搜索延迟
                filterMethod: function (val, item, index, prop) {//搜索方法
                    const py = "" + pinyin.makePy(item[prop.name]);
                    return py.indexOf(val) !== -1 || py.indexOf(val.toUpperCase()) !== -1 || item[prop.name].indexOf(val) !== -1;
                },
                model: {icon: 'hidden', label: {type: 'text'}, type: 'fixed'},
                show() {
                    // 点击关闭layui下拉框
                    $('.layui-form-selected').removeClass('layui-form-selected');
                },
                initValue:[],
                layVerType: 'tips',
                tips: '请选择'
            };
            opt = $.extend({},options, opt);
            return xmSelect.render(opt);
        },
        /**
         * 获取xmSelect对象
         * @param elStr 对象查找规则，例如#ID
         * @param isOne 是否获取单个对象
         */
        getXmSelect: function (elStr,isOne) {
            if (elStr && isOne){
                return xmSelect.get(elStr,isOne);
            }
            if (elStr){
                return xmSelect.get(elStr);
            }
            return xmSelect.get();
        }
    };

    const $body = $('body');
    // 下拉框吸附定位
    $body.on('click','.layui-form-select',function () {
        const $this = $(this);
        $this.find('.layui-anim-upbit').css({
            position: 'fixed',
            left : $this.offset().left,
            top : $this.offset().top+$this.height()+4,
            width : $this.width(),
            'min-width' : $this.width()
        });
    });
    $body.on('click','.layui-iconpicker-icon',function () {
        const $this = $(this).parents('.layui-iconpicker');
        $this.find('.layui-anim-upbit').css({
            position: 'fixed',
            left : $this.offset().left,
            top : $this.offset().top+$this.height()+4,
            width : $this.width(),
            'min-width' : $this.width()
        });
    });

    exports('mobaiSelect', select);
})