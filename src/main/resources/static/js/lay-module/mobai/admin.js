layui.define(['mobaiUtil','mobaiService','mobaiCache','mobaiSelect'], function (exports) {
    // layui 内置模块
    const $ = layui.jquery, layer = layui.layer, form = layui.form, setter = layui.cache, table = layui.table;
    // mobai自定义模块
    const mbSelect = layui.mobaiSelect, mbUtil = layui.mobaiUtil, mbService = layui.mobaiService, mbCache = layui.mobaiCache;

    const admin = {
        // 常用数据缓存初始化
        cacheInit: function () {
            mbService.ajax({
                url: 'cache',
                success: function (res) {
                    layui.data(setter.adminName, {key: 'loginUser', value: res.data.loginUser});
                    $("#nickName").html(res.data.loginUser.nickName);
                    layui.data(setter.adminName, {key: 'dict', value: res.data.dict});
                    layui.data(setter.adminName, {key: 'dictTree', value: res.data.dictTree});
                    layui.data(setter.adminName, {key: 'dept', value: res.data.dept});
                    layui.data(setter.adminName, {key: 'deptTree', value: res.data.deptTree});
                    layui.data(setter.adminName, {key: 'deptScopeTree', value: res.data.deptScopeTree});
                }
            })
        },
        // 缓存页面数据
        setViewData: function (name, opt, isCover) {
            mbCache.setViewData(name, opt, isCover);
        },
        // 通过页面标识获取页面缓存数据
        getViewData: function (name) {
            return mbCache.getViewData(name);
        },
        // 缓存数据
        setTempData: function (key, val, isSession) {
            mbCache.setTempData(key, val, isSession);
        },
        // 获取缓存临时数据
        getTempData: function (key) {
            return mbCache.getTempData(key);
        },
        // 根据字典类型获取字典数据
        getDictByType: function (type, valueHeadStr) {
            return mbCache.getDictByType(type, valueHeadStr);
        },
        // 通过字典值和字典类型获取标签,多标签可以用逗号隔开
        getDictLabel: function (value, type) {
            return mbCache.getDictLabel(type, value);
        },
        // 通过字典值和字典类型获取表格标签，会对标签添加回显样式
        getTableDictLabel: function (value, type) {
            return mbCache.getDictLabel(type, value, true);
        },
        // 获取部门树,type=1 获取缓存中角色权限部门
        getDeptTree: function (type) {
            return mbCache.getDeptTree(type);
        },
        // 根据id获取部门名称
        getDeptName: function (id) {
            return mbCache.getDeptName(id);
        },
        /**
         * 请求
         * @param opt ajax的参数
         */
        req: function (opt) {
            return mbService.ajax(opt);
        },
        // 表格渲染
        table: function (opt) {
            return mbService.table(opt);
        },
        treeTable: function (opt) {
            return mbService.treeTable(opt);
        },
        // 打开弹窗页面
        view: function (opt) {
            return mbService.view(opt);
        },
        // 表单提交
        submit: function (url, data, viewName) {
            return mbService.submit(url, data, viewName);
        },
        // 移除没有权限的元素
        renderPerm: function () {
            mbService.renderPerm();
        },
        // 判断是否有权限
        hasPerm: function (p) {
            return mbService.hasPerm(p);
        },
        /**
         * 渲染普通下拉框
         * @param opt 参数集
         * @param opt.name          指定下拉框name (必填)
         * @param opt.filter        过滤,首选过滤id,其次过滤form表单上lay-filter,最后过滤下拉框上的lay-filter
         * @param opt.data          数据
         * @param opt.prop          数据模型，例如默认：{name: 'name', value: 'value'}
         * @param opt.placeholder:  提示词
         */
        rendSelect: function (opt) {
            return mbSelect.rendSelect(opt);
        },
        /**
         * 渲染xmSelect: <br>
         * 默认单选（隐藏图标）、开启搜索（支持拼音）、打开关闭其他打开的layui下拉框
         * @param opt 参数集
         */
        rendXmSelect: function (opt) {
            return mbSelect.rendXmSelect(opt);
        },
        /**
         * 获取xmSelect对象
         * @param elStr 对象查找规则，例如#ID
         * @param isOne 是否获取单个对象
         */
        getXmSelect: function (elStr, isOne) {
            return mbSelect.getXmSelect(elStr, isOne);
        },
        /**
         * 渲染字典下拉框
         * @param filter            过滤,首选过滤id,其次过滤form表单上lay-filter,最后过滤下拉框上的lay-filter
         * @param name              指定下拉框name (必填)
         * @param dictType          字典类型 (必填)
         * @param placeholder       提示标签 （非必填）
         * @param valueHeadStr      值指定开头(过滤用)
         */
        rendSelectForDict: function (filter, name, dictType, placeholder, valueHeadStr) {
            mbSelect.rendSelect({
                name: name, filter: filter, data: mbCache.getDictByType(dictType, valueHeadStr),
                placeholder: placeholder, prop: mbCache.dictProp
            });
        },
        // 渲染xmSelect模式的字典下拉框，字典数据模型{name: 'dictName', value: 'dictValue'}
        rendXmSelectForDict: function (obj) {
            obj.data = obj.data || mbCache.getDictByType(obj.dictType);
            obj.prop = obj.prop || {name: mbCache.dictProp.name, value: mbCache.dictProp.value, type: 'fixed'};
            return mbSelect.rendXmSelect(obj);
        },
        rendXmLinkSelect: function (obj) {
            obj.data = obj.data || mbCache.getDictTreeByType(obj.dictType);
            obj.model = obj.model || {icon: 'show', label: {type: 'text'}, type: 'fixed'};
            obj.prop = obj.prop || {name: mbCache.dictProp.name, value: mbCache.dictProp.value, type: 'fixed'};
            obj.cascader = obj.cascader || {show: true,indent: 182,strict: false};
            return mbSelect.rendXmSelect(obj);
        },
        // 渲染树状的字典下拉框，字典数据模型{name: 'dictName', value: 'dictValue'}
        rendXmTreeSelectForDict : function (obj) {
            obj.data = obj.data || mbCache.getDictTreeByType(obj.dictType);
            obj.model = obj.model || {icon: 'hidden', label: {type: 'text'}, type: 'fixed'};
            obj.prop = obj.prop || {name: mbCache.dictProp.name, value: mbCache.dictProp.value, type: 'fixed'};
            obj.tree = obj.tree || {show: true, indent: 15, strict: false, expandedKeys: true,clickExpand: false,clickCheck: true};
            return mbSelect.rendXmSelect(obj);
        },
        // 渲染部门下拉框
        rendXmSelectForDept: function (obj) {
            const opt = {
                prop: {name: 'deptName', value: 'deptId'},
                tree: {show: true, indent: 15, strict: false, expandedKeys: true, clickExpand: false},
                tips: '请选择部门'
            };
            obj = $.extend({},opt, obj);
            obj.data = obj.data || mbCache.getDeptTree();
            return mbSelect.rendXmSelect(obj);
        },
        // layui的表格监听
        tableOn: function (s, f) {
            return table.on(s, f);
        },
        // layui表格的数据选中
        tableCheck: function (f) {
            return table.checkStatus(f);
        },
        // layui表格的数据选中数据的指定字段数组
        tableCheckByField: function (f, field) {
            const ids = [];
            const checkRows = table.checkStatus('indexTable');
            if (checkRows.data.length > 0) {
                checkRows.data.map(function (d) {
                    ids.push(d[field]);
                });
            }
            return ids;
        },
        /** 可编辑表格完成回调,用于处理表格渲染完成后对下拉框赋值 */
        editTableSelectDone: function (res, curr, count,fn) {
            mbService.editTableSelectDone(res, curr, count,fn);
        },
        // layui的表单数据
        formVal: function (filter, obj) {
            return mbService.formVal(filter,obj);
        },
        // layui的表单监听
        formOn: function (s, f) {
            return form.on(s, f);
        },
        // 加载动画
        loading: function () {
            return layer.load(0, {shade: false, time: 2 * 1000});
        },
        // 普通提示
        msg: function (title) {
            layer.msg(title)
        },
        // 成功消息提示
        success: function (title) {
            return layer.msg(title, {icon: 1, shade: this.shade, scrollbar: false, time: 1000, shadeClose: true});
        },
        // 错误消息提示
        error: function (title) {
            return layer.msg(title, {icon: 2, shade: this.shade, scrollbar: false, time: 3000, shadeClose: true});
        },
        warning: function (title) {
            return layer.msg(title, {icon: 2});
        },
        // layui 询问框
        confirm: function (content, options, yes, cancel) {
            return layer.confirm(content, options, yes, cancel);
        },
        closeOpen: function (index) {
            index ? layer.close(index) : layer.closeAll();
        },
        // 关闭指定弹窗名的窗口，如果没有指定则全部关闭
        closeViewByName: function (i) {
            if (i && mbCache.getViewData(i).index) {
                admin.closeOpen(mbCache.getViewData(i).index);
                admin.setViewData(i, undefined, true);
            } else {
                layer.closeAll();
            }
        },
        /** 修改配置信息 */
        putSetting: function (key, value) {
            setter[key] = value;
            mbCache.setTempData('setter', setter);
        },
        util: {
            // 深度克隆对象
            deepClone: function (obj) {
                return mbUtil.deepClone(obj);
            },
            // 获取时间格式字符串，格式 yyyy-MM-dd HH:mm:ss
            timeStr: function (date) {
                return mbUtil.dateFormat(date, 'yyyy-MM-dd HH:mm:ss')
            },
            // 获取日期格式字符串，格式 yyyy-MM-dd
            dateStr: function (date) {
                return mbUtil.dateFormat(date, 'yyyy-MM-dd')
            },
            // 获取指定格式的时间字符串
            dateFormat: function (d,f) {
                return mbUtil.dateFormat(d, f);
            },
            // 字符串转时间，格式yyyy,MM,dd,HH,mm,ss，yyyy必须有
            toDate: function (dateStr, fmt) {
                return mbUtil.strToDate(dateStr, fmt);
            },
            // 日期加减 ，a - b相差天数,fmt 日期格式
            dateDiff: function (a, b, fmt) {
                return mbUtil.dateDiff(a, b, fmt);
            },
            // 转layui树的模型, list=数组,prop=数据模型设置{name:xx,value:xx,spread:true(节点是否默认展开)}
            toLayuiTree: function (list, prop) {
                return mbUtil.toLayuiTree(list, prop);
            },
            // 获取layui tree 选中的id
            layuiTreeCheckedId: function (data) {
                return mbUtil.layuiTreeCheckedId(data);
            },
            // 移除父级id
            removeParentId: function (ids, list, idName) {
                return mbUtil.removeParentId(ids, list, idName);
            },
            // 转化为json
            parseJSON: function (str) {
                return mbUtil.parseJSON(str);
            },
            /**
             * 基于layui表格的数据导出
             * @param layuiTable    layui表格对象
             * @param url           数据请求路径
             * @param fileName      保存的文件名
             */
            exportExcelByLayui: function (layuiTable,url,fileName) {
                return mbUtil.exportExcelByLayui(layuiTable,url,fileName);
            },
            // 页面高度
            pageHeight: document.documentElement.clientHeight || document.body.clientHeight,
            // 页面宽度
            pageWidth: document.documentElement.clientWidth || document.body.clientWidth,
            // 合并两个对象,返回一个新的对象
            extend: function (target, obj) {
                return $.extend({},target, obj);
            }
        }
    };

    admin.setter = setter;

    admin.renderPerm();
    exports('mobai', admin);
})