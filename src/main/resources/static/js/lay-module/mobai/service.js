layui.define(['mobaiUtil','mobaiCache','xmSelect','treeTable'], function (exports) {
    const $ = layui.jquery, layer = layui.layer, form = layui.form,setter = layui.cache, table = layui.table;
    const mbCache = layui.mobaiCache, mbUtil = layui.mobaiUtil;
    const xmSelect = layui.xmSelect, treeTable = layui.treeTable;

    const service = {

        /** 封装ajax请求
         * @param opt.url:      路径(必填)
         * @param opt.type:     请求方式，默认get
         * @param opt.dataType: 数据类型
         * @param opt.headers:  请求头
         * @param opt.async:    异步还是同步，默认true异步
         * @param opt.success:  成功回调
         * @param opt.error:    错误回调
         * @param opt.complete: 结束回调，只针对异步
         */
        ajax: function (opt) {
            if (!opt.url) return;
            const loadIndex = layer.load(2);
            const param = mbUtil.deepClone(opt);

            opt.dataType = opt.dataType || 'json';
            // 统一设置header
            const headers = setter.getAjaxHeaders(opt.url);
            if (headers) {
                for (let i = 0; i < headers.length; i++) {
                    if (opt.headers[headers[i].name] === undefined) opt.headers[headers[i].name] = headers[i].value;
                }
            }
            const errorMsg = function () {
                layer.msg("请求失败，请联系管理员处理", {
                    icon: 2,
                    shade: this.shade,
                    scrollbar: false,
                    time: 3000,
                    shadeClose: true
                });
            };
            // success预处理
            opt.success = function (result, status, xhr) {
                // 同步不调用complete，所以要在这里关闭加载动画
                if (opt.async === false) {
                    layer.close(loadIndex);
                }
                if (result.code && result.code == 501){
                    layer.msg(result.message || "请求失败，请联系管理员处理", {
                        icon: 2,
                        shade: this.shade,
                        scrollbar: false,
                        time: 3000,
                        shadeClose: true
                    });
                }
                if (result.code && result.code == 401){
                    layer.msg(result.message || "请重新登录", {icon: 2});
                    mbUtil.sleep(function () {
                        window.location = 'login';
                    },3000);
                    return;
                }
                param.success && param.success(result, status, xhr);
            }
            opt.error = function () {
                layer.close(loadIndex);
                errorMsg();
                param.error && param.error();
            }
            // ajax 结束处理回调，只支持异步
            opt.complete = function (XMLHttpRequest, textStatus) {
                layer.close(loadIndex);
                // 如果有自定义complete，那么最后执行
                param.complete && param.complete(XMLHttpRequest, textStatus);
            }
            $.ajax(opt);
        },
        /** 基于layui的表格渲染
         * @param opt.elem:     table 容器的选择器或 DOM，例如： ’#demo‘
         * @param opt.url:      数据请求路径(必填)
         * @param opt.done:     回调
         */
        table: function (opt) {
            return table.render(service.tableOpt(opt));
        },
        /** 可编辑表格完成回调,用于处理表格渲染完成后对下拉框赋值 */
        editTableSelectDone: function (res, curr, count,fn) {
            layui.each(res.data, function (i, item) {
                layui.each($('tbody tr[data-index='+i+'] select'), function (i1, item1) {
                    if ($(item1).attr('name') && item[$(item1).attr('name')]){
                        const val = item[$(item1).attr('name')] || '';
                        $(item1).val(val);
                        fn && fn(i,$(item1).attr('name'),val);
                    }
                })
            })
            form.render('select');
        },
        /** 基于layui的表格渲染
         * @param opt.elem:     table 容器的选择器或 DOM，例如： ’#demo‘
         * @param opt.url:      数据请求路径(必填)
         * @param opt.done:     回调
         * @param opt.where:    查询条件
         * @param opt.tree:     tree 模型
         */
        treeTable: function (opt) {
            if (!opt.tree){
                console.warn("未传入tree参数");
                return ;
            }
            if (opt.where) {
                opt.where.pageNo = 1;
                opt.where.pageSize = 10000;
            }else {
                opt.where = {pageNo:1,pageSize:1000};
            }
            return treeTable.render(service.tableOpt(opt));
        },
        tableOpt: function (opt) {
            // 默认参数
            const options = {
                text: {none: '暂无相关数据'},
                page: true, size: 'sm', cellMinWidth: 80,method: 'post',
                request: {pageName: 'pageNo',limitName: 'pageSize'},
                // 表格头部工具栏，默认开启过滤
                toolbar: true, defaultToolbar: ['filter','print']
            };
            const done = mbUtil.deepClone(opt.done);
            opt = $.extend({},options, opt);
            opt.done = function (res, curr, count) {
                if (res.code && res.code == 401){
                    if (res.message){
                        layer.msg(res.message, {icon: 2});
                    }else {
                        layer.msg("请重新登录", {icon: 2});
                    }
                    mbUtil.sleep(function () {
                        window.location = 'login';
                    },3000);
                }
                // 内容溢出单元格悬停提示内容
                $('.layui-table td>div').on({
                    mouseover: function () {
                        if (this.offsetWidth < this.scrollWidth) {
                            const that = this;
                            const text = $(this).text();
                            window.layer.tips(text, that, {tips: 1, time: -1});
                        }
                    },
                    mouseout: function () {
                        layer.closeAll('tips');
                    }
                })
                service.renderPerm();
                done && done(res, curr, count);
            }
            return opt;
        },
        /**
         * 打开弹窗页面,加载页面带table必须固定窗口大小，防止table无限拉伸
         * @param opt.name      窗口名称，必填，可以通过该名称传递数据
         * @param opt.title     窗口标题
         * @param opt.type      请求模式: 1 正常，2 iframe
         * @param opt.url       请求模式的路径
         * @param opt.content   内容:layui
         * @param opt.data      绑定弹窗的数据
         * @param opt.initTab   关闭弹窗调用tab的重载
         * @param opt.subBtnName 表单提交按钮filter名称，用于弹窗提交按钮触发提交
         * @param opt.end       窗口关闭回调函数,默认表单提交成功后对绑定的表格重载
         * @param opt.success   窗口打开完成回调函数
         */
        view: function (opt) {
            // 窗口名 或 内容不存在，那么打开失败
            if (opt.name === undefined || (opt.content === undefined && opt.url ===undefined)){
                layer.msg("页面打开失败", {icon: 2, shade: this.shade, scrollbar: false, time: 3000, shadeClose: true});
                return ;
            }
            const loadIndex = layer.load(2);
            // 默认参数
            const options = {
                type: 1, shadeClose: true, area: 'auto',
                maxWidth: 'calc(100% - 50px)', maxHeight: 'calc(100% - 20px)', // 弹窗的最大长宽
                scrollbar: false,// 不允许弹窗出现滚动条
            };
            // 窗口关闭回调
            const end = mbUtil.deepClone(opt.end);
            opt.end = function () {
                // 表单操作成功，并且绑定了表格对象，重新刷新表格
                if (mbCache.getViewData(opt.name).status) {
                    if (opt.initTab) {opt.initTab.reload();}
                }
                end && end.call();
                mbCache.setViewData(opt.name, {},true);
            }
            // 窗口打开完成回调
            const success = mbUtil.deepClone(opt.success);
            opt.success = function (layero, index) {
                // 打开完成，把窗口id绑定到数据，方便其他页面调用
                mbCache.setViewData(opt.name, {index: index, data: opt.data});
                // 有提交按钮，隐藏该按钮
                if (opt.subBtnName) {
                    $('button[lay-filter="' + opt.subBtnName + '"]').css('display', 'none');
                }
                success && success.call(layero, index);
            }

            // 如果有标识提交按钮lay-filter的名称，那么点击弹窗提交按钮自动触发对应按钮点击事件
            if (opt.subBtnName) {
                opt.btn = ['提交', '关闭'];
                opt.btn1 = function (index, layero) {
                    $('button[lay-filter="' + opt.subBtnName + '"]').trigger('click')
                };
                opt.btn2 = function (index, layero) {
                    layer.close(index);
                };
            }

            // 更新自定义参数
            opt = $.extend({},options, opt);

            // 如果是非iframe的url弹窗，那么同步请求加载页面信息
            if (opt.type === 1 && opt.url) {
                service.ajax({
                    url: opt.url,async: false,dataType: 'text',
                    success: function (res) {
                        opt.content = res.toString();
                    },
                    error: function () {
                        opt.content = '页面加载失败';
                    }
                })
            }

            if (opt.type === 2 && !opt.content && opt.url){
                opt.content = opt.url;
            }

            // 传递数据提前缓存，避免加载页面过快导致数据不同步
            mbCache.setViewData(opt.name, {data: opt.data || undefined},true);
            const index = layer.open(opt);
            $(window).resize();
            layer.close(loadIndex);
            return index;
        },
        /**
         * 表单的简单提交，即简单判断成功和失败
         * @param url           提交地址
         * @param data          提交数据
         * @param name          页面标识，用于关闭页面
         */
        submit: function (url, data, name) {
            service.ajax({url: url,data: data,method: 'post',
                success: function (res) {
                    if (res.code === 200 || res.code === 0) {
                        mbCache.setViewData(name, {status: true});
                    }else {
                        if (res.message){
                            layer.msg(res.message);
                        }else {
                            layer.msg("提交失败");
                        }
                    }
                }
            })
            return false;
        },
        /**
         * 判断是否有权限
         * @param p                 权限
         * @param u                 用户信息
         * @param u.userId          用户ID
         * @param u.permitList      用户权限列表
         */
        hasPerm: function (p,u) {
            if (!u) u = mbCache.loginUser;
            if (u.userId === 1) return true;
            if (u.permitList) {
                for (let i = 0; i < u.permitList.length; i++) {
                    if (p === u.permitList[i]) return true;
                }
            }
            return false;
        },
        // 移除没有权限的元素
        renderPerm: function () {
            if (mbCache.loginUser && mbCache.loginUser.userId !== 1){
                $('[perm-show]').each(function () {
                    if (!service.hasPerm($(this).attr('perm-show'), mbCache.loginUser)) {
                        $(this).remove();
                    }
                });
            }
        },
        // layui 的 form.val的扩展
        formVal: function (filter, obj) {
            if (!filter) return;
            if (!obj) {
                return form.val(filter);
            }
            // 参数存在，那就是赋值
            service.xmVal(filter, obj);
            form.val(filter, obj);
            // 如果表单包含viewText
            const viewTextArr = $('[lay-filter="' + filter + '"] [view-text]');
            if (viewTextArr.length > 0){
                layui.each(viewTextArr,function (i,item){
                    const $xm = $(item);
                    const name = $xm.attr('view-text');
                    if (obj[name]){
                        $xm.html(obj[name]);
                    }
                })
            }
        },
        xmVal: function (filter, obj, type) {
            // 如果表单下有xmSelect的框框
            const xmSelectArr = $('[lay-filter="' + filter + '"] xm-select');
            if (xmSelectArr.length > 0){
                layui.each(xmSelectArr,function (i,item){
                    const $xm = $(item);
                    const name = $xm.find('input').attr('name');
                    const divId = $xm.parent().attr("id");
                    const val = obj[name];
                    const that = xmSelect.get('#' + divId, true);
                    if (val && val !== 'undefined'){
                        if (Object.prototype.toString.call(val) === '[object Array]'){
                            that.setValue( val );
                        }else {
                            that.setValue([ val ]);
                        }
                    }else {
                        if (type === 'reset'){
                            // 防止表单的reset触发，并且未清空提示信息，所以重新渲染
                            $('#' + divId).html('');
                            that.options.initValue = [];
                            xmSelect.render(that.options);
                        } else if(!obj) {
                            that.setValue([]);
                        }
                    }
                })
            }
        }
    };

    //搜索栏重置按钮监听
    $("form button[type='reset']").on('click', function () {
        const $form = $(this).parents('.layui-form');
        service.xmVal($form.attr('lay-filter'),{},'reset')
    });

    exports('mobaiService', service);
})