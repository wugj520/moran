layui.define(function (exports) {
    const $ = layui.jquery;
    const util = {
        // 获取对象类型
        getClass: function (o) {
            if (o === undefined) return 'Undefined';
            if (o === null) return 'Null';
            return Object.prototype.toString.call(o).slice(8, -1);
        },
        // 延迟执行
        sleep: function (fun,time) {
            setTimeout(()=>{
                fun();
            },time);
        },
        /* 深度克隆对象 */
        deepClone: function (obj) {
            let result;
            const oc = util.getClass(obj);
            if (oc === 'Object') {
                result = {};
            } else if (oc === 'Array') {
                result = [];
            } else {
                return obj;
            }
            for (const key in obj) {
                if (!obj.hasOwnProperty(key)) continue;
                const copy = obj[key], cClass = util.getClass(copy);
                if (cClass === 'Object' || cClass === 'Array') {
                    result[key] = arguments.callee(copy); // 递归调用
                } else {
                    result[key] = obj[key];
                }
            }
            return result;
        },
        /**
         * 日期时间格式化
         * @param date 时间
         * @param fmt 格式
         */
        dateFormat: function (date, fmt) {
            if (!date) {
                return '';
            }
            let ret;
            date = new Date(date);
            const opt = {
                "y+": date.getFullYear().toString(),        // 年
                "M+": (date.getMonth() + 1).toString(),     // 月
                "d+": date.getDate().toString(),            // 日
                "H+": date.getHours().toString(),           // 时
                "m+": date.getMinutes().toString(),         // 分
                "s+": date.getSeconds().toString()          // 秒
                // 有其他格式化字符需求可以继续添加，必须转化成字符串
            };
            for (const k in opt) {
                ret = new RegExp("(" + k + ")").exec(fmt);
                if (ret) {
                    fmt = fmt.replace(ret[1], (ret[1].length === 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
                }
            }
            return fmt;
        },
        /**
         * 字符串根据格式转换为时间
         * @param str 时间字符串
         * @param fmt 格式
         */
        strToDate: function (str, fmt) {
            let ind, y, M, d, H, m, s;
            const time = new Date('1900/1/1');
            if (!str) {
                return time;
            }
            str = str + '';
            ind = fmt.indexOf("y");
            if (ind !== -1) {
                y = str.substr(ind, 4);
                time.setFullYear(y);
            }
            ind = fmt.indexOf("M");
            if (ind !== -1) {
                M = str.substr(ind, 2);
                time.setMonth(M - 1);
            }
            ind = fmt.indexOf("d");
            if (ind !== -1) {
                d = str.substr(ind, 2);
                time.setDate(d)
            }
            ind = fmt.indexOf("H");
            if (ind !== -1) {
                H = str.substr(ind, 2);
                time.setHours(H)
            }
            ind = fmt.indexOf("m");
            if (ind !== -1) {
                m = str.substr(ind, 2);
                time.setMinutes(m)
            }
            ind = fmt.indexOf("s");
            if (ind !== -1) {
                s = str.substr(ind, 2);
                time.setSeconds(s)
            }
            return time;
        },
        // 日期计算 ，a - b相差天数,fmt 日期格式
        dateDiff: function (a, b, fmt) {
            let d1, d2;
            const day = 24 * 60 * 60 * 1000;
            if (fmt) {
                d1 = util.strToDate(a, fmt);
                d2 = util.strToDate(b, fmt);
            } else {
                d1 = new Date(a);
                d2 = new Date(b);
            }
            return (d1 - d2) / day;
        },
        // 日期加减,返回yyyy-MM-dd格式的字符串
        dateAdd: function (date, num, type) {// Y,M,D
            const time = new Date(date);
            if (type === 'Y') {
                time.setFullYear(time.getFullYear() + num);
            } else if (type === 'M') {
                time.setMonth(time.getMonth() + num);
            } else if (type === 'D') {
                time.setTime(time.getTime() + (num * 24 * 60 * 60 * 1000));
            }
            return util.dateFormat(time, 'yyyy-MM-dd');
        },
        // 是否在列表
        inList: function (o, l) {
            for (let i = 0; i < l.length; i++) {
                if (o == l[i]) {
                    return true;
                }
            }
            return false;
        },
        // 转化为json
        parseJSON: function (str) {
            if (typeof str === 'string') {
                try {
                    return JSON.parse(str);
                } catch (e) {
                }
            }
            return str;
        },
        /**
         * 通过判断对象是否包含children OR child,获取父ID
         * @param ps 父ID数组容器
         * @param list 对象列表
         * @param idName id字段名
         */
        getParentIds: function (ps, list, idName) {
            if (!ps) ps = [];
            for (let i = 0; i < list.length; i++) {
                if (list[i].children || list[i].child) {
                    ps.push(list[i][idName]);
                    if (list[i].children) {
                        ps = util.getParentIds(ps, list[i].children, idName)
                    } else {
                        ps = util.getParentIds(ps, list[i].child, idName)
                    }
                }
            }
            return ps;
        },
        /**
         * 移除父级id
         * @param ids 待移除数组
         * @param list 数据
         * @param idName id字段名
         */
        removeParentId: function (ids, list, idName) {
            if (!ids || ids.length === 0 || !list || list.length === 0 || !idName) {
                return [];
            }
            const parentIdList = util.getParentIds([], list, idName);
            const result = [];
            for (let i = 0; i < ids.length; i++) {
                if (!util.inList(ids[i], parentIdList)) {
                    result.push(ids[i]);
                }
            }
            return result;
        },
        /** 判断是否是IE浏览器 */
        isIE: function () {
            const userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
            const isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器
            const isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器
            const isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
            return (isIE || isEdge || isIE11);
        },
        // 转layui树的模型, list=数组,prop=数据模型设置{name:xx,value:xx,spread:true(节点是否默认展开)}
        toLayuiTree: function (list, prop) {
            const tree = [];
            if (!list || list.length === 0) {
                console.log('参数为空')
                return tree;
            }
            list.forEach(function (item) {
                const t = {};
                t.title = item[prop.title];
                t.id = item[prop.id];
                if (item.children && item.children.length > 0) {
                    if (prop.spread) {
                        t.spread = true;
                    }
                    t.children = util.toLayuiTree(item.children, prop);
                }
                tree.push(t);
            })
            return tree;
        },
        // 获取layui tree 选中的id
        layuiTreeCheckedId: function (data) {
            let id = '';
            data.forEach(function (item) {
                id = (id === '') ? item.id : (id + ',' + item.id);
                if (item.children && item.children.length > 0) {
                    const i = util.layuiTreeCheckedId(item.children);
                    if (i) id = (id + ',' + i);
                }
            })
            return id;
        },
        exportExcelByLayui:function (layuiTable,url,fileName){
            let loading = layer.msg('正在获取数据，请稍后',{icon: 16,time:-1,shade: [0.1,'#fff']});
            // 由于一次性导出吃内存厉害，所以禁止导出较大的数据量
            if (layuiTable.config.page.count > 50000){
                return layer.msg('一次导出的数据请不要超过5W！请分批导出');
            }
            // 条件继承克隆
            const where = $.extend({}, layuiTable.config.where,{page: 1, limit: 50000, pageNo: 1, pageSize: 50000});
            if(layui.device.ie) return layer.msg('IE 浏览器不支持导出');
            setTimeout(function () {
                $.ajax({
                    url: url,data:where,async:false,timeout: 5*60*1000,method: 'post',
                    success: function (res) {
                        layer.close(loading);
                        if ((res.code && res.code == 501) || !res.data){
                            layer.msg("导出数据失败！！", {icon: 2, shade: this.shade, scrollbar: false, time: 3000, shadeClose: true});
                            return;
                        }
                        const config = $.extend(true, {}, layuiTable.config);
                        if (res.count > 50000){
                            layer.confirm('数据获取成功,但是由于数据较大，生成excel对电脑要求比较高并且页面会产生卡顿情况，请确认是否继续导出？',{btn: ['继续','取消'] },function(index){
                                layer.close(index);
                                util.createExcelByLayui(config,res.data,fileName);
                            })
                        } else {
                            loading = layer.msg('数据获取成功，正在生成表格，请稍后（生成时间根据数据量决定）',{icon: 16,time:-1,shade: [0.1,'#fff']});
                            setTimeout(function () {
                                util.createExcelByLayui(config,res.data,fileName);
                                layer.close(loading);
                            },1000)
                        }
                    },
                    error: function () {
                        layer.close(loading);
                        layer.msg("导出数据失败！！", {icon: 2, shade: this.shade, scrollbar: false, time: 3000, shadeClose: true});
                    }
                })
            },500);
        },
        // 获取layui 数据字段
        getDataCols: function (tableCols,callback) {
            const arr = util.arrayCol(tableCols);
            const colArr = [];
            //重新遍历列，如果有子列，则进入递归，获取显示的字段
            const eachArr = function (obj) {
                layui.each(obj || arr, function(i, item){
                    if (item.CHILD_COLS) return eachArr(item.CHILD_COLS);
                    if (!callback) {colArr.push(item);}
                    typeof callback === 'function' && callback(i, item);
                });
            };
            eachArr();
            return callback ? arr : colArr;
        },
        arrayCol: function (tableCols) {
            const cols = $.extend(true,[],tableCols);
            let arr = [], index = 0;
            layui.each(cols,function (i1,item1) {
                layui.each(item1,function (i2,item2) {
                    //如果是组合列，则捕获对应的子列
                    if(item2.colGroup){
                        index++;
                        item2.CHILD_COLS = [];
                        layui.each(cols[i1 + 1], function(i22, item22){
                            if (item22.parentKey === item2.key){
                                item22.PARENT_COL_INDEX = index;
                                item2.CHILD_COLS.push(item22);
                            }
                        })
                    }
                    if(item2.PARENT_COL_INDEX) return; //如果是子列，则不进行追加，因为已经存储在父列中
                    arr.push(item2)
                })
            })
            return arr;
        },
        // 导出表格的表头数据，支持复杂表头，但是未合并单元格
        getDataTitle: function (cols) {
            const title = [];
            layui.each(cols,function(i1,item1){
                let arr = [];
                layui.each(item1,function (i2,item2) {
                    if(item2.field && item2.type == 'normal' && !item2.hide){
                        arr.push(item2.title);
                        if (item2.colspan > 1){
                            for (let i=0; i<(item2.colspan-1); i++){
                                arr.push('');
                            }
                        }
                    }
                })
                if (i1 > 0){
                    for (let j=0; j<i1; j++){
                        let l = 0;
                        layui.each(cols[j],function (i3,item3) {
                            if (item3.rowspan && item3.rowspan > (i1-j)){
                                arr.splice(l,0,'');
                            }
                            l += (item3.colspan ? item3.colspan : 1);
                        })
                    }
                }
                title.push(arr.join(','));
            })
            return title.join('\r\n');
        },
        createExcelByLayui:function (config,data,name) {
            const alink = document.createElement("a");
            alink.href = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8,\ufeff'+ encodeURIComponent(function(){
                let dataMain = [], content = '';
                const cols = util.getDataCols(config.cols);
                layui.each(data,function(i1,item1){
                    const valArr = [];
                    layui.each(cols,function(i3,item3){
                        if(item3.field && item3.type == 'normal' && !item3.hide){
                            content = item1[item3.field];
                            if(content === undefined || content === null) content = '';
                            valArr.push('"'+ util.parseTempData(item3,content,item1,true) + '"');
                        }
                    })
                    dataMain.push(valArr.join(','));
                })
                return util.getDataTitle(config.cols) + '\r\n' + dataMain.join('\r\n') + '\r\n' + dataTotal.join(',');
            }());
            alink.download = (name || 'table_'+ (config.index || '')) + '.xlsx';
            document.body.appendChild(alink);
            alink.click();
            document.body.removeChild(alink);
        },
        parseTempData:function (item3,content,item2,text) {
            //获取内容
            content = item3.templet ? function () {
                return typeof item3.templet === 'function' ? item3.templet(item2) : layui.laytpl($(item3.templet).html() || String(content)).render(item2)
            }() : content;
            if (text){
                content = $('<div>'+ content +'</div>').text();
            }
            if (typeof(content)=='string'){
                content = content.replaceAll('"','""');
            }else if (/^\d+\.?\d*$/.test(content)){
                content += '\t';
            }
            return content;
        }
    };

    exports('mobaiUtil', util);
})