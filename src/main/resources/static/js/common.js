//此处放layui自定义扩展

// 基础路径
window.rootPath = (function (src) {
    src = document.scripts[document.scripts.length - 1].src;
    return src.substring(0, src.lastIndexOf("/") + 1);
})();

layui.config({
    // 加载模块不缓存
    version: true,
    // 多标签是否开启
    pageTabs: true,
    // 是否缓存已经打开的Tab
    cacheTab: true,
    // 最大打开的Tab数量
    maxTabNum: 20,
    // 是否开启Tab鼠标右键菜单
    openTabCtxMenu: false,
    // 是否关闭页脚
    closeFooter: false,
    // 是否切换Tab自动刷新页面
    tabAutoRefresh: false,
    // 存储名
    adminName: 'moran',
    // admin.req的url会自动在前面加这个
    baseServer: '',
    //设定扩展的 layui 模块的所在目录，一般用于外部模块扩展
    base: rootPath + "lay-module/",
    // 字典默认字段模型
    dictProp: {value: 'dictValue', name: 'dictName',dictId:'dictId',parentId:'parentId'},
    // 菜单默认字段模型，用于菜单的加载
    menuProp: {menuId: 'menuId', title: 'menuTitle', icon: 'menuIcon', url: 'menuHref', target: 'target', children: 'children'},
    // ajax统一传递header
    getAjaxHeaders: function (url) {
        return [];
    },
}).extend({
    mobaiUtil: 'mobai/util',mobaiCache: 'mobai/cache',mobaiService: 'mobai/service',mobaiSelect: 'mobai/select', mobai: 'mobai/admin',
    xmSelect: 'select/xm-select',pinyin: 'pinyin/pinyin',treeTable: 'treeTable/treeTable',
    iconPicker: 'iconPicker/iconPicker',iconPickerFa: 'iconPicker/iconPickerFa'
})