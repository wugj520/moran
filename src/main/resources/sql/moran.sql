/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : moran

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 29/03/2022 11:14:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `config_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数名称',
  `config_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数编码',
  `config_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数值',
  `sys_flag` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统参数标识',
  `config_type` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数类型',
  `config_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数描述',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '用户管理-账号初始密码', 'user_init_password', 'MoBai789456', 'Y', 'SYS_USER', '账号的初始密码，及重置的初始密码', NULL, NULL, 1, '2022-03-18 09:36:28', '1');
INSERT INTO `sys_config` VALUES (2, '账号自助-是否开启用户注册功能', 'user_register_switch', 'FALSE', 'Y', 'SYS_USER', '是否开启注册用户功能（true开启，false关闭）', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_config` VALUES (3, '用户管理-密码字符强度等级', 'user_password_level', '2', 'Y', 'SYS_USER', '0：密码长度最少8位数；\n1：在最低8位密码长度，最少包含两种字符类型（数字、小写字母、大写字母、特殊符号、中文）；\n2：在最低8位密码长度，最少包含三种字符类型（数字、小写字母、大写字母、特殊符号、中文）；\n3：在最低8位密码长度，最少包含四种字符类型（数字、小写字母、大写字母、特殊符号、中文）；\n4：在最低8位密码长度，最少包含五种字符类型（数字、小写字母、大写字母、特殊符号、中文）；\n目前支持的特殊字符包括：~!@#$%^&*()-=_+，不能包含空格', NULL, NULL, 1, '2022-03-22 13:32:45', '1');
INSERT INTO `sys_config` VALUES (4, '用户管理-账号密码更新周期', 'user_password_update_days', '90', 'Y', 'SYS_USER', '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒账号未及时修改密码所禁用。\r\n填0则无视。', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_config` VALUES (5, '用户管理-初始密码禁用周期', 'user_init_password_update_days', '7', 'Y', 'SYS_USER', '初始密码禁用周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期未修改密码，则在登录时就会提醒账号未及时修改初始密码所禁用', NULL, NULL, 1, '2022-03-28 14:52:11', '1');
INSERT INTO `sys_config` VALUES (6, '用户管理-单点登录是否启用', 'user_login_only', '0', 'Y', 'SYS_USER', '单点登录是否开启，1：开启，同时只有一个登录连接可以使用，可以重新登录识别为最新连接。0：不开启', NULL, NULL, 1, '2022-03-28 14:51:14', '1');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dept_name` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `parent_id` bigint(0) NULL DEFAULT 0 COMMENT '父ID',
  `ancestors` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '祖级列表',
  `dept_area` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门区域',
  `dept_sort` int(0) NULL DEFAULT NULL COMMENT '部门排序',
  `dept_leader` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门负责人',
  `dept_tel` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门联系电话',
  `dept_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门邮箱',
  `dept_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门状态',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1504287506431152132 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, '成都市', 0, '0', NULL, 1, '', '', '', '1', NULL, NULL, 1, '2022-03-17 10:43:30', '1');
INSERT INTO `sys_dept` VALUES (2, '武侯区', 1, '0,1', NULL, 1, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_dept` VALUES (3, '青羊区', 1, '0,1', NULL, 2, '', '', '', '1', NULL, NULL, 1, '2022-03-18 17:14:30', '1');
INSERT INTO `sys_dept` VALUES (4, '武侯分公司1', 2, '0,1,2', NULL, 1, '', '', '', '1', NULL, NULL, 1, '2022-03-28 00:23:30', '1');
INSERT INTO `sys_dept` VALUES (5, '武侯分公司2', 2, '0,1,2', NULL, 2, '', '', '', '1', NULL, NULL, 1, '2022-03-28 00:44:58', '1');
INSERT INTO `sys_dept` VALUES (6, '武侯西区分公司', 4, '0,1,2,4', NULL, 1, '', '', '', '1', 1, '2022-03-17 10:44:25', 1, '2022-03-17 10:59:00', '0');
INSERT INTO `sys_dept` VALUES (7, '郫都区', 1, '0,1', NULL, 3, '', '', '', '1', 1, '2022-03-17 11:09:32', NULL, NULL, '1');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `dict_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dict_name` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
  `dict_value` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典值',
  `dict_type_code` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型编码',
  `parent_id` bigint(0) NULL DEFAULT 0 COMMENT '父级ID',
  `dict_sort` int(0) NULL DEFAULT 999 COMMENT '字典排序号',
  `default_flag` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '系统默认标识',
  `table_style_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格样式类型',
  `dict_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '字典状态',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '男', '1', 'sex', 0, 1, 'N', 'layui-bg-blue', '1', NULL, NULL, 1, '2022-03-24 17:43:53', '1');
INSERT INTO `sys_dict` VALUES (2, '女', '2', 'sex', 0, 2, 'N', 'layui-bg-orange', '1', NULL, NULL, 1, '2022-03-24 16:59:10', '1');
INSERT INTO `sys_dict` VALUES (3, '未知', '0', 'sex', 0, 999, 'Y', 'layui-bg-orange', '1', NULL, NULL, 1, '2022-03-24 17:20:40', '1');
INSERT INTO `sys_dict` VALUES (4, '在用', '1', 'common_status', 0, 1, 'Y', 'layui-bg-green', '1', NULL, NULL, 1, '2022-03-24 16:59:49', '1');
INSERT INTO `sys_dict` VALUES (5, '停用', '0', 'common_status', 0, 2, 'N', 'layui-bg-red', '1', NULL, NULL, 1, '2022-03-24 16:59:55', '1');
INSERT INTO `sys_dict` VALUES (6, '是', 'Y', 'yes_or_no', 0, 1, 'N', 'layui-bg-blue', '1', NULL, NULL, 1, '2022-03-24 17:05:40', '1');
INSERT INTO `sys_dict` VALUES (7, '否', 'N', 'yes_or_no', 0, 2, 'Y', 'layui-bg-orange', '1', NULL, NULL, 1, '2022-03-24 17:05:44', '1');
INSERT INTO `sys_dict` VALUES (8, '其他', '0', 'operator_type', 0, 1, 'N', '', '1', NULL, NULL, 1, '2022-03-24 17:06:15', '1');
INSERT INTO `sys_dict` VALUES (9, '新增', '1', 'operator_type', 0, 2, 'N', 'layui-bg-blue', '1', NULL, NULL, 1, '2022-03-24 17:06:25', '1');
INSERT INTO `sys_dict` VALUES (10, '删除', '2', 'operator_type', 0, 3, 'N', 'layui-bg-red', '1', NULL, NULL, 1, '2022-03-24 17:06:30', '1');
INSERT INTO `sys_dict` VALUES (11, '编辑', '3', 'operator_type', 0, 4, 'N', 'layui-bg-orange', '1', NULL, NULL, 1, '2022-03-24 17:06:34', '1');
INSERT INTO `sys_dict` VALUES (12, '查询', '4', 'operator_type', 0, 5, 'N', 'layui-bg-green', '1', NULL, NULL, 1, '2022-03-24 17:06:40', '1');
INSERT INTO `sys_dict` VALUES (13, '导出', '5', 'operator_type', 0, 6, 'N', 'layui-bg-orange', '1', NULL, NULL, 1, '2022-03-24 17:06:46', '1');
INSERT INTO `sys_dict` VALUES (14, '导入', '6', 'operator_type', 0, 7, 'N', 'layui-bg-orange', '1', NULL, NULL, 1, '2022-03-24 17:06:50', '1');
INSERT INTO `sys_dict` VALUES (15, '清空', '7', 'operator_type', 0, 8, 'N', 'layui-bg-red', '1', NULL, NULL, 1, '2022-03-24 17:06:57', '1');
INSERT INTO `sys_dict` VALUES (16, '通知', '1', 'notice_type', 0, 1, 'N', 'layui-bg-orange', '1', NULL, NULL, 1, '2022-03-24 17:10:30', '1');
INSERT INTO `sys_dict` VALUES (17, '公告', '2', 'notice_type', 0, 2, 'N', 'layui-bg-blue', '1', NULL, NULL, 1, '2022-03-24 17:10:35', '1');
INSERT INTO `sys_dict` VALUES (18, '全部数据权限', '1', 'data_scope', 0, 1, 'N', 'layui-bg-red', '1', NULL, NULL, 1, '2022-03-24 17:11:32', '1');
INSERT INTO `sys_dict` VALUES (19, '自定义数据权限', '2', 'data_scope', 0, 1, 'N', 'layui-bg-orange', '1', NULL, NULL, 1, '2022-03-24 17:11:36', '1');
INSERT INTO `sys_dict` VALUES (20, '本部门权限', '3', 'data_scope', 0, 1, 'N', 'layui-bg-green', '1', NULL, NULL, 1, '2022-03-24 17:11:39', '1');
INSERT INTO `sys_dict` VALUES (21, '本部门及以下数据权限', '4', 'data_scope', 0, 1, 'N', 'layui-bg-green', '1', NULL, NULL, 1, '2022-03-24 17:11:45', '1');
INSERT INTO `sys_dict` VALUES (22, '仅本人数据权限', '5', 'data_scope', 0, 1, 'Y', 'layui-bg-green', '1', NULL, NULL, 1, '2022-03-24 17:11:48', '1');
INSERT INTO `sys_dict` VALUES (23, '模块', 'M', 'menu_type', 0, 1, 'N', 'layui-bg-green', '1', NULL, NULL, 1, '2022-03-24 17:11:14', '1');
INSERT INTO `sys_dict` VALUES (24, '菜单', 'C', 'menu_type', 0, 2, 'N', 'layui-bg-blue', '1', NULL, NULL, 1, '2022-03-24 17:11:21', '1');
INSERT INTO `sys_dict` VALUES (25, '按钮', 'F', 'menu_type', 0, 3, 'N', 'layui-bg-orange', '1', NULL, NULL, 1, '2022-03-24 17:11:24', '1');
INSERT INTO `sys_dict` VALUES (26, '未知1', '01', 'sex', 3, 1, 'N', 'layui-bg-orange', '1', 1, '2022-03-18 15:07:48', 1, '2022-03-24 17:51:54', '1');
INSERT INTO `sys_dict` VALUES (27, '成功', '1', 'operator_status', 0, 1, 'N', 'layui-bg-blue', '1', 1, '2022-03-18 15:48:43', 1, '2022-03-24 17:11:02', '1');
INSERT INTO `sys_dict` VALUES (28, '失败', '0', 'operator_status', 0, 2, 'N', 'layui-bg-orange', '1', 1, '2022-03-18 15:48:52', 1, '2022-03-24 17:11:06', '1');
INSERT INTO `sys_dict` VALUES (29, '强退', '8', 'operator_type', 0, 9, 'N', 'layui-bg-orange', '1', 1, '2022-03-22 12:25:39', 1, '2022-03-24 17:07:00', '1');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_type_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dict_type_name` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型名称',
  `dict_type_code` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型编码',
  `dict_type_remark` varchar(900) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典类型备注',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` int(0) NULL DEFAULT 1 COMMENT '删除标志',
  PRIMARY KEY (`dict_type_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别	', 'sex', '用户性别列表', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_dict_type` VALUES (2, '常用状态', 'common_status', '常用状态列表', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_dict_type` VALUES (3, '系统是否', 'yes_or_no', '系统是否列表', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_dict_type` VALUES (4, '操作类型', 'operator_type', '操作类型列表', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_dict_type` VALUES (5, '通知类型', 'notice_type', '通知类型列表', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_dict_type` VALUES (6, '数据范围', 'data_scope', '数据范围权限', NULL, NULL, 1, '2022-03-18 11:12:38', 1);
INSERT INTO `sys_dict_type` VALUES (8, '菜单类型', 'menu_type', '菜单类型列表', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_dict_type` VALUES (9, '数据范围', 'data_scope', '数据范围权限', 1, '2022-03-18 10:56:34', 1, '2022-03-18 11:13:38', 0);
INSERT INTO `sys_dict_type` VALUES (10, '操作状态', 'operator_status', '成功 or 失败', 1, '2022-03-18 11:14:01', 1, '2022-03-18 15:48:05', 1);
INSERT INTO `sys_dict_type` VALUES (11, '公告类型', 'notice_type', '', 1, '2022-03-25 10:20:41', NULL, NULL, 1);

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log`  (
  `login_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `login_user_name` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录账号',
  `login_password` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录密码',
  `login_ip` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录IP',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录地点',
  `login_browser` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录浏览器',
  `login_os` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录系统',
  `login_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录状态',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  `login_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录提示',
  `deleted` int(0) NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`login_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1504256601499099212 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '登录日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `menu_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `parent_id` bigint(0) NULL DEFAULT NULL COMMENT '父ID',
  `menu_sort` int(0) NULL DEFAULT NULL COMMENT '显示排序',
  `menu_href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单链接',
  `target` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '打开方式',
  `menu_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单类型',
  `menu_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单状态',
  `permit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `menu_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1055 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, NULL, '_self', 'M', '1', NULL, 'layui-icon-set', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, '', '_self', 'M', '1', '', 'layui-icon-console', NULL, NULL, 1, '2022-03-22 16:31:32', '1');
INSERT INTO `sys_menu` VALUES (10, '组织架构', 1, 1, NULL, '_self', 'M', '1', NULL, 'layui-icon-user', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (11, '权限管理', 1, 2, NULL, '_self', 'M', '1', NULL, 'layui-icon-auz', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (12, '配置管理', 1, 3, NULL, '_self', 'M', '1', NULL, 'layui-icon-set', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (13, '日志管理', 1, 4, NULL, '_self', 'M', '1', NULL, 'layui-icon-date', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (14, '系统监控', 2, 1, '', '_self', 'M', '0', '', 'layui-icon-console', NULL, NULL, 1, '2022-03-22 16:31:43', '1');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 10, 1, 'system/user/index', '_self', 'C', '1', 'system:user:find', 'layui-icon-username', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (101, '部门管理', 10, 2, 'system/dept/index', '_self', 'C', '1', 'system:dept:find', 'layui-icon-friends', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (102, '岗位管理', 10, 3, 'system/post/index', '_self', 'C', '0', 'system:post:find', 'layui-icon-at', NULL, NULL, 1, '2022-03-18 09:34:11', '1');
INSERT INTO `sys_menu` VALUES (103, '角色管理', 11, 1, 'system/role/index', '_self', 'C', '1', 'system:role:find', 'layui-icon-friends', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (104, '菜单管理', 11, 2, 'system/menu/index', '_self', 'C', '1', 'system:menu:find', 'layui-icon-app', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 12, 1, 'system/dict/index', '_self', 'C', '1', 'system:dict:find', 'layui-icon-template', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 12, 2, 'system/config/index', '_self', 'C', '1', 'system:config:find', 'layui-icon-set-fill', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 12, 3, 'system/notice/index', '_self', 'C', '1', 'system:notice:find', 'layui-icon-notice', NULL, NULL, 1, '2022-03-25 10:20:15', '1');
INSERT INTO `sys_menu` VALUES (108, '操作日志', 13, 1, 'system/log/operator/index', '_self', 'C', '1', 'system:operator:find', 'layui-icon-note', NULL, NULL, 1, '2022-03-18 09:36:15', '1');
INSERT INTO `sys_menu` VALUES (109, '登录日志', 13, 2, 'system/log/login/index', '_self', 'C', '1', 'system:login:find', 'layui-icon-flag', NULL, NULL, 1, '2022-03-18 09:36:02', '1');
INSERT INTO `sys_menu` VALUES (110, '在线用户', 2, 1, 'monitor/online/index', '_self', 'C', '1', 'monitor:online:find', 'layui-icon-username', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (111, '定时任务', 2, 2, 'monitor/job/index', '_self', 'C', '0', 'monitor:job:find', 'layui-icon-date', NULL, NULL, 1, '2022-03-18 09:50:25', '1');
INSERT INTO `sys_menu` VALUES (112, '数据监控', 14, 1, 'monitor/druid/index', '_self', 'C', '0', 'monitor:druid:find', 'layui-icon-windows', NULL, NULL, 1, '2022-03-22 12:24:20', '1');
INSERT INTO `sys_menu` VALUES (113, '服务监控', 14, 2, 'monitor/server/index', '_self', 'C', '0', 'monitor:server:find', 'layui-icon-vercode', NULL, NULL, 1, '2022-03-22 12:24:28', '1');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 1, NULL, '_self', 'F', '1', 'system:user:add', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 2, NULL, '_self', 'F', '1', 'system:user:edit', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 3, NULL, '_self', 'F', '1', 'system:user:del', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1004, '重置密码', 100, 7, NULL, '_self', 'F', '1', 'system:user:edit', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1005, '部门新增', 101, 1, NULL, '_self', 'F', '1', 'system:dept:add', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1006, '部门修改', 101, 2, NULL, '_self', 'F', '1', 'system:dept:edit', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1007, '部门删除', 101, 3, NULL, '_self', 'F', '1', 'system:dept:del', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 103, 1, NULL, '_self', 'F', '1', 'system:role:add', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 103, 2, NULL, '_self', 'F', '1', 'system:role:edit', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 103, 3, NULL, '_self', 'F', '1', 'system:role:del', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1011, '菜单新增', 104, 1, NULL, '_self', 'F', '1', 'system:menu:add', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1012, '菜单修改', 104, 2, NULL, '_self', 'F', '1', 'system:menu:edit', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1013, '菜单删除', 104, 3, NULL, '_self', 'F', '1', 'system:menu:del', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1014, '字典新增', 105, 1, NULL, '_self', 'F', '1', 'system:dict:add', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1015, '字典修改', 105, 2, NULL, '_self', 'F', '1', 'system:dict:edit', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1016, '字典删除', 105, 3, NULL, '_self', 'F', '1', 'system:dict:del', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1017, '参数新增', 106, 1, NULL, '_self', 'F', '1', 'system:config:add', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1018, '参数修改', 106, 2, NULL, '_self', 'F', '1', 'system:config:edit', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1019, '参数删除', 106, 3, NULL, '_self', 'F', '1', 'system:config:del', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1020, '通知新增', 107, 1, NULL, '_self', 'F', '1', 'system:notice:add', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1021, '通知修改', 107, 2, NULL, '_self', 'F', '1', 'system:notice:edit', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1022, '通知删除', 107, 3, NULL, '_self', 'F', '1', 'system:notice:del', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1023, '操作删除', 108, 1, NULL, '_self', 'F', '1', 'system:operator:del', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1024, '日志导出', 108, 2, NULL, '_self', 'F', '1', 'system:operator:export', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1025, '登录删除', 109, 1, NULL, '_self', 'F', '1', 'system:login:del', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1026, '日志导出', 109, 2, NULL, '_self', 'F', '1', 'system:login:export', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1027, '批量强退', 110, 1, NULL, '_self', 'F', '1', 'monitor:online:batch', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1028, '单条强退', 110, 2, NULL, '_self', 'F', '1', 'monitor:online:one', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1029, '任务修改', 110, 2, NULL, '_self', 'F', '1', 'monitor:job:edit', '#', NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_menu` VALUES (1054, 'TEST', 0, 3, '', NULL, 'M', '1', NULL, 'layui-icon-app', 1, '2022-03-18 09:23:58', 1, '2022-03-18 09:35:01', '0');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `notice_title` varchar(900) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '通知标题',
  `notice_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '通知类型',
  `notice_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '通知内容',
  `notice_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '发给', '2', '<p><span style=\"color: rgb(0, 32, 96);\">打啥啊啊啊啊啊啊啊啊啊啊啊</span></p><p><span style=\"color: rgb(0, 176, 240);\">R9 6900HS 处理器</span></p><p><span style=\"color: rgb(0, 176, 240);\">RX 6800S 8GB 独立显卡（100W）</span></p><p><span style=\"color: rgb(0, 176, 240);\">16GB 4800MHz 内存</span></p><p><span style=\"color: rgb(0, 176, 240);\">1TB 固态硬盘</span></p><p><span style=\"color: rgb(0, 176, 240);\">14英寸 2560×1600分辨率 100%DCI-P3色域 120Hz刷新率 IPS屏</span></p><p><span style=\"color: rgb(0, 176, 240);\">厚 19.5~21.1mm</span></p><p><span style=\"color: rgb(0, 176, 240);\">机身重 1.7kg</span></p><p><span style=\"color: rgb(0, 176, 240);\">适配器重 725g</span></p><p><font color=\"#00b0f0\">百度</font></p>', NULL, 1, '2022-03-28 09:29:44', 1, '2022-03-28 10:27:16', '1');
INSERT INTO `sys_notice` VALUES (2, '啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊', '1', '<p><span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: xxx-large;\">【购买建议】</span></p><p>1，对屏幕素质要求较高</p><p>2，对游戏性能和便携性都有要求</p><p>3，追求个性的高预算消费者</p><p>ROG 幻14最大的特点就是外观设计，“AniMe Matrix”矩阵灯效历时多年依旧是绝无仅有的设计。</p><p>屏幕方面，它依然是那块高分辨率的120Hz广色域屏，实测色域容积为102.2%DCI-P3，色域覆盖为97.9%DCI-P3，平均ΔE 1.59，最大ΔE 4.88。</p><p>接口方面，机身左侧有一个HDMI2.0、全功能Type-C、3.5mm音频接口；</p><p>右侧有两个USB3.2 Gen2 Type-A、USB3.2 Gen2 Type-C(支持DP1.4)、MicroSD卡槽。</p><p>续航方面，它的PCMark10续航测试成绩为10小时26分钟。</p><p>噪音方面，它的满载人位分贝值为52.2dB。（环境31.7dB）</p><p><span style=\"color: rgb(255, 0, 0);\">【注意】：我们更新了噪音测试方式，换新了设备，测试位置更新为15cm远+30cm高。</span></p> ', NULL, 1, '2022-03-28 10:44:18', NULL, NULL, '1');

-- ----------------------------
-- Table structure for sys_operator_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_operator_log`;
CREATE TABLE `sys_operator_log`  (
  `operator_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `operator_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `operator_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '业务类型',
  `operator_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '方法名称',
  `operator_user` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `operator_ip` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作ip',
  `operator_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作地点',
  `request_way` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `request_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求路径',
  `request_param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
  `result_param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回参数',
  `operator_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态',
  `operator_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '操作提示',
  `operator_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `deleted` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`operator_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1504289518677819811 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_operator_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `post_name` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位名称',
  `post_code` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位编码',
  `post_sort` int(0) NULL DEFAULT NULL COMMENT '显示顺序',
  `post_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位状态',
  `remark` varchar(900) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------

-- ----------------------------
-- Table structure for sys_read
-- ----------------------------
DROP TABLE IF EXISTS `sys_read`;
CREATE TABLE `sys_read`  (
  `read_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `link_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '连接类型',
  `link_id` bigint(0) NULL DEFAULT NULL COMMENT '连接ID',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`read_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '已读' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_read
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色编码',
  `role_sort` int(0) NULL DEFAULT NULL COMMENT '角色排序',
  `data_scope` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据范围',
  `role_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色状态',
  `role_remark` varchar(900) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '普通用户', 'user', 1, '2', '1', '', NULL, NULL, 1, '2022-03-22 17:31:38', '1');
INSERT INTO `sys_role` VALUES (2, '测试', 'test', 2, '4', '0', '', 1, '2022-03-17 16:54:12', 1, '2022-03-17 17:06:04', '0');
INSERT INTO `sys_role` VALUES (3, '测试', 'test', 2, '5', '1', '', 1, '2022-03-17 17:08:03', NULL, NULL, '1');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(0) NOT NULL COMMENT '部门ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (1, 5);
INSERT INTO `sys_role_dept` VALUES (1, 4);
INSERT INTO `sys_role_dept` VALUES (1, 1504287506431152000);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(0) NOT NULL COMMENT '菜单ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_name` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `nick_name` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `dept_id` bigint(0) NULL DEFAULT NULL COMMENT '所属部门',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态',
  `last_login_ip` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后次登录IP',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后次登录时间',
  `last_pwd_update_time` datetime(0) NULL DEFAULT NULL COMMENT '最新密码修改时间',
  `create_user` bigint(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` bigint(0) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` int(0) NULL DEFAULT NULL COMMENT '删除标志',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '系统管理员', '$2a$10$M0K6RLefacD7G0S.bqVic.nZIcCu4C4YvOOCE4aiyqppuP8FYsyPO', 0, NULL, NULL, NULL, NULL, '1', '127.0.0.1', '2022-03-28 14:49:56', '2022-03-22 16:56:23', NULL, '2022-03-22 15:27:22', 1, '2022-03-22 15:27:28', 1);
INSERT INTO `sys_user` VALUES (2, 'test123', '普通用户', '$2a$10$1x3QIbJbBvNH5l0iknXvgO0BmnbPDCc71kGBT8e5ynlPL3mKboIv2', 5, '', '158', '0', NULL, '1', NULL, NULL, '2022-03-28 00:09:45', 1, '2022-03-22 16:57:59', 1, '2022-03-28 00:10:59', 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `role_id` bigint(0) NOT NULL COMMENT '角色ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (2, 1);

SET FOREIGN_KEY_CHECKS = 1;
