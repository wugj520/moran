package mobai.moran.common.web;

import lombok.extern.slf4j.Slf4j;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.ObjectUtil;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

/**
 * 全局错误拦截
 * @author mobai
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResultData errorHandler(Exception e, HttpServletResponse response) {
        log.error(e.getMessage(), e);
        ResultData r = new ResultData();
        r.setCode(501);
        if (e instanceof AccessDeniedException){
            r.setCode(403);
            r.setMessage("不允许访问");
        }else if (e instanceof BindException){
            if (ObjectUtil.isNotEmpty(((BindException)e).getGlobalError())){
                r.setMessage(((BindException)e).getGlobalError().getDefaultMessage());
            }else {
                r.setMessage("参数校验失败！！");
            }
        }else if (e instanceof ServiceException){
            r.setMessage(((ServiceException) e).tip);
        }else {
            r.setMessage("操作失败，请与管理员联系!!");
        }
        // 支持跨域
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, x-requested-with, X-Custom-Header, Authorization");
        return r;
    }
}
