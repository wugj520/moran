package mobai.moran.common.web;

import mobai.moran.common.exception.ServiceException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * spring参数注入对字符串转时间的管理
 * @author mobai
 */
@Configuration
public class DateConfig {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_SHORT_DATE_FORMAT = "yyyy-MM-dd";
    private static final String DEFAULT_DATE_FORMAT2 = "yyyy/MM/dd HH:mm:ss";
    private static final String DEFAULT_SHORT_DATE_FORMAT2 = "yyyy/MM/dd";

    @Bean
    public Converter<String, Date> localDateConverter() {
        // 此处不能使用Lambda表达式， Lambda表达式的接口是Converter<?, ?>，不能的到具体的类型
        return new Converter<String, Date>() {
            @Override
            public Date convert(String source) {
                if ("".equals(source.trim())) {
                    return null;
                }
                source = source.trim();
                SimpleDateFormat formatter = new SimpleDateFormat();
                if (source.contains("-")) {
                    if (source.contains(":")) {
                        formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
                    } else {
                        formatter = new SimpleDateFormat(DEFAULT_SHORT_DATE_FORMAT);
                    }
                } else if (source.contains("/")) {
                    if (source.contains(":")) {
                        formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT2);
                    } else {
                        formatter = new SimpleDateFormat(DEFAULT_SHORT_DATE_FORMAT2);
                    }
                }
                try {
                    return formatter.parse(source.replaceAll("T"," "));
                } catch (ParseException e) {
                    throw new ServiceException(String.format("parser %s to Date fail", source));
                }
            }
        };
    }
}
