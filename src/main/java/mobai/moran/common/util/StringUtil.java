package mobai.moran.common.util;

/**
 * 字符串工具类
 * @author mobai
 */
public class StringUtil extends ObjectUtil{

    public static final String EMPTY_JSON = "{}";
    public static final char C_BACKSLASH = '\\';
    public static final char C_DELIM_START = '{';
    public static final char C_DELIM_END = '}';
    private static final String NULL_STR = "";
    private static final char SEPARATOR = '_';

    /** 包含数字正则 */
    public static final String NUMBER_REX = ".*\\d+.*";
    /** 包含大写字母 */
    public static final String UPPERCASE_REX = ".*[A-Z]+.*";
    /** 包含小写字母*/
    public static final String LOWERCASE_REX = ".*[a-z]+.*";
    /** 包含特殊符号 */
    public static final String SYMBOL_REX = ".*[~!@#$%^&*()_+|<>,.?/:;'\\[\\]{}\"]+.*";
    /** 包含中文 */
    public static final String CHINESE_REX = ".[\\u4e00-\\u9fa5]+.";

    /** 包含空格 */
    public static final String EMPTY_STR_REX = ".*[\\s\\p{Zs}]+.*";

    /**
     * 字串是否包含前缀
     * @param str       字符串
     * @param prefix    前缀
     * @return  字符串是否包含前缀
     */
    public static boolean hasPrefix(String str,String prefix){
        if (isNotEmpty(str) && isNotNull(prefix)){
            return str.toLowerCase().matches("^"+prefix.toLowerCase()+".*");
        }
        return false;
    }
    /**
     * 去除字符串指定的前缀
     * @param str       字符串
     * @param prefix    指定前缀
     */
    public static String removePrefix(String str,String prefix){
        if (hasPrefix(str,prefix)){
            return str.substring(prefix.length());
        }
        return ifEmpty(str,"");
    }

    /**
     * 截取字符串
     *
     * @param str 字符串
     * @param start 开始
     * @param end 结束
     * @return 结果
     */
    public static String substring(final String str, int start, int end) {
        if (str == null) { return NULL_STR; }

        if (end < 0) { end = str.length() + end; }
        if (start < 0) { start = str.length() + start; }

        if (end > str.length()) { end = str.length(); }

        if (start > end) { return NULL_STR; }

        if (start < 0) { start = 0; }
        if (end < 0) { end = 0; }

        return str.substring(start, end);
    }

    /**
     * 格式化字符串<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：format("this is {} for {}", "a", "b") -> this is a for b<br>
     * 转义{}： format("this is \\{} for {}", "a", "b") -> this is \{} for a<br>
     * 转义\： format("this is \\\\{} for {}", "a", "b") -> this is \a for b<br>
     *
     * @param template 字符串模板
     * @param params 参数列表
     * @return 结果
     */
    public static String format(String template, Object... params) {
        if (hasEmpty(template,params)){
            return template;
        }
        int templateLength = template.length();
        // 初始化定义好的长度以获得更好的性能
        StringBuilder sb = new StringBuilder(templateLength + 50);

        int handledPosition = 0;
        // 占位符所在位置
        int delimIndex;

        for (int argIndex = 0; argIndex < params.length; argIndex++) {
            delimIndex = template.indexOf(EMPTY_JSON, handledPosition);
            if (delimIndex == -1) {
                if (handledPosition == 0) {
                    return template;
                } else {
                    // 字符串模板剩余部分不再包含占位符，加入剩余部分后返回结果
                    sb.append(template, handledPosition, templateLength);
                    return sb.toString();
                }
            }
            else {
                if (delimIndex > 0 && template.charAt(delimIndex - 1) == C_BACKSLASH) {
                    if (delimIndex > 1 && template.charAt(delimIndex - 2) == C_BACKSLASH) {
                        // 转义符之前还有一个转义符，占位符依旧有效
                        sb.append(template, handledPosition, delimIndex - 1);
                        sb.append(params[argIndex].toString());
                        handledPosition = delimIndex + 2;
                    } else {
                        // 占位符被转义
                        argIndex--;
                        sb.append(template, handledPosition, delimIndex - 1);
                        sb.append(C_DELIM_START);
                        handledPosition = delimIndex + 1;
                    }
                } else {
                    // 正常占位符
                    sb.append(template, handledPosition, delimIndex);
                    sb.append(params[argIndex].toString());
                    handledPosition = delimIndex + 2;
                }
            }
        }

        // 加入最后一个占位符后所有的字符
        sb.append(template, handledPosition,templateLength);
        return sb.toString();
    }

}
