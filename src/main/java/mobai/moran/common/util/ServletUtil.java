package mobai.moran.common.util;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import mobai.moran.common.exception.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 保存Http请求的上下文，在任何地方快速获取HttpServletRequest和HttpServletResponse
 * @author mobai
 */
@Slf4j
public class ServletUtil {

    /** 本机ip地址 */
    private static final String LOCAL_IP = "127.0.0.1";

    /** 本机ip地址的ipv6地址 */
    private static final String LOCAL_REMOTE_HOST = "0:0:0:0:0:0:0:1";

    /** IP地理位置查询 */
    public static final String IP_URL = "http://whois.pconline.com.cn/ipJson.jsp";

    private static final String UNKNOWN = "unknown";

    /**
     * 验证必要请求参数是否包含空
     * @param es 验证字段字符串，用逗号分割
     * @return 含空为true
     */
    public static boolean paramsHasEmpty(String es) {
        return paramsHasEmpty(getRequest(),es);
    }
    /**
     * 验证必要请求参数是否为空
     * @param req 请求
     * @param es      验证字段字符串，用逗号分割
     * @return 含空为true
     */
    public static boolean paramsHasEmpty(HttpServletRequest req, String es) {
        if (es != null && !"".equals(es.trim())) {
            String[] strArr = es.split(",");
            for (String str : strArr) {
                if ("".equals(str.trim()) || "".equals(req.getParameter(str).trim())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 请求参数装成字符串
     */
    public static String paramsToJsonStr(HttpServletRequest req) {
        JSONObject json = new JSONObject();
        Enumeration<String> e = req.getParameterNames();
        while (e.hasMoreElements()) {
            String name = e.nextElement();
            String value = req.getParameter(name);
            //空数据就不存，避免查询参数报错
            if (value == null || "".equals(value.trim())) {
                continue;
            }
            json.put(name,value);
        }
        return json.toJSONString();
    }

    /**
     * 获取请求参数，不会获取空参
     */
    public static Map<String, Object> getParams() {
        return getParams(getRequest());
    }

    /**
     * 获取请求参数，不会获取空参
     * @param req 请求
     */
    public static Map<String, Object> getParams(HttpServletRequest req) {
        Map<String, Object> params = new HashMap<>();
        Enumeration<String> e = req.getParameterNames();
        while (e.hasMoreElements()) {
            String name = e.nextElement();
            String value = req.getParameter(name);
            //空数据就不存，避免查询参数报错
            if (value == null || "".equals(value.trim())) {
                continue;
            }
            params.put(name, value);
        }
        return params;
    }

    /** 获取所有请求参数 */
    public static Map<String, Object> getAllParams() {
        return getAllParams(getRequest());
    }

    /**
     * 获取所有请求参数
     * @param req 请求
     */
    public static Map<String, Object> getAllParams(HttpServletRequest req) {
        Map<String, Object> params = new HashMap<>();
        Enumeration<String> e = req.getParameterNames();
        while (e.hasMoreElements()) {
            String name = e.nextElement();
            String value = req.getParameter(name);
            params.put(name, value);
        }
        return params;
    }

    /** 根据参数名称获取请求参数 */
    public static String getParameter(String name) {
        return getRequest().getParameter(name);
    }

    /** 获取当前请求的request对象 */
    public static HttpServletRequest getRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            throw new ServiceException("获取请求失败-request");
        } else {
            return requestAttributes.getRequest();
        }
    }

    /** 获取当前请求的response对象 */
    public static HttpServletResponse getResponse() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            throw new ServiceException("获取请求失败-response");
        } else {
            return requestAttributes.getResponse();
        }
    }

    public static String getRequestIp() {
        return getRequestIp(getRequest());
    }

    /** 获取请求ip */
    public static String getRequestIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Forwarded-For");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip.replaceAll("(<[^<]*?>)|(<[\\s]*?/[^<]*?>)|(<[^<]*?/[\\s]*?>)", "");
    }

    /**
     * 获取请求所属位置
     * @return 位置
     */
    public static String getRequestLocation() {
        String ip = getRequestIp();
        return getLocationByIp(ip);
    }

    /**
     * 获取请求所属位置
     * @return 位置
     */
    public static String getRequestLocation(HttpServletRequest request) {
        String ip = getRequestIp(request);
        return getLocationByIp(ip);
    }

    /** 根据IP获取地理位置 */
    public static String getLocationByIp(String ip) {
        // 如果获取不到，返回 "-"
        String resultJson = "-";
        if (LOCAL_IP.equals(ip)){
            return "内网IP";
        }
        try {
            String rspStr = HttpUtil.sendPost(IP_URL,"ip=" + ip + "&json=true");

            if (!"".equals(rspStr.trim())){
                JSONObject obj = JSONObject.parseObject(rspStr);
                return obj.getString("addr");
            }
        }catch (Exception e) {
            log.error("获取地理位置异常 {}", ip);
        }
        return resultJson;
    }

    /**
     * 将字符串渲染到客户端
     * @param response 渲染对象
     * @param string   待渲染的字符串
     */
    public static void renderString(HttpServletResponse response, String string) throws IOException {
        response.setStatus(HttpStatus.OK.value());
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().print(string);
    }

    /**
     * 是否是Ajax异步请求
     *
     * @param request 请求信息
     */
    public static boolean isAjaxRequest(HttpServletRequest request) {
        String accept = request.getHeader("accept");
        if (accept != null && accept.contains("application/json")) {
            return true;
        }
        String xRequestedWith = request.getHeader("X-Requested-With");
        if (xRequestedWith != null && xRequestedWith.contains("XMLHttpRequest")) {
            return true;
        }
        String uri = request.getRequestURI();
        if (".json".equalsIgnoreCase(uri) || ".xml".equalsIgnoreCase(uri)){
            return true;
        }
        String ajax = request.getParameter("__ajax");
        return "json".equalsIgnoreCase(ajax) || "xml".equalsIgnoreCase(ajax);
    }
}
