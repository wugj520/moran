package mobai.moran.common.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * map 数据的树工具
 * @author mobai
 */
public class TreeUtil {

    static final String PARENT_ID = "parentId";

    /**
     * 自动获取顶级节点来构建树
     * @param list mapList
     * @param idName id字段名
     * @return 树型Map列表
     */
    public static List<Map<String, Object>> getChild(List<Map<String, Object>> list, String idName) {
        if (ObjectUtil.isEmpty(list)){
            return list;
        }
        List<String> ids = new ArrayList<>();
        for (Map<String, Object> obj : list) {
            if (!obj.isEmpty()){
                ids.add(obj.get(idName).toString());
            }
        }
        List<Map<String, Object>> childList = new ArrayList<>();
        Iterator<Map<String, Object>> it = list.iterator();
        while (it.hasNext()) {
            Map<String, Object> obj = it.next();
            // 如果父节点不在查询的数据中，那么默认为顶级节点
            if (!ids.contains(obj.get(PARENT_ID).toString())) {
                childList.add(obj);
                //顶级节点可以删除，减少递归量
                it.remove();
            }
        }
        return getChildList(list, idName, childList);
    }

    /**
     * 根据父节点ID构建树
     * @param list     对象list
     * @param parentId 父ID
     * @param idName   主键ID 的字段名，用于递归做父ID
     */
    public static List<Map<String, Object>> getChild(List<Map<String, Object>> list, String parentId, String idName) {
        if (ObjectUtil.isEmpty(list)){
            return list;
        }
        List<Map<String, Object>> childList = new ArrayList<>();
        Iterator<Map<String, Object>> it = list.iterator();
        while (it.hasNext()) {
            Map<String, Object> obj = it.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点 , 避免递归报错此处不查询子节点的子节点
            if (parentId.equals(obj.get(PARENT_ID).toString())) {
                childList.add(obj);
                //顶级节点可以删除，减少递归量
                it.remove();
            }
        }
        return getChildList(list, idName, childList);
    }

    /**
     * 遍历父节点,获取子节点
     * @param list       源数据
     * @param idName     id字段名称
     * @param parentList 父节点数据数组
     */
    private static List<Map<String, Object>> getChildList(List<Map<String, Object>> list, String idName, List<Map<String, Object>> parentList) {
        for (Map<String, Object> obj : parentList) {
            List<Map<String, Object>> child = getChild(list, obj.get(idName).toString(), idName);
            if (!child.isEmpty()) {
                obj.put("children", child);
                obj.put("child", child);
            }
        }
        return parentList.size() == 0 ? new ArrayList<>() : parentList;
    }
}
