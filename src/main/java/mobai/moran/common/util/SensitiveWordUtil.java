package mobai.moran.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 数据脱敏工具类
 * @author mobai
 */
public class SensitiveWordUtil {

    static final String PHONE = "(13[0-9]|14[5|7]|15[0-9]|18[0-9]|19[4|5])\\d{8}";
    static final String TEL = "\\d{3}-\\d{8}|\\d{4}-\\d{7,8}";
    static final String IDS = "(\\d{15})|(\\d{18})|(\\d{17}(\\d|X|x))";
    static final String ADDRESS = "([0-9０-９a-zA-Z一二三四五六七八九十壹贰叁肆伍陆柒捌玖拾]+\\s*[栋号楼期F组])|([0-9０-９a-zA-Z一二三四五六七八九十壹贰叁肆伍陆柒捌玖拾]+\\s*单元)";


    public static Object phone(Object src){
        return sensitive(src,PHONE,String.format("1%10s","").replace(" ","*"));
    }
    public static Object tel(Object src){
        return sensitive(src,TEL);
    }
    public static Object ids(Object src){
        return sensitive(src,IDS);
    }
    public static Object address(Object src){return sensitive(src,ADDRESS);}

    /**
     * 数据脱敏
     * @param src       数据
     * @param reg       需要脱敏的数据正则
     * @param str2      需要替换成的字符串
     * @return  返回替换好的字符串obj
     */
    public static Object sensitive(Object src,String reg,String str2) {
        if (src == null) return null;
        StringBuffer str=new StringBuffer(src.toString());
        // 预编译
        Pattern patten = Pattern.compile(reg);
        // 进行匹配
        Matcher matcher = patten.matcher(src.toString());
        //matcher.replaceAll("*");
        while(matcher.find()) {
            str.replace(matcher.start(),matcher.end(),str2);
            matcher = patten.matcher(str);
        }
        return str;
    }

    /**
     * 数据脱敏
     * @param src   数据
     * @param reg       需要脱敏的数据正则
     * @return  返回替换好的字符串obj
     */
    public static Object sensitive(Object src,String reg) {
        if (src == null) return null;
        StringBuffer str=new StringBuffer(src.toString());
        // 预编译
        Pattern patten = Pattern.compile(reg);
        // 进行匹配
        Matcher matcher = patten.matcher(src.toString());
        while(matcher.find()) {
            str.replace(matcher.start(),matcher.end(),String.format("%"+(matcher.end()-matcher.start())+"s","").replace(" ","*"));
            matcher = patten.matcher(str);
        }
        return str;
    }

    public static void main(String[] args) {
        String str = "吴15812345678以为020-12345678地址12单元，123号";
        System.out.println(address(str));
    }
}
