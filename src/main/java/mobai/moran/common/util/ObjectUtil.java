package mobai.moran.common.util;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author mobai
 */
@Slf4j
public class ObjectUtil {

    /** list克隆 */
    public static <T> List<T> deepCopy(List<T> src){
        List<T> dest = null;
        try {
            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(byteOut);
            out.writeObject(src);
            ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
            ObjectInputStream in = new ObjectInputStream(byteIn);
            dest = (List<T>) in.readObject();
        }catch (Exception e){
            log.error("list克隆失败：",e);
        }
        return dest;
    }

    @SuppressWarnings("unchecked")
    public static <T> T cast(Object obj) {
        return (T) obj;
    }

    /**
     * 判断对象A有没有包含对象B
     * @param obj 对象A
     * @param element 对象B
     */
    public static boolean contains(Object obj, Object element) {
        if (obj == null) {
            return false;
        } else if (obj instanceof String) {
            return element != null && ((String) obj).contains(element.toString());
        } else if (obj instanceof Collection) {
            return ((Collection<?>)obj).contains(element);
        } else if (obj instanceof Map) {
            return ((Map<?, ?>)obj).containsValue(element);
        } else {
            Object o;
            if (obj instanceof Iterator) {
                do {
                    Iterator iter = (Iterator) obj;
                    if (!iter.hasNext()) {
                        return false;
                    }
                    o = iter.next();
                } while(!equal(o, element));
                return true;
            } else if (obj instanceof Enumeration) {
                do {
                    Enumeration enumeration = (Enumeration) obj;
                    if (!enumeration.hasMoreElements()) {
                        return false;
                    }
                    o = enumeration.nextElement();
                } while(!equal(o, element));
                return true;
            } else {
                if (obj.getClass().isArray()) {
                    for(int i = 0; i < Array.getLength(obj); ++i) {
                        if (equal(Array.get(obj, i), element)) {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
    }

    /**
     * 比较两个对象是否相等。<br>
     * 相同的条件有两个，满足其一即可：<br>
     * <ol>
     * <li>obj1 == null &amp;&amp; obj2 == null</li>
     * <li>obj1.equals(obj2)</li>
     * </ol>
     *
     * @param obj1 参数1
     * @param obj2 参数2
     */
    public static boolean equal(Object obj1, Object obj2) {
        if (obj1 == null && obj2 == null) {
            return true;
        }
        if (obj1 instanceof BigDecimal && obj2 instanceof BigDecimal) {
            BigDecimal bigNum1 = (BigDecimal) obj1;
            return 0 == bigNum1.compareTo((BigDecimal) obj2);
        }
        return Objects.equals(obj1, obj2);
    }

    /**
     * 对象不能为null
     *
     * @param object  对象
     * @param message 提示信息
     * @return 如果 <code>object</code> 是null,抛出 {@link NullPointerException}<br>
     */
    public static <T> T notNull(T object, String message, Object... values) {
        return Objects.requireNonNull(object, () -> String.format(message, values));
    }

    /**
     * 字符对象不能为 {@code null}、{@code ""}、空格、全角空格、制表符、换行符，等不可见字符
     *
     * @param chars   字符对象
     * @param message 提示信息模板
     * @param values  信息模板填充参数
     * @return 如果 <code>chars</code> 是null,抛出 {@link NullPointerException}<br>
     * 如果 <code>chars</code> 是blank,抛出 {@link IllegalArgumentException}<br>
     */
    public static <T extends CharSequence> T notBlank(T chars, String message, Object... values) {
        Objects.requireNonNull(chars, () -> String.format(message, values));
        if (isBlank(chars)) {
            throw new IllegalArgumentException(String.format(message, values));
        }
        return chars;
    }

    /**
     * 判断字符串是否包含空白
     * @param objs 字符数组
     */
    public static boolean hasBlank(CharSequence... objs) {
        if (isNotEmpty(objs)) {
            for (CharSequence obj : objs) {
                if (isBlank(obj)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isNotBlank(CharSequence cs){
        return !isBlank(cs);
    }

    /**
     * <p>字符序列对象是否为空白，空白的定义如下：</p>
     * <ol>
     * 	   <li>{@code null}</li>
     * 	   <li>空字符串：{@code ""}</li>
     *     <li>空格、全角空格、制表符、换行符，等不可见字符</li>
     * </ol>
     *
     * @param cs 字符序列对象
     */
    public static boolean isBlank(CharSequence cs) {
        int strLen = isNull(cs) ? 0 : cs.length();
        if (strLen == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断对象数组是否含空
     * @param objs 对象数组
     */
    public static boolean hasEmpty(Object... objs) {
        if (isNotEmpty(objs)) {
            for (Object obj : objs) {
                if (isEmpty(obj)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断对象是否为空， 如果空 返回默认值
     * @param object       对象
     * @param defaultValue 默认对象
     */
    public static <T> T ifEmpty(T object, T defaultValue) {
        return isEmpty(object) ? defaultValue : object;
    }

    /**
     * 判断对象是否不为empty,判断类型：字符串、Collection、Map、数组、迭代器
     */
    public static boolean isNotEmpty(Object value) {
        return !isEmpty(value);
    }

    /**
     * 判断对象是否为empty,判断类型：字符串、Collection、Map、数组、迭代器、枚举
     */
    public static boolean isEmpty(Object obj) {
        //只要是null 必然是empty
        if (isNull(obj)) {
            return true;
        }
        //判断是否常见类型，是则调用对象判空方法
        if (obj instanceof CharSequence) {
            return ((CharSequence) obj).length() == 0;
        } else if (obj instanceof Collection<?>) {
            //判断是不是 Collection 或 Map , Collection 包含List，Set，Queue
            return ((Collection<?>) obj).isEmpty();
        } else if (obj instanceof Map<?, ?>) {
            return ((Map<?, ?>) obj).isEmpty();
        } else if (obj instanceof Object[]) {
            return ((Object[]) obj).length == 0;
        } else if (obj instanceof Iterator<?>) {
            return !((Iterator<?>) obj).hasNext();
        } else if (obj instanceof Enumeration<?>) {
            return !((Enumeration<?>) obj).hasMoreElements();
        }
        return false;
    }

    /**
     * 判断对象数组是否含NULL
     * @param objs 对象数组
     */
    public static boolean hasNull(Object... objs) {
        if (isNotNull(objs)) {
            for (Object obj : objs) {
                if (isNull(obj)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断对象是否为null， 如果是null 返回默认值
     * @param obj          对象
     * @param defaultValue 默认值
     */
    public static <T> T ifNull(T obj, T defaultValue) {
        return obj != null ? obj : defaultValue;
    }

    /**
     * 判断对象value 不是null
     */
    public static boolean isNotNull(Object obj) {
        return !isNull(obj);
    }

    /**
     * 判断对象 是null
     */
    public static boolean isNull(Object obj) {
        return null == obj;
    }

    /**
     * 是否是数组
     */
    public static boolean isArray(Object obj) {return isNotNull(obj) && obj.getClass().isArray();}

}
