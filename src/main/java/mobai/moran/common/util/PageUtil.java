package mobai.moran.common.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import mobai.moran.common.base.vo.PageData;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 分页工具
 * @author mobai
 */
public class PageUtil {

    /** 分页大小的字段命名 */
    private static final String PAGE_SIZE_PARAM_NAME = "pageSize";
    /** 页数的字段命名 */
    private static final String PAGE_NO_PARAM_NAME = "pageNo";

    /**
     * 从请求中获取分页数据，默认页面大小10
     */
    public static <T> Page<T> defaultPage() {
        int pageSize = 10;
        int pageNo = 1;

        HttpServletRequest request = ServletUtil.getRequest();

        //每页条数
        String pageSizeString = request.getParameter(PAGE_SIZE_PARAM_NAME);
        if (ObjectUtil.isNotEmpty(pageSizeString)) {
            pageSize = Integer.parseInt(pageSizeString);
        }
        //第几页
        String pageNoString = request.getParameter(PAGE_NO_PARAM_NAME);
        if (ObjectUtil.isNotEmpty(pageNoString)) {
            pageNo = Integer.parseInt(pageNoString);
        }

        return new Page<>(pageNo, pageSize);
    }

    public static int getPageSize() {
        //每页条数
        String pageSizeString = ServletUtil.getRequest().getParameter(PAGE_SIZE_PARAM_NAME);
        if (ObjectUtil.isNotEmpty(pageSizeString)) {
            return Integer.parseInt(pageSizeString);
        }
        return 10;
    }

    /**
     * 将mybatis-plus的page转成自定义的PageResult，扩展了totalPage总页数
     */
    public static PageData createPageResult(IPage<?> page) {
        PageData pageResult = new PageData();
        pageResult.setCode(0);
        pageResult.setCount(page.getTotal());
        pageResult.setData(page.getRecords());
        return pageResult;
    }

    /**
     * 将mybatis-plus的page转成自定义的PageResult，扩展了totalPage总页数
     */
    public static PageData createPageResult(List<?> list) {
        PageData pageResult = new PageData();
        pageResult.setCode(0);
        pageResult.setCount(list.size());
        pageResult.setData(list);
        return pageResult;
    }
}
