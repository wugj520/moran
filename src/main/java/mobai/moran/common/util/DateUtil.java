package mobai.moran.common.util;

import java.util.Calendar;

/**
 * @author mobai
 */
public class DateUtil {

    public static int getNowPlan(){
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR)*100+cal.get(Calendar.MONTH)+1;
    }
}
