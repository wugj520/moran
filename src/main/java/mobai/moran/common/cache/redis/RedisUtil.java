package mobai.moran.common.cache.redis;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 *  redis 工具类
 * @author mobai
 */
@Component
public class RedisUtil {

    public static RedisTemplate<String,Object> redisTemplate;

    /**
     * 静态对象注入，需要交给spring管理才能自动注入，所以类上加上@Component注解
     * @param redisTemplate redisTemplate
     */
    @Resource
    public void setRedisTemplate(RedisTemplate<String,Object>  redisTemplate){
        RedisUtil.redisTemplate = redisTemplate;
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key 缓存的键值
     * @param val 缓存的值
     */
    public static void put(String key,Object val){
        redisTemplate.opsForValue().set(key,val);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key 缓存的键值
     * @param val 缓存的值
     * @param timeout 时间
     * @param timeUnit 时间颗粒度
     */
    public static void put(String key, Object val, long timeout, TimeUnit timeUnit){
        redisTemplate.opsForValue().set(key, val, timeout, timeUnit);
    }

    /**
     * 设置有效时间,默认秒
     *
     * @param key Redis键
     * @param timeout 超时时间
     * @return true=设置成功；false=设置失败
     */
    public static boolean expire(String key,long timeout){
        return expire(key, timeout,TimeUnit.SECONDS);
    }

    /**
     * 设置有效时间
     *
     * @param key Redis键
     * @param timeout 超时时间
     * @param unit 时间单位
     * @return true=设置成功；false=设置失败
     */
    public static boolean expire(String key,long timeout,TimeUnit unit){
        return Boolean.TRUE.equals(redisTemplate.expire(key, timeout, unit));
    }

    /**
     * 获得缓存的基本对象。
     *
     * @param key 缓存标识
     * @return 缓存标识对应的数据
     */
    public static Object get(String key){
        return redisTemplate.opsForValue().get(key);
    }

    public static boolean hasKey(String key) { return Boolean.TRUE.equals(redisTemplate.hasKey(key));}

    /**
     * 通过标识移除缓存
     * @param key   标识
     */
    public static void remove(String key){
        redisTemplate.delete(key);
    }

    /**
     * 获取所有标识
     * @param prefix    前缀
     * @return          包含指定前缀的标识列表
     */
    public static Collection<String> keys(String prefix){
        return redisTemplate.keys(prefix);
    }
}
