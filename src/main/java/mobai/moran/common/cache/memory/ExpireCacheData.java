package mobai.moran.common.cache.memory;

import java.util.Date;

/**
 * 缓存数据。存入数据的时候把存入的实际数据增加一个外包装，顺便加上存入时间，和过期时间
 * @author mobai
 */
public class ExpireCacheData<T> {

    /**
     * 缓存的数据，可以是任意类型的对象
     */
    public T data;

    /**
     * 过期时间 小于等于0 标识永久存活
     */
    public long expire;

    /**
     * 存活时间
     */
    public Date expireTime;

    /**
     * 创建缓存数据,不会过期
     * @param t      缓存的数据，可以是任意类型的对象
     */
    ExpireCacheData(T t) {
        this.data = t;
        this.expire = 0;
        this.expireTime = new Date(System.currentTimeMillis() + this.expire);
    }

    /**
     * 创建缓存数据
     * @param t      缓存的数据，可以是任意类型的对象
     * @param expire 过期时间，单位是秒
     */
    ExpireCacheData(T t, long expire) {
        this.data = t;
        this.expire = expire <= 0 ? 0 : expire;
        this.expireTime = new Date(System.currentTimeMillis() + this.expire);
    }

}
