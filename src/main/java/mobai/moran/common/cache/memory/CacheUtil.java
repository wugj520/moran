package mobai.moran.common.cache.memory;

import mobai.moran.common.util.ObjectUtil;
import mobai.moran.common.util.StringUtil;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 内存缓存工具
 * <p>使用map做缓存管理，拿来存储字典和参数</p>
 * @author mobai
 */
public class CacheUtil {

    private static final Map<String,ExpireCacheData<Object>> cache = new HashMap<>();

    static {
        // 周期性执行任务,10秒一次
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                for (String key : cache.keySet()) {
                    ExpireCacheData<Object> obj = cache.get(key);
                    // 数据为空，或者已过期
                    if (ObjectUtil.isEmpty(obj) || (obj.expire>0 && obj.expireTime.before(new Date()))){
                        cache.remove(key);
                    }
                }
            }
        },0, 10 * 1000);
    }

    /**
     * 添加缓存
     * @param key   标识
     * @param val   值
     */
    public static void put(String key,Object val){
        ExpireCacheData<Object> obj = cache.get(key);
        if (obj != null && (obj.expire <= 0 || obj.expireTime.after(new Date()))){
            obj.data = val;
            cache.put(key,obj);
        }else {
            cache.put(key,new ExpireCacheData<>(val));
        }
    }

    /**
     * 添加缓存
     * @param key   标识
     * @param val   值
     * @param timeout   过期时间（毫秒数）
     */
    public static void put(String key, Object val, long timeout){
        cache.put(key,new ExpireCacheData<>(val,timeout));
    }

    /**
     * 设置过期时间
     * @param key   标识
     * @param timeout   过期时间（毫秒数）
     */
    public static boolean expire(String key,long timeout){
        Object obj = get(key);
        if (ObjectUtil.isNotEmpty(obj)){
            put(key,obj,timeout);
            return true;
        }
        return false;
    }

    /**
     * 通过标识获取缓存对象
     * @param key   标识
     * @return      缓存对象
     */
    public static Object get(String key){
        ExpireCacheData<Object> obj = cache.get(key);
        if (ObjectUtil.isNotEmpty(obj) && (obj.expire <= 0 || obj.expireTime.after(new Date()))){
            return obj.data;
        }
        return null;
    }

    /**
     * 通过标识移除缓存
     * @param key   标识
     */
    public static void remove(String key){
        cache.remove(key);
    }

    /**
     * 通过标识前缀移除缓存
     * @param prefix    前缀
     */
    public static void removeByPrefix(String prefix){
        Set<String> keys = (Set<String>) keys(prefix);
        if (ObjectUtil.isNotEmpty(keys)){
            for (String key : keys){
                cache.remove(key);
            }
        }
    }

    /**
     * 获取所有标识
     * @return  标识列表
     */
    public static Collection<String> keys(){
        return cache.keySet();
    }

    /**
     * 通过前缀获取标识列表
     * @param prefix    前缀
     * @return          包含指定前缀的标识列表
     */
    public static Collection<String> keys(String prefix){
        Set<String> keys = cache.keySet();
        if (ObjectUtil.isEmpty(keys)){
            return new HashSet<>();
        }
        keys = keys.stream().filter(key -> StringUtil.hasPrefix(key,prefix)).collect(Collectors.toSet());
        return keys;
    }
}
