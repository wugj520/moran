package mobai.moran.common.file;

import mobai.moran.common.constant.Constants;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.system.entity.SysFile;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.UUID;

/**
 * 文件工具
 * @author mobai
 */
public class FileUtil {

    /**
     * 获取文件后缀名
     */
    public String getFileSuffix(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    /**
     * 创建文件对象
     * @return
     */
    public static SysFile buildFileObj(MultipartFile file){
        //判断文件是否为空
        if (file.isEmpty()) {
            throw new ServiceException("文件不能为空");
        }
        //判断文件后缀名是否合法
        int dotPos = file.getOriginalFilename().lastIndexOf(Constants.SUFFIX_SPLIT);
        if (dotPos < 0) {
            throw new ServiceException("文件名称不合法");
        }
        //文件名
        String orgName = file.getOriginalFilename();
        String name = orgName.substring(0, orgName.lastIndexOf(Constants.SUFFIX_SPLIT));
        //文件后缀名
        String fileExt = orgName.substring(dotPos + 1).toLowerCase();
        // 判断是否是合法的文件后缀
        if (!MimeTypeUtil.isFileAllowed(fileExt)) {
            throw new ServiceException("文类类型不符合要求");
        }
        //生成新的文件名
        String fileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + fileExt;
        SysFile sf = new SysFile();
        sf.setFileName(fileName);
        sf.setFileInitName(name);
        sf.setFileSuffix(fileExt);
        sf.setFileType(fileExt);
        sf.setFileSize(file.getSize());
        sf.setPutTime(new Date());
        return sf;
    }

    public static void downLoad(String url,String path, HttpServletResponse response) {
        InputStream in = null;
        try {
            URL httpUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) httpUrl.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(100000);
            conn.setReadTimeout(200000);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.connect();
            in = conn.getInputStream();
            byte[] bs = new byte[1024];
            int len = 0;
            response.reset();
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setContentType("application/octet-stream");
            String fileName = url.replaceAll(path + "/", "");
            response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
            ServletOutputStream out = response.getOutputStream();
            while ((len = in.read(bs)) != -1) {
                out.write(bs, 0, len);
            }
            out.flush();
            out.close();
        } catch (Exception e) {
            throw new RuntimeException(url + "下载失败");
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
