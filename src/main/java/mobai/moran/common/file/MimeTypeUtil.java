package mobai.moran.common.file;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mobai
 */
public class MimeTypeUtil {

    private static Map<String,String> map = new HashMap<>();

    public static String[] IMAGE_FILE_EXTEND = new String[]{
            "png", "bmp", "jpg", "jpeg", "gif", "svg", "ico",
            "xls", "xlsx", "doc", "docx", "txt", "pdf", "ppt", "pptx",
            "zip", "gz", "mp3", "mp4", "avi", "wav",
            "html", "htm", "java","sql", "xml","js","py","php","vue","sh","cmd","py3","css"
    };

    static{
        map.put("image/png",".png" );
        map.put("image/bmp",".bmp" );
        map.put("image/jpeg",".jpg");
        map.put("image/gif",".gif" );
        map.put("application/x-shockwave-flash",".svg");

        map.put("application/vnd.ms-excel",".xls" );
        map.put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",".xlsx" );
        map.put("application/msword",".doc" );
        map.put("application/vnd.openxmlformats-officedocument.wordprocessingml.document",".docx" );
        map.put("text/plain",".txt");
        map.put("application/pdf",".pdf" );
        map.put("application/vnd.ms-powerpoint",".ppt" );
        map.put("application/vnd.openxmlformats-officedocument.presentationml.presentation",".pptx" );

        map.put("application/zip",".zip" );
        map.put("application/x-zip-compressed",".zip" );
        map.put("multipart/x-zip",".zip" );
        map.put("application/x-compressed",".zip" );
        map.put("audio/mpeg3",".mp3" );
        map.put("video/avi",".avi" );
        map.put("audio/wav",".wav" );
        map.put("application/x-gzip",".gz");
        map.put("text/html",".html");
    }

    public static String getFileExtension(String mimeType){
        return map.get(mimeType);
    }

    public static String getContentType(String filenameExtension) {
        if (filenameExtension.equalsIgnoreCase(".pdf")) {
            return "application/pdf";
        }
        if (filenameExtension.equalsIgnoreCase(".bmp")) {
            return "image/bmp";
        }
        if (filenameExtension.equalsIgnoreCase(".gif")) {
            return "image/gif";
        }
        if (filenameExtension.equalsIgnoreCase(".jpeg") ||
                filenameExtension.equalsIgnoreCase(".jpg") ||
                filenameExtension.equalsIgnoreCase(".png")) {
            return "image/jpg";
        }
        if (filenameExtension.equalsIgnoreCase(".html")) {
            return "text/html";
        }
        if (filenameExtension.equalsIgnoreCase(".txt")) {
            return "text/plain";
        }
        if (filenameExtension.equalsIgnoreCase(".vsd")) {
            return "application/vnd.visio";
        }
        if (filenameExtension.equalsIgnoreCase(".pptx") ||
                filenameExtension.equalsIgnoreCase(".ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (filenameExtension.equalsIgnoreCase(".docx"))
        {
            return "application/msword";
        }
        if (filenameExtension.equalsIgnoreCase(".xml")) {
            return "text/xml";
        }
        return "image/jpg";
    }

    public static boolean isFileAllowed(String fileSuffix) {
        for (String ext : IMAGE_FILE_EXTEND) {
            if (ext.equals(fileSuffix)) {
                return true;
            }
        }
        return false;
    }
}
