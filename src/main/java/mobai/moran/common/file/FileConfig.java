package mobai.moran.common.file;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件存储配置
 * @author mobai
 */
@Configuration
@ConfigurationProperties(prefix = "fc")
public class FileConfig {

    /** 存储类型嘛 */
    private static String type;

    private static Map<String,String> local = new HashMap<>();

    private static Map<String,String> qiniu = new HashMap<>();

    private static Map<String,String> ali = new HashMap<>();

    public static String getType() {
        return type;
    }

    public static void setType(String type) {
        FileConfig.type = type;
    }

    public static Map<String, String> getLocal() {
        return local;
    }

    public static void setLocal(Map<String, String> local) {
        FileConfig.local = local;
    }

    public static Map<String, String> getQiniu() {
        return qiniu;
    }

    public static void setQiniu(Map<String, String> qiniu) {
        FileConfig.qiniu = qiniu;
    }

    public static Map<String, String> getAli() {
        return ali;
    }

    public static void setAli(Map<String, String> ali) {
        FileConfig.ali = ali;
    }
}
