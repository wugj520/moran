package mobai.moran.common.manager;

import lombok.extern.slf4j.Slf4j;
import mobai.moran.system.entity.SysOperatorLog;
import mobai.moran.system.entity.SysLoginLog;

import java.util.TimerTask;

/**
 * 异步工厂（产生任务用）
 * @author mobai
 */
@Slf4j
public class AsyncFactory {

    /**
     * 记录登陆信息
     * @param login 登录日志信息
     * @return 任务task
     */
    public static TimerTask recordLoginInfo(SysLoginLog login){
        return new TimerTask() {
            @Override
            public void run() {
                login.insert();
            }
        };
    }

    /**
     * 操作日志记录
     * @param log 操作日志信息
     * @return 任务task
     */
    public static TimerTask recordOperatorLog(SysOperatorLog log) {
        return new TimerTask() {
            @Override
            public void run() {
                log.insert();
            }
        };
    }
}
