package mobai.moran.common.exception;

/**
 * 操作异常
 * @author mobai
 */
public class ServiceException extends RuntimeException{

    /** 编码 */
    public int code;
    /** 返回给用户的提示信息 */
    public final String tip;

    public ServiceException(String tip) {
        super(tip);
        this.tip = tip;
    }

    public ServiceException(int code,String tip) {
        super(tip);
        this.code = code;
        this.tip = tip;
    }
}
