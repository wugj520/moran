package mobai.moran.common.constant;

/**
 * 通用常量信息
 * @author mobai
 */
public class Constants {

    /** 路径目录分隔符 */
    public static final String DIR_SPLIT = "/";
    /** 字符串分隔符 */
    public static final String STRING_SPLIT = ",";
    /** 文件后缀分隔符 */
    public static final String SUFFIX_SPLIT = ".";

    /** 通用成功标识 */
    public static final String STATUS_SUCCESS = "1";
    /** 通用失败标识 */
    public static final String STATUS_FAIL = "0";

    /** 在用 */
    public static final String IN_USE = "1";
    /** 停用 */
    public static final String STOP_USE = "0";

    /** yes */
    public static final String YES = "Y";
    /** no */
    public static final String NO = "N";

    /** 未删除 */
    public static final String NO_DELETE = "1";
    /** 已删除 */
    public static final String DELETED = "0";

    /** 验证码key */
    public static final String CAPTCHA_CODE_KEY = "CAPTCHA_CODE:";
    /** 登录key */
    public static final String LOGIN_USER_KEY = "LOGIN_USER:";
    /** 密码错误key */
    public static final String PWD_ERROR_KEY = "PWD_ERROR:";
}
