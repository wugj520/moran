package mobai.moran.common.validation.paramIn;

import mobai.moran.system.util.DictUtil;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.ObjectUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 验证参数是否在字典或数组中
 * @author mobai
 */
public class ParamInValidator implements ConstraintValidator<ParamIn, Object> {

    /** 字典类型 */
    private String dictType;
    /** 值数组 */
    private String[] values;
    /** 提示信息 */
    private String message;

    @Override
    public void initialize(ParamIn annotation) {
        dictType = annotation.dictType();
        values = annotation.values();
        message = annotation.message();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        boolean b = (ObjectUtil.isEmpty(o) || (values.length == 0 && ObjectUtil.isBlank(dictType)));
        if (!b) {
            if (!ObjectUtil.contains(values,o.toString()) && ObjectUtil.isBlank(DictUtil.getDictName(dictType,o.toString()))) {
                throw new ServiceException(message);
            }
        }
        return true;
    }
}
