package mobai.moran.common.validation.paramIn;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 验证类型参数是不是在数组 或者 在字典中，注解放在参数上<br>
 * 优先判断数组，如果数组为空再判断字典类型
 * @author mobai
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ParamInValidator.class)
public @interface ParamIn {

    String message() default "不是指定参数范围";

    /** 值数组 */
    String[] values() default {};
    /** 字典类型 */
    String dictType() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
