package mobai.moran.common.validation.fieldRepeat;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.ObjectUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * 基于mybatis plus的重复字段验证,调用mybatis plus 的Model模式<br>
 * 需要继承 ModelEntity
 * @author mobai
 */
public class FieldRepeatValidator implements ConstraintValidator<FieldRepeat, Object> {

    private String[] fields;
    private String message;

    /** 主键id字段名称，在修改时用于排除自身 */
    private String idFieldName;
    /** id值 */
    private Object idFieldValue;

    @Override
    public void initialize(FieldRepeat fileRepeat) {
        this.fields = fileRepeat.fields();
        this.message = fileRepeat.message();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext context) {
        if (ObjectUtil.isEmpty(o) || fields.length ==0) {
            return  true;
        }
        if (fieldRepeat(o)){
            // 重新定义默认的错误信息模板
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(this.message).addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean fieldRepeat(Object o) {
        ModelEntity model = (ModelEntity) o;
        QueryWrapper<ModelEntity> entityWrapper = new QueryWrapper<>();
        try {
            for (Map.Entry<String, Object> entry : getColumns(fields, o).entrySet()) {
                // 只要有一个校验参数为空，那么跳过此验证
                if (ObjectUtil.isBlank(entry.getValue().toString())){
                    return true;
                }
                // 注入重复字段条件
                entityWrapper.eq(entry.getKey(), entry.getValue());
            }
        } catch (Exception e) {
            throw new ServiceException("参数校验异常~请联系支撑人员处理。");
        }
        // 如果id值存在，那么就是编辑，所以要排除自身
        if (idFieldValue != null) {
            entityWrapper.ne(idFieldName, idFieldValue);
        }
        return model.selectCount(entityWrapper) != 0;
    }

    /**
     * 获取需要校验的字段信息
     * @param fields 需要校验重复的字段
     * @param o 实体类对象
     */
    private Map<String, Object> getColumns(String[] fields, Object o) throws IllegalAccessException {
        Field[] fieldList = o.getClass().getDeclaredFields();
        Map<String, Object> map = new HashMap<>();
        for (Field f : fieldList) {
            // 设置对象中成员 属性private为可读
            f.setAccessible(true);
            for (String field : fields) {
                // 把驼峰参数名转化为下划线模式，判断是否是校验字段
                if (field.equals(StringUtils.camelToUnderline(f.getName()))) {
                    map.put(field, f.get(o));
                }
            }
            // 通过传入的实体类中 @TableId 注解的值是否为空，来判断是编辑还是新增
            if (f.isAnnotationPresent(TableId.class)) {
                TableId tableId = f.getAnnotation(TableId.class);
                idFieldName = tableId.value();
                idFieldValue = f.get(o);
            }
        }
        return map;
    }
}
