package mobai.moran.common.validation.fieldRepeat;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 字段重复校验<br>
 * 目前是基于mybatis plus的ActiveRecord模式，需要对象类继承Model.<br>
 * 注解需要放在类上面
 * @author mobai
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FieldRepeatValidator.class)
@Repeatable(FieldRepeats.class)
public @interface FieldRepeat {

    /** 需要校验的字段 */
    String[] fields() default {};

    /** 提示信息 */
    String message() default "你所输入的内容已存在";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
