package mobai.moran.common.validation.fieldRepeat;

import java.lang.annotation.*;

/**
 * 字段重复检验注解的容器，用于可以多次添加校验重复的注解
 * @author mobai
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldRepeats {

    FieldRepeat[] value();
}
