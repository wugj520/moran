package mobai.moran.common.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Set;

/**
 * 继承 mybatis plus 的Model, 在实体的mapper继承 baseMapper 的情况下，可以通过实体调用dao方法
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ModelEntity extends Model<ModelEntity> implements Serializable {

    // 用于搜索参数
    /** 开始时间 */
    @TableField(exist = false)
    private String searchBeginTime;
    /** 结束时间 */
    @TableField(exist = false)
    private String searchEndTime;
    /** 搜索关键字 */
    @TableField(exist = false)
    private String searchKey;
    /** 可访问部门列表 */
    @TableField(exist = false)
    private Set<Long> userDeptScope;

    // 用于校验分组
    /** 参数校验分组：增加 */
    public @interface add { }
    /** 参数校验分组：删除 */
    public @interface del { }
    /** 参数校验分组：编辑 */
    public @interface edit { }
    /** 参数校验分组：查询 */
    public @interface find { }
}
