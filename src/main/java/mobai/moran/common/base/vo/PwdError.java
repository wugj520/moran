package mobai.moran.common.base.vo;

import lombok.Data;

import java.util.Date;

/**
 * 密码错误的数据记录
 * @author mobai
 */
@Data
public class PwdError {

    /** 错误次数 */
    Integer num;
    /** 错误的时间 */
    Date time;
}
