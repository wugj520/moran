package mobai.moran.common.base.vo;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * 登录的请求参数
 * @author mobai
 */
@Data
@ToString
public class LoginRequest {

    @NotBlank(message = "账号不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;

    /** 记住我，不传就是false */
    private Boolean remember = false;

    /** 验证码图形对应的缓存key */
    private String verKey;

    /** 用户输入的验证码的值 */
    private String verCode;

    /** 是否写入cookie会话信息 */
    private Boolean createCookie = true;
}
