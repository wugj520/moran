package mobai.moran.common.base.vo;

import lombok.Data;

/**
 * http响应结果封装
 * @author mobai
 */
@Data
public class ResultData {

    /** 请求是否成功 */
    private Boolean success;

    /** 响应状态码 */
    private int code;

    /** 响应信息 */
    private String message;

    /** 响应对象 */
    private Object data;

    public ResultData() {
        this.success = true;
        this.code = 0;
        this.message = "请求成功";
    }

    public ResultData(Object data) {
        this.success = true;
        this.code = 0;
        this.message = "请求成功";
        this.data = data;
    }

    public ResultData(int code, String message) {
        this.success = true;
        this.code = code;
        this.message = message;
    }

    public ResultData(int code, String message, Object data) {
        this.success = true;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ResultData(Boolean success,int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public ResultData(Boolean success, int code, String message, Object data) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
