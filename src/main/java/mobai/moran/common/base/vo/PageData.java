package mobai.moran.common.base.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页结果封装
 * @author mobai
 */
@Data
public class PageData implements Serializable {

    /** 总记录数 */
    private long count;

    /** 列表数据 */
    private List<?> data;

    /** 消息状态码 */
    private int code;

    /** 消息内容 */
    private String msg;
}
