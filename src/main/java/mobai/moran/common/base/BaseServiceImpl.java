package mobai.moran.common.base;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.PageUtil;
import mobai.moran.system.security.util.SecurityUtil;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 处理层实现
 * @author mobai
 */
public class BaseServiceImpl<M extends BaseMapper<T>, T extends ModelEntity> extends ServiceImpl<BaseMapper<T>, T> implements BaseService<T> {

    @Override
    public void add(T t) {
        this.save(t);
    }

    @Override
    public void edit(T t) {
        this.updateById(t);
    }

    @Override
    public void del(T t) {
        this.removeById(t);
    }

    @Override
    public Map<String, Object> detail(Serializable id) {
        return this.baseMapper.detail(id);
    }

    @Override
    public List<Map<String, Object>> findList(T t) {
        t.setUserDeptScope(SecurityUtil.getLoginUser().getDeptScope());
        return this.baseMapper.list(t);
    }

    @Override
    public PageData findPageList(T t) {
        t.setUserDeptScope(SecurityUtil.getLoginUser().getDeptScope());
        if (PageUtil.getPageSize()>50000){
            throw new ServiceException("禁止过大的数据获取");
        }
        IPage<Map<String,Object>> page = this.baseMapper.list(PageUtil.defaultPage(),t);
        return PageUtil.createPageResult(page);
    }
}
