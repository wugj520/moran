package mobai.moran.common.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 实体基类
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaseEntity extends ModelEntity{

    /** 创建时间 */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;
    /** 创建人 */
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private Long createUser;
    /** 更新时间 */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
    /** 更新人 */
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private Long updateUser;

    /** 删除标识 */
    @TableField(value = "deleted", fill = FieldFill.INSERT)
    @TableLogic
    private String deleted ;

}
