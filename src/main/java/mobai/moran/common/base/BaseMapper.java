package mobai.moran.common.base;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 通用mapper扩展
 * @author mobai
 */
public interface BaseMapper<T> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {

    /**
     * 自定义分页查询
     * @param page      分页数据
     * @param params    条件参数
     */
    IPage<Map<String,Object>> list(@Param("page") Page<?> page, @Param("params")T params);

    /**
     * 自定义列表查询，使用必须写SQL
     * @param params 请求参数
     */
    List<Map<String,Object>> list(@Param("params")T params);

    /**
     * 自定义详情查询，使用必须写SQL
     * @param id 主键
     */
    Map<String, Object> detail(Serializable id);

}
