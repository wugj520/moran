package mobai.moran.common.base;

import com.baomidou.mybatisplus.extension.service.IService;
import mobai.moran.common.base.vo.PageData;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 数据处理接口
 * @author mobai
 */
public interface BaseService<T extends ModelEntity> extends IService<T> {

    /**
     * 添加
     * @param t 实体类对象，需要继承BaseEntity
     */
    void add(T t);

    /**
     * 编辑
     * @param t 实体类对象，需要继承BaseEntity
     */
    void edit(T t);

    /**
     * 删除
     * @param t 实体类对象，需要继承BaseEntity
     */
    void del(T t);

    /**
     * 详情
     * @param id ID
     * @return  详细信息
     */
    Map<String,Object> detail(Serializable id);

    /**
     * 根据条件查询所有
     * @param t 实体类对象，需要继承BaseEntity，可以在对象内部扩展查询条件
     * @return  数据列表
     */
    List<Map<String,Object>> findList(T t);

    /**
     * 根据条件分页查询
     * @param t 实体类对象，需要继承BaseEntity，可以在对象内部扩展查询条件
     * @return  数据列表
     */
    PageData findPageList(T t);
}
