package mobai.moran.common.mp;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import mobai.moran.common.constant.Constants;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.common.util.ObjectUtil;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.ReflectionException;

import java.util.Date;

/**
 * @author mobai
 */
@Slf4j
public class MyMetaObjectHandler  implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        try {
            setValue(metaObject, "createUser", this.getUserId());
            setValue(metaObject, "createTime", new Date());
            setValue(metaObject, "deleted", Constants.NO_DELETE);
        } catch (ReflectionException e) {
            log.warn("新增数据自动填充失败");
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        try {
            setValue(metaObject, "updateUser", this.getUserId());
            setValue(metaObject, "updateTime", new Date());
        } catch (ReflectionException e) {
            log.warn("编辑数据自动填充失败");
        }
    }

    private Long getUserId() {
        if (SecurityUtil.hasLogin()){
            return SecurityUtil.getLoginUser().getUserId();
        }else {
            return 0L;
        }
    }

    /** 设置属性 */
    private void setValue(MetaObject metaObject, String fieldName, Object value) {
        Object originalAttr = getFieldValByName(fieldName, metaObject);
        if (ObjectUtil.isEmpty(originalAttr)) {
            setFieldValByName(fieldName, value, metaObject);
        }
    }
}
