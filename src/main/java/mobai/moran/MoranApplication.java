package mobai.moran;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author mobai
 */
@SpringBootApplication
public class MoranApplication {
    public static void main(String[] args){
        SpringApplication.run(MoranApplication.class, args);
    }
}
