package mobai.moran.system.annotation;

import mobai.moran.system.constant.OperatorType;

import java.lang.annotation.*;

/**
 * 日志记录注解
 *
 * @author moran
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    /** 模块名称 */
    String moduleName() default "";
    /** 操作类型 */
    String OperatorType() default OperatorType.OTHER;
    /** 是否保存请求参数 */
    boolean saveRequestData() default true;
    /** 是否保存返回数据 */
    boolean saveResultData() default true;
}
