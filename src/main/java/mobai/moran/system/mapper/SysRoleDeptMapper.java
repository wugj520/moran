package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.link.SysRoleDept;

/**
 * @author mobai
 */
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}
