package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysRole;

import java.util.List;

/**
 * @author mobai
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 根据角色ID获取关联的菜单列表
     *
     * @param roleId 角色ID
     * @return 角色关联的菜单列表
     */
    List<Long> selectMenuIdsByRoleId(Long roleId);

    /**
     * 根据角色ID获取关联的部门权限列表
     *
     * @param roleId
     * @return 角色关联的部门权限列表
     */
    List<Long> selectDeptIdsByRoleId(Long roleId);
}
