package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysConfig;

/**
 * 系统参数配置
 * @author mobai
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
