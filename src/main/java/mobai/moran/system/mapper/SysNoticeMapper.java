package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysNotice;

/**
 * 通知公告
 * @author mobai
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {

}
