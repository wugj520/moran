package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysMenu;

/**
 * @author mobai
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
