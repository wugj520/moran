package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysDept;

/**
 * @author mobai
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    /**
     * 修改该部门的父级部门状态
     * @param status 状态
     * @param ancestors 祖级列表
     */
    void updateParentStatus(String status,String ancestors);

    /**
     * 通过用户名查询部门
     * @param userName  用户名
     */
    SysDept findByUserName(String userName);

    /**
     * 修改子级的祖级列表
     * @param deptId    父级部门
     * @param newParentAncestors    新父级的祖级列表
     * @param oldParentAncestors    旧父级的祖级列表
     * @return
     */
    int updateChildrenAncestors(Long deptId, String newParentAncestors, String oldParentAncestors);
}
