package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.link.SysRoleMenu;

/**
 * @author mobai
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
