package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysDict;
import org.apache.ibatis.annotations.Param;

/**
 * 字典管理
 * @author mobai
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    /**
     * 同步修改字典类型
     *
     * @param oldDictType 旧字典类型
     * @param newDictType 新旧字典类型
     * @return 结果
     */
    int updateDictType(@Param("oldDictType") String oldDictType, @Param("newDictType") String newDictType);
}
