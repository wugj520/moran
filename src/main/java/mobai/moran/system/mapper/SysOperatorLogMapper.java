package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysOperatorLog;

/**
 * @author mobai
 */
public interface SysOperatorLogMapper extends BaseMapper<SysOperatorLog> {

}
