package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.link.SysUserRole;

/**
 * @author mobai
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
