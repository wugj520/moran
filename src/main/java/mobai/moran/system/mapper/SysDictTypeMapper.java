package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysDictType;

/**
 * 字典类型管理
 * @author mobai
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
