package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysFile;

import java.util.List;

/**
 * @author mobai
 */
public interface SysFileMapper extends BaseMapper<SysFile> {


    /**
     * 根据id查询它所有上级（包含自身）
     * @param id 根节点
     * @return
     */
    List<SysFile> selectParentList(Long id);
}
