package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysUserGroup;

/**
 * 用户组信息
 * @author mobai
 */
public interface SysUserGroupMapper extends BaseMapper<SysUserGroup> {

}
