package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysTask;

/**
 * 任务
 * @author mobai
 */
public interface SysTaskMapper extends BaseMapper<SysTask> {

}
