package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysRead;

/**
 * @author mobai
 */
public interface SysReadMapper extends BaseMapper<SysRead> {

}
