package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysRole;
import mobai.moran.system.entity.SysUser;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 用户管理
 * @author mobai
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 通过用户ID查询角色编码
     * @param userId    用户ID
     * @return  角色编码列表
     */
    List<SysRole> getUserRoleById(Long userId);

    /**
     * 通过用户ID查询角色编码
     * @param userId    用户ID
     * @return  角色编码列表
     */
    Set<Long> getRoleIdByUserId(Long userId);

    /**
     * 通过用户ID查询用户菜单权限
     * @param userId    用户ID
     * @return 菜单权限列表
     */
    Set<String> getUserMenuPermitById(Long userId);

    /**
     * 通过部门ID查询部门及以下的ID
     * @param deptId    部门ID
     * @return 部门ID列表
     */
    Set<Long> getDeptIdById(Long deptId);

    /**
     * 通过角色ID查询自定义的部门ID
     * @param roleId    角色ID
     * @return 部门ID列表
     */
    Set<Long> getDeptIdByRoleId(Long roleId);

    /**
     * 通过部门ID查询用户，返回格式为下拉样式
     * @param deptId    部门ID
     * @return  用户列表
     */
    List<Map<String,Object>> getUserByDeptIdForSelect (Long deptId);
}
