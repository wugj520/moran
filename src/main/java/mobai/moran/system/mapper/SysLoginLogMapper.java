package mobai.moran.system.mapper;

import mobai.moran.common.base.BaseMapper;
import mobai.moran.system.entity.SysLoginLog;

/**
 * @author mobai
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {

}
