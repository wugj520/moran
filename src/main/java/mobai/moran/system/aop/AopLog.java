package mobai.moran.system.aop;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.manager.AsyncFactory;
import mobai.moran.common.manager.AsyncManager;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.common.util.ServletUtil;
import mobai.moran.common.util.StringUtil;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysOperatorLog;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.system.entity.vo.LoginUser;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;

/**
 * <p>使用 aop 切面记录请求日志信息</p>
 * @author mobai
 */
@Aspect
@Component
@Slf4j
public class AopLog {

    /** 切入点 */
    @Pointcut("@annotation(mobai.moran.system.annotation.Log)")
    public void log() {

    }

    /**
     * 处理完请求后执行
     *
     * @param joinPoint 切点
     */
    @AfterReturning(pointcut = "log()", returning = "jsonResult")
    public void doAfterReturning(JoinPoint joinPoint, Object jsonResult) {
        handleLog(joinPoint, null, jsonResult);
    }

    /**
     * 拦截异常操作
     *
     * @param joinPoint 切点
     * @param e 异常
     */
    @AfterThrowing(value = "log()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Exception e) {
        handleLog(joinPoint, e, null);
    }

    /**
     *
     * @param point         切点
     * @param e             异常
     * @param result        返回值p
     */
    protected void handleLog(final JoinPoint point, final Exception e, Object result){
        try{
            // 获得注解
            Log controllerLog = getAnnotationLog(point);
            if (controllerLog == null) {
                return;
            }
            // 获取当前的用户
            LoginUser user = SecurityUtil.getLoginUser();
            // 操作日志记录
            SysOperatorLog operator = new SysOperatorLog();
            operator.setOperatorTitle(controllerLog.moduleName());
            operator.setOperatorType(controllerLog.OperatorType());

            // 设置方法名称
            String className = point.getTarget().getClass().getName();
            String methodName = point.getSignature().getName();
            operator.setOperatorMethod(className + "." + methodName);
            // 记录操作人信息
            if (user != null){
                operator.setOperatorUser(user.getUsername());
            }
            operator.setOperatorIp(ServletUtil.getRequestIp());
            operator.setOperatorLocation(ServletUtil.getRequestLocation());
            // 记录请求和返回信息
            operator.setRequestWay(ServletUtil.getRequest().getMethod());
            operator.setRequestUrl(ServletUtil.getRequest().getRequestURI());
            if (controllerLog.saveRequestData()){
                operator.setRequestParam(getReqValue(point));
            }
            if (controllerLog.saveResultData()){
                operator.setResultParam(JSON.toJSONString(result));
            }
            if (ObjectUtil.isNotEmpty(e)){
                operator.setOperatorStatus(Constants.STATUS_FAIL);
                operator.setOperatorMsg(e.getMessage());
            }else{
                operator.setOperatorStatus(Constants.STATUS_SUCCESS);
            }
            // 当自动填充有获取request时，不使用自动填充，否则会抱错
            operator.setOperatorTime(new Date());
            operator.setDeleted(Constants.NO_DELETE);
            // 保存数据库
            AsyncManager.me().execute(AsyncFactory.recordOperatorLog(operator));
        }catch (Exception e1){
            // 记录本地异常日志
            log.error("==操作日志记录异常==");
            log.error("异常信息:{}", e1.getMessage());
            e1.printStackTrace();
        }
    }

    private String getReqValue(JoinPoint joinPoint){
        String requestMethod = ServletUtil.getRequest().getMethod();
        if (HttpMethod.PUT.name().equals(requestMethod) || HttpMethod.POST.name().equals(requestMethod)){
            return StringUtil.substring(argsToString(joinPoint.getArgs()),0,2000);
        }else {
            Map<?, ?> paramsMap = (Map<?, ?>) ServletUtil.getRequest().getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
            return StringUtil.substring(paramsMap.toString(),0,2000);
        }
    }

    /** 参数拼装 */
    private String argsToString(Object[] paramsArray){
        StringBuilder params = new StringBuilder();
        if (ObjectUtil.isNotEmpty(paramsArray)){
            for (Object o : paramsArray){
                if (!isFilterObject(o)){
                    params.append(JSON.toJSON(o).toString()).append(" ");
                }
            }
        }
        return params.toString().trim();
    }

    /** 判断是否需要过滤的对象。 */
    public boolean isFilterObject(Object o){
        return o instanceof MultipartFile || o instanceof HttpServletRequest || o instanceof HttpServletResponse;
    }

    /**
     * 获取log注解
     * @param joinPoint 切面信息
     */
    private Log getAnnotationLog(JoinPoint joinPoint){
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        if (method != null) {
            return method.getAnnotation(Log.class);
        }
        return null;
    }
}
