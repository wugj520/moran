package mobai.moran.system.security;

import mobai.moran.system.security.config.AccessDeniedHandlerImpl;
import mobai.moran.system.security.config.AuthenticationEntryPointImpl;
import mobai.moran.system.security.config.JwtRequestFilter;
import mobai.moran.system.security.config.LogoutSuccessHandlerImpl;
import mobai.moran.system.security.service.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;


/**
 * spring security配置
 * @author mobai
 */
@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(
        // 启用Spring Security的@PreAuthorize 以及@PostAuthorize 注解。
        prePostEnabled = true,
        // 启用Spring Security的@Secured 注解。
        securedEnabled = true,
        // 启用@RoleAllowed 注解
        jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    UserDetailsService userDetailsService;

    /**
     * 解决 无法直接注入 AuthenticationManager
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * 加密器
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 身份认证接口
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Override
    public void configure(WebSecurity web) {
        //解决静态资源被拦截的问题
        web.ignoring().antMatchers(loadExcludePath ());
    }

    // 放行路径
    public String[] loadExcludePath () {
        return new String[] {"/static/**","/assets/**","/lib/**","/js/**","/css/**"};
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // CSRF禁用，因为不使用session
        http.csrf().disable();
        // 认证失败处理类
        http.exceptionHandling().authenticationEntryPoint(new AuthenticationEntryPointImpl());
        // 所以不使用session，使用这个会导致getAuthentication()为null
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        // 匿名访问路径
        http.authorizeRequests().antMatchers("/login", "/captchaImage").permitAll();
        // get请求放行
        http.authorizeRequests().antMatchers(HttpMethod.GET, loadExcludePath ()).permitAll();
        // 其余的都需授权访问
        http.authorizeRequests().anyRequest().authenticated();
        // 禁用页面缓存，返回的都是json
        http.headers().frameOptions().disable();
        // 退出处理
        http.logout().logoutUrl("/logout").logoutSuccessHandler(new LogoutSuccessHandlerImpl());
        // 权限不足处理
        http.exceptionHandling().accessDeniedHandler(new AccessDeniedHandlerImpl());
        // jwt过滤!!! JwtRequestFilter 不要让spring管理，不然不会放行静态资源
        http.addFilterBefore(new JwtRequestFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
