package mobai.moran.system.security.util;

import mobai.moran.common.cache.memory.CacheUtil;
import mobai.moran.common.cache.redis.RedisUtil;
import mobai.moran.common.constant.Constants;

import java.util.concurrent.TimeUnit;

/**
 * 密码错误处理工具类
 * @author mobai
 */
public class PwdErrorUtil {

    /**
     * 保存密码错误记录
     * @param username  登录账号
     */
    public static void setPwdError(String username){
        int errorNum = getPwdErrorNum(username);
        errorNum += 1;
        if (SecurityUtil.IS_REDIS_SAVE){
            RedisUtil.put(getPwdErrorKey(username),errorNum,60*60*1000L, TimeUnit.MILLISECONDS);
        }else {
            CacheUtil.put(getPwdErrorKey(username),errorNum,60*60*1000L);
        }
    }

    /**
     * 是否禁止登录,(密码错误时间间隔1小时内，错误次数+1，当登录成功自动移除错误次数。当满足错误次数3，锁定账号1小时。)
     * @param username  登录账号
     */
    public static boolean loginIsBan(String username){
        int errorNum = getPwdErrorNum(username);
        return errorNum >= 3;
    }

    /** 密码错误数据清空 */
    public static void delPwdError(String username){
        if (SecurityUtil.IS_REDIS_SAVE){
            RedisUtil.remove(getPwdErrorKey(username));
        }else {
            CacheUtil.remove(getPwdErrorKey(username));
        }
    }

    private static int getPwdErrorNum(String username){
        Object error ;
        if (SecurityUtil.IS_REDIS_SAVE){
            error = RedisUtil.get(getPwdErrorKey(username));
        }else {
            error = CacheUtil.get(getPwdErrorKey(username));
        }
        if (error != null){
            return (int) error;
        }
        return 0;
    }

    private static String getPwdErrorKey(String username){
        return Constants.PWD_ERROR_KEY+username;
    }
}
