package mobai.moran.system.security.util;

import mobai.moran.common.cache.memory.CacheUtil;
import mobai.moran.common.cache.redis.RedisUtil;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.common.util.ServletUtil;
import mobai.moran.common.util.StringUtil;
import mobai.moran.system.entity.vo.LoginUser;
import mobai.moran.system.entity.vo.SystemUserOnline;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 安全服务工具类
 * @author mobai
 */
public class SecurityUtil {

    /** 是否使用redis缓存 */
    public static final boolean IS_REDIS_SAVE = true;

    /**
     * 获取loginUser<br>
     * 优先从session获取，如果没有，那么走redis里面获取,都没有返回null或者报错
     */
    public static LoginUser getLoginUser(){
        return JwtUtil.getLoginUser(ServletUtil.getRequest());
    }

    /** 退出当前会话 */
    public static void loginOut(){
        JwtUtil.loginOut();
    }

    /** 退出指定用户
     * @param key   标识
     */
    public static void loginOut(String key) {
        JwtUtil.clearLoginUser(key);
    }

    /** 是否登录（无报错，只有是否） */
    public static boolean hasLogin() {
       return JwtUtil.hasLogin();
    }

    /** 在线用户 */
    public static List<SystemUserOnline> onlineUser(){
        Collection<String> keys;
        if (IS_REDIS_SAVE){
            keys = RedisUtil.keys(Constants.LOGIN_USER_KEY + "*");
        }else {
            keys = CacheUtil.keys(Constants.LOGIN_USER_KEY);
        }
        List<SystemUserOnline> userOnlineList = new ArrayList<>();
        System.out.println(keys);
        if (ObjectUtil.isEmpty(keys)){
            return  userOnlineList;
        }
        for (String key : keys) {
            Object obj = (IS_REDIS_SAVE ? RedisUtil.get(key) : CacheUtil.get(key));
            if (obj instanceof LoginUser) {
                SystemUserOnline online = new SystemUserOnline();
                LoginUser user = (LoginUser) obj;
                online.setTokenId(key);
                online.setUserName(user.getUsername());
                online.setDeptName(user.getDeptName());
                online.setIpaddr(user.getIpaddr());
                online.setLoginLocation(ServletUtil.getLocationByIp(user.getIpaddr()));
                online.setBrowser(user.getBrowser());
                online.setOs(user.getOs());
                online.setLoginTime(user.getLoginTime());
                online.setExpireTime(user.getExpireTime());
                userOnlineList.add(online);
            }
        }
        return userOnlineList;
    }

    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword 真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    public static int pwdSafeLv(String pwd){
        if (ObjectUtil.isBlank(pwd) || pwd.length()<8){
            return 0;
        }
        int num = 0;
        if (pwd.matches(StringUtil.NUMBER_REX)){
            num += 1;
        }
        if (pwd.matches(StringUtil.LOWERCASE_REX)){
            num += 1;
        }
        if (pwd.matches(StringUtil.UPPERCASE_REX)){
            num += 1;
        }
        if (pwd.matches(StringUtil.SYMBOL_REX)){
            num += 1;
        }
        if (pwd.matches(StringUtil.CHINESE_REX)){
            num += 1;
        }
        return  num - 1;
    }

    /**
     * 是否为管理员
     *
     * @param userId 用户ID
     * @return 结果
     */
    public static boolean isAdmin(Long userId) {
        return userId != null && 1L == userId;
    }

    /**
     * 保存密码错误记录
     * @param username  登录账号
     */
    public static void setPwdError(String username){
        PwdErrorUtil.setPwdError(username);
    }

    /**
     * 是否禁止登录,(规定时间内错误次数达到3次)
     * @param username  登录账号
     */
    public static boolean loginIsBan(String username){
        return PwdErrorUtil.loginIsBan(username);
    }

    /** 密码错误数据清空 */
    public static void delPwdError(String username){
        PwdErrorUtil.delPwdError(username);
    }

}
