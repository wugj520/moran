package mobai.moran.system.security.util;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import mobai.moran.common.cache.memory.CacheUtil;
import mobai.moran.common.cache.redis.RedisUtil;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.common.util.ServletUtil;
import mobai.moran.system.entity.vo.LoginUser;
import mobai.moran.system.util.ConfigUtil;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * JWT组件
 * <p>
 *  一个完整的JwtToken由三部分组成：头部+负载信息+签名
 *  header 存放JwtToken签名的算法 | token的类型：{"alg": "HS512","typ": "JWT"}
 *  payload 主要存放用户名、创建时间、生成时间：{"sub":"wang","created":1489079981393,"exp":1489684781}
 *  signature 生成算法：HMACSHA512(base64UrlEncode(header) + "." +base64UrlEncode(payload),secret)
 * </p>
 * @author mobai
 */
@Slf4j
public class JwtUtil {

    /** 默认请求头 */
    private static final String DEFAULT_HEADER = "Authorization";
    /** 默认令牌秘钥 */
    private static final String DEFAULT_SECRET = "MoBaiLoveXiaMo";
    /** 前端未定义标识 */
    private static final String WEB_NULL = "undefined";
    /** 默认令牌请求有效期 */
    private static final long DEFAULT_EXPIRE_TIME = 60*60*1000;
    /** 记住我的令牌有效期 */
    private static final long REMEMBER_ME_EXPIRE_TIME = 7*24*60*60*1000;
    /** 令牌自动刷新的时间间隔 */
    private static final long MILLIS_MINUTE_TEN = 20*60*1000;

    /** 退出当前会话（会清除当前连接的cookie） */
    public static void loginOut(){
        clearCookie();
        loginOut(getClaimsByToken(getToken(ServletUtil.getRequest())).getSubject());
    }

    /** 退出指定会话 */
    private static void loginOut(String key){
        clearLoginUser(key);
    }

    public static boolean hasLogin(){
        String token = getToken(ServletUtil.getRequest());
        if (ObjectUtil.isBlank(token)){
            return false;
        }
        try {
            LoginUser user = getLoginUserByKey(getClaimsByToken(token).getSubject());
            return ObjectUtil.isNotEmpty(user);
        }catch (Exception e){
            return false;
        }
    }

    /**
     * 从请求中拿到token
     */
    public static String getToken(HttpServletRequest request) {
        // 1、优先走请求头获取token
        String token = request.getHeader(DEFAULT_HEADER);
        // 2、当请求头没有带token，尝试走cookie获取
        if (ObjectUtil.isBlank(token)){
            Cookie[] cookies = request.getCookies();
            if(ObjectUtil.isNotEmpty(cookies)){
                for (Cookie cookie : cookies) {
                    if (DEFAULT_HEADER.equals(cookie.getName())){
                        token = cookie.getValue();
                    }
                }
            }
        }
        // 3、当token为空或者undefined，返回空字符串
        if (ObjectUtil.isBlank(token) || WEB_NULL.equals(token)){
            return "";
        }
        return token.trim();
    }

    /**
     * 根据用户信息生成token
     */
    public static String createToken(LoginUser loginUser) {
        if (ObjectUtil.isBlank(loginUser.getUuid())){
            loginUser.setUuid(UUID.randomUUID().toString());
        }
        long timeOut = DEFAULT_EXPIRE_TIME;
        if (!loginUser.getRememberMe() && ConfigUtil.containsKey("user_login_cache_time")){
            timeOut = Long.parseLong(ConfigUtil.get("user_login_cache_time"))*60*1000;
        }
        if (loginUser.getRememberMe()){
            if (ConfigUtil.containsKey("user_login_remember_time")){
                timeOut = Long.parseLong(ConfigUtil.get("user_login_remember_time"))*60*1000;
            }else {
                timeOut = REMEMBER_ME_EXPIRE_TIME;
            }
        }
        String key = getTokenKey(loginUser.getUsername(),loginUser.getUuid());
        saveLoginUser(key,loginUser,timeOut);
        String token = createToken(key,timeOut);
        cacheCookie(token,timeOut);
        return token;
    }

    /**
     * 通过负载和主题生成token
     * @param subject   主题
     */
    private static String createToken(String subject,long timeOut){
        Map<String, Object> claims = new HashMap<>();
        return Jwts.builder()
                .setClaims(claims)
                // 面向用户
                .setSubject(subject)
                // 签发时间
                .setIssuedAt(new Date(System.currentTimeMillis()))
                // 过期时间
                .setExpiration(new Date(System.currentTimeMillis() + timeOut))
                // 签名方法
                .signWith(SignatureAlgorithm.HS512, DEFAULT_SECRET)
                .compact();
    }

    /**
     * 验证token的IP是否当前一致并且未过期
     * @param token         客户端传入的token
     */
    public static boolean validateToken(String token){
        Claims claims = getClaimsByToken(token);
        if (ObjectUtil.isEmpty(claims)){
            return false;
        }
        LoginUser user = getLoginUserByKey(claims.getSubject());
        if (ObjectUtil.isEmpty(user)){
            clearCookie();
            return false;
        }
        // 已不在线 或者 ip不一致，那么清除cookie中的token让其重新登录
        if (!ServletUtil.getRequestIp().equals(user.getIpaddr())){
            clearCookie();
            clearLoginUser(claims.getSubject());
            throw new ServiceException(401,"请重新登录！！！");
        }
        // 如果单点登录已开启，那么检测本账号是不是最新连接
        if (Constants.STATUS_SUCCESS.equals(ConfigUtil.get("user_login_only"))){
            Date loginTime = getMaxLoginTimeByUserName(user.getUsername());
            if (loginTime != null && !loginTime.equals(user.getLoginTime())){
                clearCookie();
                clearLoginUser(claims.getSubject());
                throw new ServiceException(401,"该账号已经在其他地方登录，如非本人登录，请及时修改密码或者与管理员进行联系");
            }
        }
        // token快要到期了，刷新一下到期时间
        long time = MILLIS_MINUTE_TEN;
        if (ConfigUtil.containsKey("user_login_refresh_time")){
            time = Long.parseLong(ConfigUtil.get("user_login_refresh_time"))*60*1000;
        }
        if (claims.getExpiration().before(new Date(System.currentTimeMillis() + time))){
            createToken(user);
        }
        return true;
    }

    public static LoginUser getLoginUser(HttpServletRequest request){
        try {
            return getLoginUserByKey(getClaimsByToken(getToken(request)).getSubject());
        }catch (Exception e){
            throw new ServiceException("获取用户信息异常");
        }
    }

    public static Date getMaxLoginTimeByUserName(String userName){
        Collection<String> keys ;
        if (SecurityUtil.IS_REDIS_SAVE){
            keys = RedisUtil.keys(getTokenKey(userName,"*"));
        }else {
            keys = CacheUtil.keys(getTokenKey(userName,""));
        }
        Date loginTime = null;
        for (String key: keys){
            LoginUser user = getLoginUserByKey(key);
            if (loginTime == null){
                loginTime = user.getLoginTime();
            }else {
                if (loginTime.before(user.getLoginTime())){
                    loginTime = user.getLoginTime();
                }
            }
        }
        return loginTime;
    }
    /**
     * 通过指定标识获取缓存中的loginUser
     * @param key   标识
     * @return  用户的部分登录信息
     */
    public static LoginUser getLoginUserByKey(String key){
        if (SecurityUtil.IS_REDIS_SAVE){
            return (LoginUser) RedisUtil.get(key);
        }else {
            return (LoginUser) CacheUtil.get(key);
        }
    }

    /**
     * 清除指定key的loginUser的缓存
     * @param key   标识
     */
    public static void clearLoginUser(String key){
        if (SecurityUtil.IS_REDIS_SAVE){
            RedisUtil.remove(key);
        }else {
            CacheUtil.remove(key);
        }
    }

    /**
     * 缓存loginUser
     * @param key   标识
     * @param user  用户信息
     * @param timeOut   超时时间
     */
    public static void saveLoginUser(String key,LoginUser user,long timeOut){
        user.setExpireTime(new Date(System.currentTimeMillis() + timeOut));
        if (SecurityUtil.IS_REDIS_SAVE){
            RedisUtil.put(key,user,timeOut, TimeUnit.MILLISECONDS);
        }else {
            CacheUtil.put(key,user,timeOut);
        }
    }

    /**
     * 从token中拿到负载信息
     */
    private static Claims getClaimsByToken(String token){
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(DEFAULT_SECRET).parseClaimsJws(token).getBody();
        } catch (Exception e){
            log.error("JWT解析失败：", e);
            clearCookie();
            if (e instanceof ExpiredJwtException) {
                throw new ServiceException(401,"登录已过期，请重新登录！！！");
            }else {
                throw new ServiceException(401,"认证失败，请重新登录");
            }
        }
        if (claims == null) {
            throw new ServiceException(401,"认证失败，请重新登录");
        }
        return claims;
    }

    /**
     * 把token缓存到cookie
     * @param token token
     * @param maxAge  持续时间
     */
    private static void cacheCookie(String token,Long maxAge) {
        //创建cookie
        Cookie authorization = new Cookie(DEFAULT_HEADER, token);
        authorization.setMaxAge(maxAge.intValue());
        authorization.setHttpOnly(true);
        authorization.setPath("/");
        ServletUtil.getResponse().addCookie(authorization);
    }

    /**
     * 清除cookie中的token缓存
     */
    public static void clearCookie() {
        Cookie temp = new Cookie(DEFAULT_HEADER, "");
        temp.setMaxAge(0);
        temp.setPath("/");
        ServletUtil.getResponse().addCookie(temp);
    }

    private static String getTokenKey(String username,String uuid){
        return Constants.LOGIN_USER_KEY + username + ":" + uuid;
    }
}
