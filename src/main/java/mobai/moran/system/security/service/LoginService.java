package mobai.moran.system.security.service;

import mobai.moran.common.base.vo.LoginRequest;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.manager.AsyncFactory;
import mobai.moran.common.manager.AsyncManager;
import mobai.moran.system.entity.SysLoginLog;
import mobai.moran.system.entity.SysUser;
import mobai.moran.system.entity.vo.LoginUser;
import mobai.moran.system.security.util.JwtUtil;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.system.service.SysUserService;
import mobai.moran.system.util.ConfigUtil;
import mobai.moran.system.util.LogUtil;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 登录校验方法, PS:防止嵌套，所以不能随意放在其他service里
 * @author mobai
 */
@Service
public class LoginService {

    @Resource
    SysUserService userService;

    @Resource
    private AuthenticationManager authenticationManager;

    public String login(LoginRequest login){
        SysLoginLog loginLog = LogUtil.createLoginLog(login, Constants.STATUS_SUCCESS, "登录成功");
        String token = "";
        // 用户验证
        try {
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
            LoginUser loginUser = (LoginUser) authentication.getPrincipal();
            // 登录成功
            // 1.先清空密码错误次数
            SecurityUtil.delPwdError(login.getUsername());
            // 2.二次校验用户信息
            SysUser user = userService.getById(loginUser.getUserId());
            if (user == null){
                loginError(loginLog, "登录异常");
                return token;
            }
            if (Constants.STATUS_FAIL.equals(user.getStatus())){
                loginError(loginLog, "登录失败，账号已停用");
                return token;
            }
            String pwdUpdateDays = ConfigUtil.get("user_password_update_days");
            if (user.getLastLoginTime() == null) {
                user.setLastLoginTime(new Date());
            }
            if (user.getLastPwdUpdateTime() == null){
                user.setLastPwdUpdateTime(new Date());
            }
            if (!"0".equals(pwdUpdateDays) && user.getLastPwdUpdateTime().before(new Date(System.currentTimeMillis() - (Integer.parseInt(pwdUpdateDays)*24*60*60*1000L) ))){
                loginError(loginLog, "登录失败，已经 " + pwdUpdateDays + " 未修改密码，请联系管理员进行处理。");
                return token;
            }
            String initPwdUpdateDays = ConfigUtil.get("user_init_password_update_days");
            if (!"0".equals(initPwdUpdateDays) && SecurityUtil.matchesPassword(ConfigUtil.get("user_init_password"),user.getPassword()) && user.getLastPwdUpdateTime().before(new Date(System.currentTimeMillis() - (Integer.parseInt(initPwdUpdateDays)*24*60*60*1000L) ))){
                loginError(loginLog, "登录失败，未及时修改初始密码，请联系管理员进行处理。");
                return token;
            }

            // 3.补充信息
            loginUser.setRememberMe(login.getRemember());
            loginUser.setBrowser(loginLog.getLoginBrowser());
            loginUser.setOs(loginLog.getLoginOs());
            loginUser.setIpaddr(loginLog.getLoginIp());
            loginUser.setLoginTime(new Date());
            loginUser = userService.loginUserReplenish(loginUser);
            // 4.记录信息
            user.setLastLoginIp(loginLog.getLoginIp());
            user.setLastLoginTime(new Date());
            user.updateById();
            // 记录日志
            AsyncManager.me().execute(AsyncFactory.recordLoginInfo(loginLog));
            return JwtUtil.createToken(loginUser);
        }catch (Exception e){
            if (e instanceof BadCredentialsException) {
                SecurityUtil.setPwdError(login.getUsername());
                loginError(loginLog, "用户不存在/密码错误");
            } else if (e instanceof ServiceException) {
                loginError(loginLog, ((ServiceException) e).tip);
            } else {
                e.printStackTrace();
                loginError(loginLog, "登录失败");
            }
        }
        return token;
    }


    private void loginError(SysLoginLog loginLog,String msg){
        loginLog.setLoginStatus(Constants.STATUS_FAIL);
        loginLog.setLoginMsg(msg);
        AsyncManager.me().execute(AsyncFactory.recordLoginInfo(loginLog));
        throw new ServiceException(msg);
    }

}
