package mobai.moran.system.security.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.system.entity.SysUser;
import mobai.moran.system.entity.vo.LoginUser;
import mobai.moran.system.service.SysUserService;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 用户验证处理
 * @author mobai
 */
@Slf4j
@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    SysUserService userService;

    /** 单独拿出来，避免产生互相调用问题 */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        QueryWrapper<SysUser> qw = new QueryWrapper<>();
        qw.eq("user_name",username);
        SysUser user =userService.getOne(qw);
        if (ObjectUtil.isEmpty(user)){
            log.info("登录用户：{} 不存在.", username);
            throw new UsernameNotFoundException("登录用户：" + username + " 不存在");
        }
        if(Constants.DELETED.equals(user.getDeleted())){
            log.info("登录用户：{} 已被删除.", username);
            throw new ServiceException("登录用户：" + username + " 不存在");
        }
        LoginUser loginUser = new LoginUser();
        loginUser.setUserId(user.getUserId());
        loginUser.setUserName(username);
        loginUser.setPassword(user.getPassword());
        loginUser.setStatus(user.getStatus());
        loginUser.setDeptId(user.getDeptId());
        loginUser.setNickName(user.getNickName());
        return loginUser;
    }
}
