package mobai.moran.system.security.service;

import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.system.entity.vo.LoginUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 自定义权限实现，ss取自SpringSecurity首字母
 * @author mobai
 */
@Service("ss")
public class PermissionService {

    /** 所有权限标识 */
    private static final String ALL_PERMISSION = "*:*:*";

    /** 管理员角色权限标识 */
    private static final String SUPER_ADMIN = "admin";

    /** 分隔符 */
    private static final String DELIMETER = ",";

    /**
     * 验证用户是否具备某权限
     * @param permit    权限标识
     * @return  用户是否具备某权限
     */
    public boolean hasPermit(String permit) {
        LoginUser user = SecurityUtil.getLoginUser();
        if (ObjectUtil.hasEmpty(permit,user) && !SecurityUtil.isAdmin(user.getUserId()) && ObjectUtil.isEmpty(user.getPermitList())){
            return false;
        }
        return hasPermit(user,permit);
    }

    /**
     * 验证用户是否具有以下任意一个权限
     * @param permits 以 PERMISSION_NAMES_DELIMETER 为分隔符的权限列表
     * @return 用户是否具有以下任意一个权限
     */
    public boolean hasAnyPermit(String permits) {
        LoginUser user = SecurityUtil.getLoginUser();
        if (ObjectUtil.hasEmpty(permits,user,user.getPermitList())){
            return false;
        }
        for (String permit : permits.split(DELIMETER)){
            if (hasPermit(user,permit)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断用户是否拥有某个角色
     * @param roleCode 角色编码
     * @return 用户是否具备某角色
     */
    public boolean hasRole(String roleCode) {
        LoginUser user = SecurityUtil.getLoginUser();
        if (ObjectUtil.hasEmpty(roleCode,user,user.getRoleList())){
            return false;
        }
        return hasRole(user,roleCode);
    }

    /**
     * 验证用户是否具有以下任意一个角色
     * @param roleCodes 以 ROLE_NAMES_DELIMETER 为分隔符的角色列表
     * @return 用户是否具有以下任意一个角色
     */
    public boolean hasAnyRole(String roleCodes){
        LoginUser user = SecurityUtil.getLoginUser();
        if (ObjectUtil.hasEmpty(roleCodes,user,user.getRoleList())){
            return false;
        }
        for (String roleCode : roleCodes.split(DELIMETER)){
            if (hasRole(user,roleCode)){
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否包含角色
     *
     * @param user 用户信息
     * @param roleCode 角色编码
     * @return 用户是否具备某角色
     */
    private boolean hasRole(LoginUser user, String roleCode) {
        return SecurityUtil.isAdmin(user.getUserId()) || user.getRoleList().contains(ALL_PERMISSION) || user.getRoleList().contains(StringUtils.trim(roleCode));
    }

    /**
     * 判断是否包含权限
     *
     * @param user 用户信息
     * @param permit 权限字符串
     * @return 用户是否具备某权限
     */
    private boolean hasPermit(LoginUser user, String permit) {
        return SecurityUtil.isAdmin(user.getUserId()) || user.getPermitList().contains(ALL_PERMISSION) || user.getPermitList().contains(StringUtils.trim(permit));
    }
}
