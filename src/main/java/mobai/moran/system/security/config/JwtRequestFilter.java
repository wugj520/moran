package mobai.moran.system.security.config;

import lombok.extern.slf4j.Slf4j;
import mobai.moran.system.entity.vo.LoginUser;
import mobai.moran.system.security.util.JwtUtil;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.common.util.ObjectUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * token过滤器 验证token有效性 <br>
 * 不能把jwt过滤交给spring管理，不然不会放行路径
 * @author mobai
 */
@Slf4j
public class JwtRequestFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String token = JwtUtil.getToken(request);
        if (ObjectUtil.isNotBlank(token)){
            // token检验成功，并且getAuthentication() 是null,那么就要重新搞一哈。。
            // 要是这个不重新获取，那么就会认证失败调用AuthenticationEntryPoint
            if (JwtUtil.validateToken(token) && SecurityContextHolder.getContext().getAuthentication() == null){
                LoginUser user = SecurityUtil.getLoginUser();
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }
        chain.doFilter(request, response);
    }
}
