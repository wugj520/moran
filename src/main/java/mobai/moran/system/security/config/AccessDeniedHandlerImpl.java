package mobai.moran.system.security.config;

import com.alibaba.fastjson.JSON;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.util.ServletUtil;
import mobai.moran.common.util.StringUtil;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 权限不足异常处理类
 * @author mobai
 */
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {
        String msg = StringUtil.format("请求访问：{}，认证失败，缺少访问权限", request.getRequestURI());
        ServletUtil.renderString(response, JSON.toJSONString(new ResultData(HttpStatus.FORBIDDEN.value(),msg)));
    }
}
