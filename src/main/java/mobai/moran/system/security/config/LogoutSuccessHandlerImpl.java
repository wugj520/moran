package mobai.moran.system.security.config;

import com.alibaba.fastjson.JSON;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.manager.AsyncFactory;
import mobai.moran.common.manager.AsyncManager;
import mobai.moran.system.entity.SysLoginLog;
import mobai.moran.system.util.LogUtil;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.common.util.ServletUtil;
import mobai.moran.system.entity.vo.LoginUser;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义退出处理
 * @author mobai
 */
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        LoginUser user = SecurityUtil.getLoginUser();
        if (ObjectUtil.isNotEmpty(user)){
            // 记录用户退出日志
            SysLoginLog login = LogUtil.createLoginLog(user.getUsername(), Constants.STATUS_SUCCESS, "退出成功");
            AsyncManager.me().execute(AsyncFactory.recordLoginInfo(login));
        }
        SecurityUtil.loginOut();
        if (ServletUtil.isAjaxRequest(request)){
            ServletUtil.renderString(response, JSON.toJSONString(new ResultData(HttpStatus.OK.value(),"退出成功")));
        }else {
            response.sendRedirect("login");
        }
    }
}
