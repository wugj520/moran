package mobai.moran.system.security.config;

import com.alibaba.fastjson.JSON;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.util.ServletUtil;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 认证失败处理类 返回未授权
 * <p>拒绝每个未经身份验证的请求并发送错误代码401。</p>
 * @author mobai
 */
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException
    {
        if (ServletUtil.isAjaxRequest(request)){
            //String msg = StringUtil.format("请求访问：{}，认证失败，无法访问系统资源，请重新登录", request.getRequestURI());
            ServletUtil.renderString(response, JSON.toJSONString(new ResultData(HttpStatus.UNAUTHORIZED.value(),"登录连接失效，请重新登录")));
        }else {
            response.setHeader("location", "/login");
            response.setStatus(302);
        }
    }
}
