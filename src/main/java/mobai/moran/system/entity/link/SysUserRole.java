package mobai.moran.system.entity.link;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.ModelEntity;

/**
 * 用户和角色的关联
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_user_role")
public class SysUserRole extends ModelEntity {

    /** 用户ID */
    private Long userId ;
    /** 角色ID */
    private Long roleId ;
}
