package mobai.moran.system.entity.link;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.ModelEntity;

/**
 * 角色和部门
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_role_dept")
public class SysRoleDept extends ModelEntity {

    /** 角色ID */
    private Long roleId ;
    /** 部门ID */
    private Long deptId ;
}
