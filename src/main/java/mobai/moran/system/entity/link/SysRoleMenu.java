package mobai.moran.system.entity.link;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.ModelEntity;

/**
 * 角色和菜单
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_role_menu")
public class SysRoleMenu extends ModelEntity {

    /** 角色ID */
    private Long roleId ;
    /** 菜单ID */
    private Long menuId ;
}
