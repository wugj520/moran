package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.validation.fieldRepeat.FieldRepeat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 字典类型
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_dict_type")
@FieldRepeat(fields = {"dict_type_name"}, message = "字典类型名称已存在，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
@FieldRepeat(fields = {"dict_type_code"}, message = "字典类型编码已存在，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
public class SysDictType extends BaseEntity {

    /** 字典类型ID */
    @TableId(value = "dict_type_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long dictTypeId ;

    /** 字典类型名称 */
    @TableField("dict_type_name")
    @NotBlank(message = "字典类型名称不能为空！！！", groups = {add.class,edit.class})
    private String dictTypeName ;

    /** 字典类型编码 */
    @TableField("dict_type_code")
    @NotBlank(message = "字典类型编码不能为空！！！", groups = {add.class,edit.class})
    private String dictTypeCode ;

    /** 字典备注 */
    @TableField(value = "dict_type_remark")
    private String dictTypeRemark ;

}
