package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.validation.fieldRepeat.FieldRepeat;
import mobai.moran.common.validation.paramIn.ParamIn;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * 部门信息
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_dept")
@FieldRepeat(fields = {"dept_name"}, message = "部门名称不能重复，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
public class SysDept extends BaseEntity {

    /** id */
    @TableId(value = "dept_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long deptId ;

    /** 部门名称 */
    @NotBlank(message = "部门名称不能为空！！！", groups = {add.class,edit.class})
    private String deptName ;

    /** 父ID */
    private Long parentId ;

    /** 祖级列表 */
    private String ancestors ;

    /** 部门区域 */
    private String deptArea ;

    /** 部门排序 */
    @NotNull(message = "部门排序不能为空！！！", groups = {add.class,edit.class})
    private Integer deptSort ;

    /** 部门负责人 */
    private String deptLeader ;

    /** 部门联系电话 */
    private String deptTel ;

    /** 部门邮箱 */
    private String deptEmail ;

    /** 部门状态 */
    @NotBlank(message = "部门状态不能为空！！！", groups = {add.class,edit.class})
    @ParamIn(message = "请选择正确状态", dictType = "sys_common_status")
    private String deptStatus ;

    /** 用于查询的部门访问权限列表 */
    @TableField(exist = false)
    private Set<Long> userDeptScope;

    @TableField(exist = false)
    private String hasUser;
}
