package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;

/**
 * 已读模块
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@TableName("sys_read")
@Data
public class SysRead extends BaseEntity {

    /** 字典ID */
    @TableId(value = "read_id" )
    private Long readId ;
    /** 连接类型 */
    private String linkType ;
    /** 连接ID */
    private Long linkId ;
    /** 内容 */
    private String content ;
    /** 状态 */
    private String status ;
}
