package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.ModelEntity;

import java.util.Date;

/**
 * 系统日志
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_operator_log")
public class SysOperatorLog extends ModelEntity {

    /** id */
    @TableId(value = "operator_id")
    private Long operatorId ;
    /** 标题 */
    private String operatorTitle ;
    /** 业务类型 */
    private String operatorType ;
    /** 方法名称 */
    private String operatorMethod ;
    /** 操作人 */
    private String operatorUser ;
    /** 操作ip */
    private String operatorIp ;
    /** 操作地点 */
    private String operatorLocation ;
    /** 请求方式 */
    private String requestWay ;
    /** 请求路径 */
    private String requestUrl ;
    /** 请求参数 */
    private String requestParam ;
    /** 返回参数 */
    private String resultParam ;
    /** 状态 */
    private String operatorStatus ;
    /** 操作提示 */
    private String operatorMsg ;

    /** 操作时间 */
    private Date operatorTime ;

    /** 删除标志 */
    @TableLogic
    private String deleted ;
}
