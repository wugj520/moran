package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.validation.fieldRepeat.FieldRepeat;
import mobai.moran.common.validation.paramIn.ParamIn;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@FieldRepeat(fields = {"user_name"}, message = "用户名已存在，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
@FieldRepeat(fields = {"nick_name","dept_id"}, message = "同部门下昵称不能重复，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
@TableName("sys_user")
public class SysUser extends BaseEntity {

    /** id */
    @TableId(value = "user_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long userId ;

    /** 用户名 */
    @NotBlank(message = "用户名不能为空！！！", groups = {add.class,edit.class})
    private String userName ;

    /** 昵称 */
    @NotBlank(message = "昵称不能为空！！！", groups = {add.class,edit.class})
    private String nickName ;

    /** 密码 */
    private String password ;

    /** 所属部门 */
    @NotNull(message = "所属部门不能为空！！！", groups = {add.class,edit.class})
    private Long deptId ;

    /** 邮箱 */
    private String email ;

    /** 手机 */
    private String phone ;

    /** 性别 */
    @ParamIn(message = "请选择正确性别", dictType = "sys_user_sex")
    private String sex ;

    /** 头像 */
    private String avatar ;

    /** 状态 */
    @NotBlank(message = "状态不能为空！！！", groups = {edit.class})
    @ParamIn(message = "请选择正确状态", dictType = "sys_common_status")
    private String status ;

    /** 最后次登录IP */
    private String lastLoginIp ;

    /** 最后次登录时间 */
    private Date lastLoginTime ;

    /** 最后次修改密码的时间 */
    private Date lastPwdUpdateTime ;

    /** 角色组 */
    @TableField(exist = false)
    private Long[] roleIds;
    /** 岗位组 */
    @TableField(exist = false)
    private Long[] postIds;
}
