package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.validation.fieldRepeat.FieldRepeat;
import mobai.moran.common.validation.paramIn.ParamIn;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 字典
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@TableName("sys_dict")
@Data
@FieldRepeat(fields = {"dict_name", "dict_type_code" ,"dict_parent_id"}, message = "字典名称已存在，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
@FieldRepeat(fields = {"dict_value", "dict_type_code","dict_parent_id"}, message = "字典值已存在，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
public class SysDict extends BaseEntity {

    /** 字典ID */
    @TableId(value = "dict_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long dictId ;

    /** 字典名称 */
    @TableField("dict_name")
    @NotBlank(message = "字典名称不能为空！！！", groups = {add.class,edit.class})
    private String dictName ;

    /** 字典值 */
    @TableField("dict_value")
    @NotBlank(message = "字典值不能为空！！！", groups = {add.class,edit.class})
    private String dictValue ;

    /** 字典类型编码 */
    @TableField("dict_type_code")
    @NotBlank(message = "字典类型编码不能为空！！！", groups = {add.class,edit.class,find.class})
    private String dictTypeCode ;

    /** 上级字典的id */
    @TableField("parent_id")
    private Long parentId ;

    /** 字典排序 */
    @TableField("dict_sort")
    @NotNull(message = "字典排序不能为空！！！", groups = {add.class,edit.class})
    private Integer dictSort ;

    /** 系统默认标识 */
    @TableField("default_flag")
    @NotBlank(message = "系统默认必填！！！", groups = {add.class,edit.class})
    @ParamIn(message = "请选择正确的系统默认", dictType = "sys_yes_no")
    private String defaultFlag ;

    /** 表格样式类型 */
    @TableField("table_style_type")
    private String tableStyleType ;

    /** 字典状态 */
    @TableField("dict_status")
    @NotBlank(message = "字典状态不能为空！！！", groups = {edit.class})
    @ParamIn(message = "请选择正确状态", dictType = "sys_common_status")
    private String dictStatus ;

}
