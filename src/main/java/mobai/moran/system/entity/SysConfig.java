package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.validation.fieldRepeat.FieldRepeat;
import mobai.moran.common.validation.paramIn.ParamIn;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 参数配置
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@TableName("sys_config")
@Data
@FieldRepeat(fields = {"config_name"}, message = "配置名称已存在，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
@FieldRepeat(fields = {"config_code"}, message = "配置编码已存在，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
public class SysConfig extends BaseEntity {

    /** 配置ID */
    @TableId(value = "config_id" , type = IdType.ASSIGN_ID )
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long configId ;
    /** 配置名称 */
    @TableField("config_name")
    @NotBlank(message = "配置名称不能为空！！！", groups = {add.class,edit.class})
    private String configName ;
    /** 配置编码 */
    @TableField("config_code")
    @NotBlank(message = "配置编码不能为空！！！", groups = {add.class,edit.class})
    private String configCode ;
    /** 配置属性值 */
    @TableField("config_value")
    @NotBlank(message = "配置属性值不能为空！！！", groups = {add.class,edit.class})
    private String configValue ;
    /** 是否系统参数 */
    @TableField("sys_flag")
    @NotBlank(message = "是否系统参数不能为空！！！", groups = {add.class,edit.class})
    @ParamIn(message = "请填写正确的是否系统参数字典", dictType = "sys_yes_no")
    private String sysFlag ;
    /** 配置备注 */
    @TableField("config_remark")
    private String configRemark ;
    /** 配置类型;来自字典的常量类型分类 */
    @TableField("config_type")
    private String configType ;
}
