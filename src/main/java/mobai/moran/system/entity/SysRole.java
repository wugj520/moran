package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;
import mobai.moran.common.validation.paramIn.ParamIn;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 角色信息
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_role")
public class SysRole extends BaseEntity {

    /** id */
    @TableId(value = "role_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long roleId ;

    /** 角色名称 */
    @NotBlank(message = "角色名称不能为空！！！", groups = {add.class,edit.class})
    private String roleName ;

    /** 角色编码 */
    @NotBlank(message = "角色编码不能为空！！！", groups = {add.class,edit.class})
    private String roleCode ;

    /** 角色排序 */
    @NotNull(message = "排序不能为空！！！", groups = {add.class,edit.class})
    private Integer roleSort ;

    /** 数据范围 */
    @NotBlank(message = "数据范围不能为空！！！", groups = {add.class,edit.class})
    @ParamIn(message = "请选择正确数据范围", dictType = "sys_data_scope")
    private String dataScope ;

    /** 角色状态 */
    @NotBlank(message = "角色状态不能为空！！！", groups = {edit.class})
    @ParamIn(message = "请选择正确状态", dictType = "sys_common_status")
    private String roleStatus ;

    /** 备注 */
    private String roleRemark ;

    /** 菜单组 */
    @TableField(exist = false)
    private Long[] menuIds;

    /** 部门组（数据权限） */
    @TableField(exist = false)
    private Long[] deptIds;

}
