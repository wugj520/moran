package mobai.moran.system.entity.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

/**
 * 登录用户身份权限
 * @author mobai
 */
@Data
@ToString
public class LoginUser implements UserDetails {

    /** uuid,区分每个登录链接 */
    private String uuid;
    /** 用户ID */
    private Long userId;
    /** 登录账号 */
    private String userName;
    /** 用户密码 */
    private String password;
    /** 记住我 */
    private Boolean rememberMe;

    /** 部门名称 */
    private String nickName;
    /** 部门Id */
    private Long deptId;
    /** 部门名称 */
    private String deptName;
    /** 用户状态 */
    private String status;
    /** ip地址 */
    private String ipaddr;
    /** 浏览器类型 */
    private String browser;
    /** 操作系统 */
    private String os;
    /** 登录时间 */
    private Date loginTime;
    /** 到期时间 */
    private Date expireTime;


    private Set<String> roleList;

    private Set<String> permitList;

    /** 是否拥有全部部门权限 */
    private Boolean hasAllDeptScope = false;
    /** 可访问部门列表 */
    private Set<Long> deptScope;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    /**
     * 账户是否未过期,过期无法验证
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 指定用户是否解锁,锁定的用户无法进行身份验证
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 密码是否未过期，过期的凭据防止认证
     */
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 是否激活
     */
    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }
}
