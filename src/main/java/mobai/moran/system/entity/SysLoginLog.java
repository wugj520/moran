package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.ModelEntity;

import java.util.Date;

/**
 * 登录日志
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_login_log")
public class SysLoginLog extends ModelEntity {

    @TableId(value = "login_id")
    private Long loginId ;

    /** 登录账号 */
    private String loginUserName ;

    /** 登录密码 */
    private String loginPassword ;

    /** 登录IP */
    private String loginIp ;

    /** 登录地点 */
    private String loginLocation ;

    /** 登录浏览器 */
    private String loginBrowser ;

    /** 登录系统 */
    private String loginOs ;

    /** 登录状态 */
    private String loginStatus ;

    /** 登录时间 */
    private Date loginTime ;

    /** 登录提示 */
    private String loginMsg ;

    /** 删除标志 */
    @TableLogic
    private String deleted ;
}
