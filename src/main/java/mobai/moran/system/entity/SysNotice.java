package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;
import mobai.moran.common.validation.paramIn.ParamIn;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 通知公告
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@TableName("sys_notice")
@Data
public class SysNotice extends BaseEntity {

    /** 字典ID */
    @TableId(value = "notice_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long noticeId ;

    /** 通知标题 */
    @TableField("notice_title")
    @NotBlank(message = "标题不能为空！！！", groups = {add.class,edit.class})
    private String noticeTitle ;

    /** 通知类型 */
    @TableField("notice_type")
    @NotBlank(message = "通知类型不能为空！！！", groups = {add.class,edit.class})
    @ParamIn(message = "请选择正确通知类型", dictType = "sys_notice_type")
    private String noticeType ;

    /** 通知内容 */
    @TableField("notice_content")
    @NotBlank(message = "内容不能为空！！！", groups = {add.class,edit.class})
    private String noticeContent ;

}
