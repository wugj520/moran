package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 菜单管理
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@TableName("sys_menu")
@Data
public class SysMenu extends BaseEntity {

    @TableId(value = "menu_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long menuId ;

    /** 标题 */
    @NotBlank(message = "标题不能为空！！！", groups = {add.class,edit.class})
    private String menuTitle ;

    /** 父ID */
    @NotNull(message = "上级不能为空！！！", groups = {add.class,edit.class})
    private Long parentId ;

    /** 显示排序 */
    @NotNull(message = "菜单排序不能为空！！！", groups = {add.class,edit.class})
    private Integer menuSort ;

    /** 菜单链接 */
    private String menuHref ;

    /** 打开方式 */
    private String target ;

    /** 菜单类型 */
    @NotBlank(message = "菜单类型不能为空！！！", groups = {add.class,edit.class})
    private String menuType ;

    /** 菜单状态 */
    @NotBlank(message = "菜单状态不能为空！！！", groups = {add.class,edit.class})
    private String menuStatus ;

    /** 权限标识 */
    private String permit ;

    /** 菜单图标 */
    private String menuIcon ;

    @TableField(exist = false)
    private Long userId;
    @TableField(exist = false)
    private String roleStatus;
}
