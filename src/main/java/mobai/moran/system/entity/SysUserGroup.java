package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.validation.fieldRepeat.FieldRepeat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 用户分组
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@FieldRepeat(fields = {"user_group_name","create_user","deleted"}, message = "分组名称已存在，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
@TableName("sys_user_group")
public class SysUserGroup extends ModelEntity {

    /** id */
    @TableId(value = "user_group_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long userGroupId;

    /** 用户组名称 */
    @NotBlank(message = "用户组名称不能为空！！！", groups = {add.class,edit.class})
    private String userGroupName;

    /** 用户组 */
    @NotBlank(message = "用户组不能为空！！！", groups = {add.class,edit.class})
    private String userGroup;

    /** 创建时间 */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;
    /** 创建人 */
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private Long createUser;
    /** 更新时间 */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
    /** 删除标识 */
    @TableField(value = "deleted", fill = FieldFill.INSERT)
    @TableLogic
    private String deleted ;

}
