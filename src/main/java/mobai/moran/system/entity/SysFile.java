package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.ModelEntity;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_file")
public class SysFile extends ModelEntity {


    /** id */
    @TableId(value = "file_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long fileId ;

    private Long parentId ;
    /** 文件名称 */
    private String fileName;
    /** 文件原名 */
    private String fileInitName;
    /** 文件路径 */
    private String fileUrl;
    /** 文件后缀 */
    private String fileSuffix;
    /** 文件类型 */
    private String fileType;
    /** 文件大小 */
    private Long fileSize;
    /** 文件来源：用于识别是哪里保存的 */
    private String fileSource;
    /** 文件保存类型：本地、七牛、阿里OSS？ */
    private String fileSaveType;

    private Long putUser;
    private Date putTime;

    /** 更新时间 */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
    /** 更新人 */
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private Long updateUser;

    /** 删除标识 */
    @TableField(value = "deleted", fill = FieldFill.INSERT)
    @TableLogic
    private String deleted ;
}
