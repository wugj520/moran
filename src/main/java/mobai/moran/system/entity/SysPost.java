package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;
import mobai.moran.common.validation.paramIn.ParamIn;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 岗位信息
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_post")
public class SysPost extends BaseEntity {

    /** id */
    @TableId(value = "post_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long postId ;

    /** 岗位名称 */
    @NotBlank(message = "岗位名称不能为空！！！", groups = {add.class,edit.class})
    private String postName ;

    /** 岗位编码 */
    @NotBlank(message = "岗位编码不能为空！！！", groups = {add.class,edit.class})
    private String postCode ;

    /** 显示顺序 */
    @NotNull(message = "显示顺序不能为空！！！", groups = {add.class,edit.class})
    private Integer postSort ;

    /** 岗位状态 */
    @NotBlank(message = "岗位状态不能为空！！！", groups = {add.class,edit.class})
    @ParamIn(message = "请选择正确状态", dictType = "sys_common_status")
    private String postStatus ;

    /** 备注 */
    private String remark ;
}
