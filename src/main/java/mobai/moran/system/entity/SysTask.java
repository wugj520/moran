package mobai.moran.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mobai.moran.common.base.BaseEntity;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.validation.fieldRepeat.FieldRepeat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 任务
 * @author mobai
 */
@EqualsAndHashCode(callSuper = true)
@Data
@FieldRepeat(fields = {"task_title","deleted"}, message = "任务标题已存在，请重新输入！",groups = {ModelEntity.add.class, ModelEntity.edit.class})
@TableName("sys_task")
public class SysTask extends BaseEntity {

    /** id */
    @TableId(value = "task_id")
    @NotNull(message = "非法操作，ID不能为空！！！", groups = {edit.class,del.class})
    private Long taskId ;

    /** 任务标题 */
    @NotBlank(message = "标题不能为空！！！", groups = {add.class,edit.class})
    private String taskTitle;
    /** 任务截止时间 */
    private Date taskCloseDate;
    /** 任务类型 */
    @NotBlank(message = "任务类型不能为空！！！", groups = {add.class,edit.class})
    private String taskType;
    /** 任务接收人员：以逗号分开的人员组 */
    @NotBlank(message = "接收人不能为空！！！", groups = {add.class,edit.class})
    private String taskUser;
    /** 任务描述 */
    private String taskNote;
    /** 任务派发清单字段 */
    private String taskDispatchField;
    /** 任务清单回复字段相关信息 */
    private String taskBackField;
    /** 任务状态 */
    private String taskStatus;
}
