package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysLoginLog;

/**
 * 登录日志 处理接口
 * @author mobai
 */
public interface SysLoginLogService extends BaseService<SysLoginLog> {

    /**
     * 清空
     */
    void clean();
}
