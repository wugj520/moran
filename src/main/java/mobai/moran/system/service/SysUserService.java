package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysUser;
import mobai.moran.system.entity.vo.LoginUser;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author mobai
 */
public interface SysUserService extends BaseService<SysUser> {

    /**
     * 登录用户信息补充
     * @param loginUser  登录用户
     */
    LoginUser loginUserReplenish(LoginUser loginUser);

    /**
     * 通过用户ID查询角色编码
     * @param userId    用户ID
     * @return  角色编码列表
     */
    Set<Long> getRoleIdsByUserId(Long userId);

    /**
     * 通过部门ID查询用户，返回格式为下拉样式
     * @param deptId    部门ID
     * @return  用户列表
     */
    List<Map<String,Object>> getUserByDeptIdForSelect (Long deptId);
}
