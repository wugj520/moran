package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysMenu;

import java.util.List;
import java.util.Map;

/**
 * @author mobai
 */
public interface SysMenuService extends BaseService<SysMenu> {

    /**
     * 根据用户ID查询菜单树
     * @param userId    用户ID
     * @return 菜单树
     */
    List<Map<String, Object>> selectMenuTreeByUserId(Long userId);
}
