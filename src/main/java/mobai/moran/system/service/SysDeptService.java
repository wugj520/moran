package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysDept;

/**
 * @author mobai
 */
public interface SysDeptService extends BaseService<SysDept> {

}
