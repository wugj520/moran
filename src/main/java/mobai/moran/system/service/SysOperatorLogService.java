package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysOperatorLog;

/**
 * 操作日志
 * @author mobai
 */
public interface SysOperatorLogService extends BaseService<SysOperatorLog> {

    /**
     * 清空
     */
    void clean();
}
