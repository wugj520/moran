package mobai.moran.system.service.impl;

import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.system.util.DictUtil;
import mobai.moran.system.entity.SysDict;
import mobai.moran.system.mapper.SysDictMapper;
import mobai.moran.system.service.SysDictService;
import org.springframework.stereotype.Service;


/**
 * 字典管理逻辑处理
 * @author mobai
 */
@Service
public class SysDictServiceImpl extends BaseServiceImpl<SysDictMapper, SysDict> implements SysDictService {

    @Override
    public void add(SysDict sysDict){
        this.save(sysDict);
        updateDictCache(sysDict.getDictTypeCode());
    }

    @Override
    public void edit(SysDict sysDict){
        if (sysDict.getDictId().equals(sysDict.getParentId())){
            throw new ServiceException("上级字典不能选自己");
        }
        this.updateById(sysDict);
        updateDictCache(sysDict.getDictTypeCode());
    }

    @Override
    public void del(SysDict sysDict){
        SysDict old = this.baseMapper.selectById(sysDict.getDictId());
        this.removeById(sysDict);
        updateDictCache(old.getDictTypeCode());
    }

    /**
     * 更新字典缓存
     */
    private void updateDictCache(String dictTypeCode){
        SysDict dict = new SysDict();
        dict.setDictTypeCode(dictTypeCode);
        DictUtil.set(dictTypeCode,this.baseMapper.list(dict));
    }
}
