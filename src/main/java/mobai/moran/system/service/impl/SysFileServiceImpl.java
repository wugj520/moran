package mobai.moran.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import lombok.SneakyThrows;
import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.file.FileConfig;
import mobai.moran.common.file.FileUtil;
import mobai.moran.common.file.MimeTypeUtil;
import mobai.moran.system.entity.SysFile;
import mobai.moran.system.mapper.SysFileMapper;
import mobai.moran.system.service.SysFileService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * @author mobai
 */
@Service
public class SysFileServiceImpl extends BaseServiceImpl<SysFileMapper, SysFile> implements SysFileService {

    @Resource
    UploadManager uploadManager;
    @Resource
    private BucketManager bucketManager;
    @Resource
    private Auth auth;


    @Override
    public ResultData upload(MultipartFile[] files, String source) {
        if (files == null || files.length == 0) {
            throw new ServiceException("文件不能为空");
        }
        for (MultipartFile file : files) {
            SysFile sf = null;
            if ("local".equals(FileConfig.getType())){
                sf = localUpload(file);
            }else if("qiniu".equals(FileConfig.getType())){
                sf = qiniuUpload(file);
            }else if("ali".equals(FileConfig.getType())){
                sf = aliUpload(file);
            }
            if (sf == null) {
                throw new ServiceException("文件保存失败");
            }
            sf.setFileSource(source);
        }
        return new ResultData();
    }

    @Override
    public void download(String url, HttpServletResponse response) {
        if ("local".equals(FileConfig.getType())){
            FileUtil.downLoad(url, FileConfig.getLocal().get("nginxUrl"), response);
        }else if("qiniu".equals(FileConfig.getType())){
            FileUtil.downLoad(url, FileConfig.getQiniu().get("path"), response);
        }else if("ali".equals(FileConfig.getType())){
            //
        }
    }

    @Override
    public void del(SysFile t){
        SysFile file = this.baseMapper.selectById(t);
        if (file == null) throw new ServiceException("文件删除失败，未找到该文件");

    }

    @SneakyThrows
    private SysFile localUpload(MultipartFile file) {
        SysFile sf = FileUtil.buildFileObj(file);
        File folder = new File(FileConfig.getLocal().get("uploadPath"));
        //目录不存在则创建
        if (!folder.isDirectory()) {
            folder.mkdirs();
        }
        file.transferTo(new File(folder + Constants.DIR_SPLIT + sf.getFileName()));
        // 返回上传文件的访问路径
        String url = FileConfig.getLocal().get("nginxUrl") + Constants.DIR_SPLIT + sf.getFileName();
        sf.setFileUrl(url);
        sf.setFileSaveType("local");
        return sf;
    }

    @SneakyThrows
    private SysFile qiniuUpload(MultipartFile file) {
        SysFile sf = FileUtil.buildFileObj(file);
        String upToken = auth.uploadToken(FileConfig.getQiniu().get("bucket"));
        Response response = uploadManager.put(file.getBytes(), sf.getFileName(), upToken);
        //解析上传成功的结果
        DefaultPutRet putRet = JSON.parseObject(response.bodyString(), DefaultPutRet.class);
        String url = FileConfig.getQiniu().get("path") + Constants.DIR_SPLIT + putRet.key;
        sf.setFileUrl(url);
        sf.setFileSaveType("qiniu");
        return sf;
    }

    @SneakyThrows
    private SysFile aliUpload(MultipartFile file) {
        SysFile sf = FileUtil.buildFileObj(file);
        OSS ossClient = getAliOSS();
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(MimeTypeUtil.getContentType(sf.getFileSuffix()));
        PutObjectRequest putObjectRequest = new PutObjectRequest(FileConfig.getAli().get("bucket"), sf.getFileName(),file.getInputStream());
        putObjectRequest.setMetadata(metadata);
        ossClient.putObject(putObjectRequest);
        String url = FileConfig.getAli().get("path") + Constants.DIR_SPLIT + sf.getFileName();
        sf.setFileUrl(url);
        ossClient.shutdown();
        sf.setFileSaveType("ali");
        return sf;
    }

    private OSS getAliOSS(){
        return new OSSClientBuilder().build(
                FileConfig.getAli().get("endpoint"),
                FileConfig.getAli().get("accessKey"),
                FileConfig.getAli().get("secretKey")
        );
    }
}
