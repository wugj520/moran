package mobai.moran.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.system.entity.SysDept;
import mobai.moran.system.mapper.SysDeptMapper;
import mobai.moran.system.service.SysDeptService;
import mobai.moran.system.entity.SysUser;
import mobai.moran.system.mapper.SysUserMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author mobai
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysDeptServiceImpl extends BaseServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    @Resource
    SysDeptMapper deptMapper;
    @Resource
    SysUserMapper userMapper;

    @Override
    public void add(SysDept dept){
        if (!dept.getParentId().equals(0L)){
            SysDept parent = deptMapper.selectById(dept.getParentId());
            if (Constants.STOP_USE.equals(parent.getDeptStatus())){
                throw new ServiceException("部门停用，不允许新增");
            }
        }
        dept.setAncestors(getAncestors(dept));
        super.add(dept);
    }

    @Override
    public void edit(SysDept dept){
        if (dept.getParentId().equals(dept.getDeptId())) {
            throw new ServiceException("修改失败!!!上级部门不能是自己");
        }
        QueryWrapper<ModelEntity> qw = new QueryWrapper<>();
        qw.eq("parent_id",dept.getDeptId());
        qw.eq("dept_status", Constants.STATUS_SUCCESS);
        if (Constants.STOP_USE.equals(dept.getDeptStatus()) && dept.selectCount(qw)>0){
            throw new ServiceException("该部门包含未停用的子部门!!!不能停用");
        }
        SysDept old = deptMapper.selectById(dept.getDeptId());
        if (ObjectUtil.isEmpty(old)){
            throw new ServiceException("数据查询失败!!!");
        }
        // 修改了父级部门，那么就要修祖级列表
        if (!old.getParentId().equals(dept.getParentId())){
            dept.setAncestors(getAncestors(dept));
            deptMapper.updateChildrenAncestors(dept.getDeptId(),dept.getAncestors(),old.getAncestors());
        }
        super.edit(dept);
        // 如果部门状态已修改为启用状态，那么更新所有父级为启用
        if (!old.getDeptStatus().equals(dept.getDeptStatus()) && Constants.IN_USE.equals(dept.getDeptStatus())){
            // 如果没有祖级列表，所以这里重新后台获取一哈
            if (ObjectUtil.isBlank(dept.getAncestors())){
                dept = super.getById(dept.getDeptId());
            }
            deptMapper.updateParentStatus(dept.getDeptStatus(),dept.getAncestors());
        }
    }

    @Override
    public void del(SysDept dept){
        QueryWrapper<ModelEntity> qw = new QueryWrapper<>();
        qw.eq("parent_id",dept.getDeptId());
        qw.eq("deleted", Constants.NO_DELETE);
        if (dept.selectCount(qw)>0){
            throw new ServiceException("存在下级部门,不允许删除");
        }
        qw.clear();
        QueryWrapper<SysUser> qwu = new QueryWrapper<>();
        qwu.eq("dept_id",dept.getDeptId());
        qwu.eq("deleted", Constants.NO_DELETE);
        if (userMapper.selectCount(qwu) > 0){
            throw new ServiceException("部门存在用户,不允许删除");
        }
        super.del(dept);
    }

    private String getAncestors(SysDept dept){
        if (dept.getParentId().equals(0L)){
            return "0";
        }
        SysDept parent = deptMapper.selectById(dept.getParentId());
        return parent.getAncestors() + "," + parent.getDeptId();
    }
}
