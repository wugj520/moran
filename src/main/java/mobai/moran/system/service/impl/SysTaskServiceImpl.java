package mobai.moran.system.service.impl;

import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.system.entity.SysTask;
import mobai.moran.system.mapper.SysTaskMapper;
import mobai.moran.system.service.SysTaskService;
import org.springframework.stereotype.Service;

/**
 * @author mobai
 */
@Service
public class SysTaskServiceImpl extends BaseServiceImpl<SysTaskMapper, SysTask> implements SysTaskService {

}
