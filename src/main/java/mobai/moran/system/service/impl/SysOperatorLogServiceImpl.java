package mobai.moran.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.common.constant.Constants;
import mobai.moran.system.entity.SysOperatorLog;
import mobai.moran.system.mapper.SysOperatorLogMapper;
import mobai.moran.system.service.SysOperatorLogService;
import org.springframework.stereotype.Service;

/**
 * @author mobai
 */
@Service
public class SysOperatorLogServiceImpl extends BaseServiceImpl<SysOperatorLogMapper, SysOperatorLog> implements SysOperatorLogService {

    @Override
    public void clean() {
        UpdateWrapper<SysOperatorLog> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("deleted", Constants.DELETED);
        super.update(updateWrapper);
    }
}
