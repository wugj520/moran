package mobai.moran.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.system.entity.SysDept;
import mobai.moran.system.entity.SysRole;
import mobai.moran.system.entity.vo.LoginUser;
import mobai.moran.system.entity.SysUser;
import mobai.moran.system.entity.link.SysUserRole;
import mobai.moran.system.mapper.SysDeptMapper;
import mobai.moran.system.mapper.SysUserMapper;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.system.service.SysUserService;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.system.util.ConfigUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * 用户管理
 * @author mobai
 */
@Service
@Slf4j
@Transactional(rollbackFor=Exception.class)
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Resource
    SysUserMapper userMapper;
    @Resource
    SysDeptMapper deptMapper;

    @Override
    public LoginUser loginUserReplenish(LoginUser loginUser){
        if (ObjectUtil.isEmpty(loginUser)){
            return loginUser;
        }
        if (SecurityUtil.isAdmin(loginUser.getUserId()) ){
            loginUser.setHasAllDeptScope(true);
        }
        List<SysRole> roleList = getUserRoleById(loginUser.getUserId());
        Set<String> roleCode = new HashSet<>();
        for (SysRole role : roleList){
            // 如果有所有部门权限，那么设定为true
            if (Constants.STATUS_SUCCESS.equals(role.getDataScope())){
                loginUser.setHasAllDeptScope(true);
            }
            roleCode.add(role.getRoleCode());
        }
        loginUser.setRoleList(roleCode);
        loginUser.setPermitList(getUserMenuPermitById(loginUser.getUserId()));
        SysDept dept = deptMapper.selectById(loginUser.getDeptId());
        if (dept!= null){
            loginUser.setDeptName(dept.getDeptName());
        }
        // 如果没有所有部门权限，那么一个个查角色所能查询的部门
        if (!loginUser.getHasAllDeptScope()){
            Set<Long> deptScope = new HashSet<>();
            for (SysRole role : roleList){
                switch (role.getDataScope()) {
                    case "2":
                        // 自定义部门
                        deptScope.addAll(userMapper.getDeptIdByRoleId(role.getRoleId()));
                        break;
                    case "3":
                        // 本部门权限
                        deptScope.add(dept.getDeptId());
                        break;
                    case "4":
                        // 部门及以下权限
                        deptScope.addAll(userMapper.getDeptIdById(dept.getDeptId()));
                        break;
                }
            }
            loginUser.setDeptScope(deptScope);
        }
        return loginUser;
    }

    @Override
    public void add(SysUser user){
        user.setPassword(SecurityUtil.encryptPassword(ConfigUtil.get("user_init_password")));
        user.setLastPwdUpdateTime(new Date());
        if (user.insert()){
            addUserRol(user);
        }
    }

    @Override
    public void edit(SysUser user){
        if (SecurityUtil.isAdmin(user.getUserId())){
            throw new ServiceException("不允许操作超级管理员用户");
        }
        if (userMapper.updateById(user) > 0){
            delUserRole(user.getUserId());
            addUserRol(user);
        }
    }

    @Override
    public void del(SysUser user){
        if (SecurityUtil.isAdmin(user.getUserId())){
            throw new ServiceException("不允许操作超级管理员用户");
        }
        SysUser u = new SysUser();
        u.setUserId(user.getUserId());
        u.setDeleted(Constants.DELETED);
        userMapper.updateById(u);
    }

    private void delUserRole(Long userId){
        SysUserRole ur = new SysUserRole();
        QueryWrapper<ModelEntity> qw = new QueryWrapper<>();
        qw.eq("user_id",userId);
        ur.delete(qw);
    }

    private void addUserRol(SysUser user){
        Long[] roleArr = user.getRoleIds();
        if (ObjectUtil.isNotEmpty(roleArr)){
            for (Long roleId : roleArr){
                SysUserRole ur = new SysUserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(roleId);
                ur.insert();
            }
        }
    }

    @Override
    public Set<Long> getRoleIdsByUserId (Long userId){
        return userMapper.getRoleIdByUserId(userId);
    }

    @Override
    public List<Map<String, Object>> getUserByDeptIdForSelect(Long deptId) {
        return userMapper.getUserByDeptIdForSelect(deptId);
    }

    /**
     * 通过用户ID查询角色
     * @param userId    用户ID
     * @return 角色列表
     */
    public List<SysRole> getUserRoleById(Long userId) {
        return userMapper.getUserRoleById(userId);
    }

    /**
     * 通过用户ID查询用户菜单权限
     * @param userId    用户ID
     * @return 菜单权限列表
     */
    public Set<String> getUserMenuPermitById(Long userId) {
        return userMapper.getUserMenuPermitById(userId);
    }
}
