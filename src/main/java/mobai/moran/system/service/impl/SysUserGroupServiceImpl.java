package mobai.moran.system.service.impl;

import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.system.entity.SysUserGroup;
import mobai.moran.system.mapper.SysUserGroupMapper;
import mobai.moran.system.service.SysUserGroupService;
import org.springframework.stereotype.Service;

/**
 * @author mobai
 */
@Service
public class SysUserGroupServiceImpl extends BaseServiceImpl<SysUserGroupMapper, SysUserGroup> implements SysUserGroupService {

}
