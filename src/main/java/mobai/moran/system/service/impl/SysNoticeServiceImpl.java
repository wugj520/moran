package mobai.moran.system.service.impl;

import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.system.entity.SysNotice;
import mobai.moran.system.mapper.SysNoticeMapper;
import mobai.moran.system.service.SysNoticeService;
import org.springframework.stereotype.Service;

/**
 * @author mobai
 */
@Service
public class SysNoticeServiceImpl extends BaseServiceImpl<SysNoticeMapper, SysNotice> implements SysNoticeService {

}
