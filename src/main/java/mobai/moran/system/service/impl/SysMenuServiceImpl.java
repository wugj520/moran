package mobai.moran.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.TreeUtil;
import mobai.moran.system.entity.SysMenu;
import mobai.moran.system.mapper.SysMenuMapper;
import mobai.moran.system.service.SysMenuService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author mobai
 */
@Service
public class SysMenuServiceImpl extends BaseServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {


    @Override
    public void add(SysMenu menu){
        super.add(menu);
        // 如果是菜单
        if (menu.getMenuType().equals("C")){
            int num = menu.getMenuHref().lastIndexOf("/");
            String s1 = ""+menu.getMenuHref();
            String s2 = s1.substring(0,num).replaceAll("/",":");
            menu.setMenuType("F");
            menu.setParentId(menu.getMenuId());
            menu.setMenuHref(null);

            menu.setMenuId(null);
            menu.setMenuTitle("查询");
            menu.setMenuSort(1);
            menu.setPermit(s2+":find");
            menu.insert();

            menu.setMenuId(null);
            menu.setMenuTitle("新增");
            menu.setMenuSort(2);
            menu.setPermit(s2+":add");
            menu.insert();

            menu.setMenuId(null);
            menu.setMenuTitle("编辑");
            menu.setMenuSort(3);
            menu.setPermit(s2+":edit");
            menu.insert();

            menu.setMenuId(null);
            menu.setMenuTitle("删除");
            menu.setMenuSort(4);
            menu.setPermit(s2+":del");
            menu.insert();

            menu.setMenuId(null);
            menu.setMenuTitle("导出");
            menu.setMenuSort(5);
            menu.setPermit(s2+":export");
            menu.insert();
        }
    }

    @Override
    public void edit(SysMenu menu){
        if (menu.getParentId().equals(menu.getMenuId())) {
            throw new ServiceException("删除失败!!!上级菜单不能是自己");
        }
        super.edit(menu);
    }

    @Override
    public void del(SysMenu menu){
        QueryWrapper<SysMenu> qw = new QueryWrapper<>();
        qw.eq("parent_id",menu.getMenuId());
        qw.eq("deleted",Constants.NO_DELETE);
        if (this.baseMapper.selectCount(qw)>0){
            throw new ServiceException("修改失败!!!还有下级菜单在用");
        }
        super.removeById(menu);
    }

    @Override
    public List<Map<String, Object>> selectMenuTreeByUserId(Long userId) {
        SysMenu menu = new SysMenu();
        // 不是超级管理员，所以需要用户角色在用
        if (userId != 1){
            menu.setUserId(userId);
            menu.setRoleStatus(Constants.IN_USE);
        }
        menu.setMenuStatus(Constants.IN_USE);
        menu.setMenuType("'M','C'");
        List<Map<String,Object>> menuList = this.findList(menu);
        return TreeUtil.getChild(menuList,"0","menuId");
    }
}
