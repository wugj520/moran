package mobai.moran.system.service.impl;

import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.system.util.ConfigUtil;
import mobai.moran.system.entity.SysConfig;
import mobai.moran.system.mapper.SysConfigMapper;
import mobai.moran.system.service.SysConfigService;
import mobai.moran.common.exception.ServiceException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;

/**
 * @author mobai
 */
@Service
public class SysConfigServiceImpl extends BaseServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {

    /**
     * 项目启动时，初始化参数到缓存
     */
    @PostConstruct
    public void initConfigCache() {
        List<SysConfig> configList = this.baseMapper.selectByMap(new HashMap<>());
        for (SysConfig config : configList){
            ConfigUtil.set(config.getConfigCode(),config.getConfigValue());
        }
    }

    @Override
    public void add(SysConfig sysConfig){
        this.save(sysConfig);
        ConfigUtil.set(sysConfig.getConfigCode(),sysConfig.getConfigValue());
    }

    @Override
    public void edit(SysConfig sysConfig){
        SysConfig old = this.getById(sysConfig.getConfigId());
        if (!old.getConfigCode().equals(sysConfig.getConfigCode())){
            throw new ServiceException("参数编码不能修改");
        }
        this.updateById(sysConfig);
        ConfigUtil.set(sysConfig.getConfigCode(),sysConfig.getConfigValue());
    }

}
