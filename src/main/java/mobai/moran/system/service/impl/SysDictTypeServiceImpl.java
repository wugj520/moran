package mobai.moran.system.service.impl;

import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.system.util.DictUtil;
import mobai.moran.system.entity.SysDict;
import mobai.moran.system.entity.SysDictType;
import mobai.moran.system.mapper.SysDictMapper;
import mobai.moran.system.mapper.SysDictTypeMapper;
import mobai.moran.system.service.SysDictTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * 字典类型业务处理
 * @author mobai
 */
@Service
public class SysDictTypeServiceImpl extends BaseServiceImpl<SysDictTypeMapper, SysDictType> implements SysDictTypeService {

    @Resource
    SysDictMapper dictMapper;

    /**
     * 项目启动时，初始化字典到缓存
     */
    @PostConstruct
    public void initDictCache(){
        List<SysDictType> dictTypeList = this.baseMapper.selectByMap(new HashMap<>());
        SysDict dict = new SysDict();
        for (SysDictType dictType : dictTypeList){
            dict.setDictTypeCode(dictType.getDictTypeCode());
            DictUtil.set(dictType.getDictTypeCode(),dictMapper.list(dict));
        }
    }

    @Override
    public void edit(SysDictType sysDictType){
        SysDictType old = this.getById(sysDictType.getDictTypeId());
        super.edit(sysDictType);
        // 字典类型编码有变化，更新字典缓存
        if (!old.getDictTypeCode().equals(sysDictType.getDictTypeCode())){
            DictUtil.remove(old.getDictTypeCode());
            SysDict dict = new SysDict();
            dict.setDictTypeCode(sysDictType.getDictTypeCode());
            DictUtil.set(sysDictType.getDictTypeCode(),dictMapper.list(dict));
        }
    }
}
