package mobai.moran.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.system.entity.SysRole;
import mobai.moran.system.entity.link.SysRoleDept;
import mobai.moran.system.entity.link.SysRoleMenu;
import mobai.moran.system.mapper.SysRoleMapper;
import mobai.moran.system.service.SysRoleService;
import mobai.moran.system.entity.link.SysUserRole;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Map;

/**
 * @author mobai
 */
@Service
@Transactional(rollbackFor=Exception.class)
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Resource
    SysRoleMapper roleMapper;

    @Override
    public Map<String, Object> detail(Serializable id){
        Map<String, Object> role = super.detail(id);
        role.put("menuIds",roleMapper.selectMenuIdsByRoleId((Long) id));
        role.put("deptIds",roleMapper.selectDeptIdsByRoleId((Long) id));
        return role;
    }

    @Override
    public void add(SysRole sysRole) {
        super.add(sysRole);
        addRoleDept(sysRole);
        addRoleMenu(sysRole);
    }

    @Override
    public void edit(SysRole sysRole) {
        super.edit(sysRole);
        addRoleDept(sysRole);
        addRoleMenu(sysRole);
    }

    @Override
    public void del(SysRole sysRole) {
        SysUserRole ur = new SysUserRole();
        QueryWrapper<ModelEntity> qw = new QueryWrapper<>();
        qw.eq("role_id",sysRole.getRoleId());
        if (ur.selectCount(qw) > 0){
            throw new ServiceException("角色已分配,不能删除");
        }
        sysRole.deleteById();
        addRoleDept(sysRole);
        addRoleMenu(sysRole);
    }

    /**
     * 新增角色部门信息(数据权限)
     * @param role 角色对象
     */
    public void addRoleDept(SysRole role){
        SysRoleDept rd = new SysRoleDept();
        QueryWrapper<ModelEntity> qw = new QueryWrapper<>();
        qw.eq("role_id",role.getRoleId());
        rd.delete(qw);
        if (ObjectUtil.isNotEmpty(role.getDeptIds())){
            rd.setRoleId(role.getRoleId());
            for (Long deptId : role.getDeptIds()){
                rd.setDeptId(deptId);
                rd.insert();
            }
        }
    }

    /**
     * 新增角色菜单信息
     * @param role 角色对象
     */
    public void addRoleMenu(SysRole role){
        SysRoleMenu rm = new SysRoleMenu();
        QueryWrapper<ModelEntity> qw = new QueryWrapper<>();
        qw.eq("role_id",role.getRoleId());
        rm.delete(qw);
        if (ObjectUtil.isNotEmpty(role.getMenuIds())){
            rm.setRoleId(role.getRoleId());
            for (Long menuId : role.getMenuIds()){
                rm.setMenuId(menuId);
                rm.insert();
            }
        }
    }
}
