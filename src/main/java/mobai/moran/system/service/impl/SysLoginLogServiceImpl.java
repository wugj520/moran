package mobai.moran.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import mobai.moran.common.base.BaseServiceImpl;
import mobai.moran.common.constant.Constants;
import mobai.moran.system.entity.SysLoginLog;
import mobai.moran.system.mapper.SysLoginLogMapper;
import mobai.moran.system.service.SysLoginLogService;
import org.springframework.stereotype.Service;

/**
 * @author mobai
 */
@Service
public class SysLoginLogServiceImpl extends BaseServiceImpl<SysLoginLogMapper, SysLoginLog> implements SysLoginLogService {

    @Override
    public void clean() {
        UpdateWrapper<SysLoginLog> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("deleted", Constants.DELETED);
        super.update(updateWrapper);
    }
}
