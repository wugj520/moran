package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysNotice;

/**
 * @author mobai
 */
public interface SysNoticeService extends BaseService<SysNotice> {

}
