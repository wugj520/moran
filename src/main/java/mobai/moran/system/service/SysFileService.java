package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.system.entity.SysFile;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @author mobai
 */
public interface SysFileService extends BaseService<SysFile> {

    /**
     * 上传文件
     */
    ResultData upload(MultipartFile[] files, String source);

    /**
     * 下载文件
     */
    void download(String url, HttpServletResponse response);
}
