package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysTask;

/**
 * @author mobai
 */
public interface SysTaskService extends BaseService<SysTask> {

}
