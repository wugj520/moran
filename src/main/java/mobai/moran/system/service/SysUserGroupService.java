package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysUserGroup;

/**
 * @author mobai
 */
public interface SysUserGroupService extends BaseService<SysUserGroup> {

}
