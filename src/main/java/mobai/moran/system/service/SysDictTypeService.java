package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysDictType;


/**
 * 字典类型管理
 * @author mobai
 */
public interface SysDictTypeService extends BaseService<SysDictType> {

}
