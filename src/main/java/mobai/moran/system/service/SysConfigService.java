package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysConfig;

/**
 * @author mobai
 */
public interface SysConfigService extends BaseService<SysConfig> {

}
