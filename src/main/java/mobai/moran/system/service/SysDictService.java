package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysDict;

/**
 * 字典管理
 * @author mobai
 */
public interface SysDictService extends BaseService<SysDict> {

}
