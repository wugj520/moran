package mobai.moran.system.service;

import mobai.moran.common.base.BaseService;
import mobai.moran.system.entity.SysRole;

/**
 *
 * @author mobai
 */
public interface SysRoleService extends BaseService<SysRole> {

}
