package mobai.moran.system.controller;

import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysRole;
import mobai.moran.system.service.SysRoleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 角色管理
 * @author mobai
 */
@Controller
@RequestMapping("/system/role")
public class SysRoleController {

    @Resource
    SysRoleService roleService;

    @GetMapping("/index")
    public String index(){
        return "system/role/index";
    }
    @GetMapping("/form")
    public String form(){
        return "system/role/form";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:role:find')")
    @PostMapping("/list")
    public PageData list(SysRole role){
        return roleService.findPageList(role);
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:role:find')")
    @PostMapping("/info/{id}")
    public ResultData info(@PathVariable(value = "id") Long id){
        return new ResultData(roleService.detail(id));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:role:add')")
    @Log(moduleName = "角色管理", OperatorType = OperatorType.ADD)
    @PostMapping("/add")
    public ResultData add(@Validated(ModelEntity.add.class) SysRole role){
        roleService.add(role);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:role:edit')")
    @Log(moduleName = "角色管理", OperatorType = OperatorType.EDIT)
    @PostMapping("/edit")
    public ResultData edit(@Validated(ModelEntity.edit.class) SysRole role){
        roleService.edit(role);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:role:del')")
    @Log(moduleName = "角色管理", OperatorType = OperatorType.DEL)
    @PostMapping("/del")
    public ResultData del(@Validated(ModelEntity.del.class) SysRole role){
        roleService.del(role);
        return new ResultData();
    }
}
