package mobai.moran.system.controller;

import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.util.TreeUtil;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysDept;
import mobai.moran.system.entity.vo.LoginUser;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.system.service.SysDeptService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author mobai
 */
@Controller
@RequestMapping("/system/dept")
public class SysDeptController  {

    @Resource
    private SysDeptService deptService;

    @GetMapping("/index")
    public String index(){
        return "system/dept/index";
    }

    @GetMapping("/form")
    public String form(){
        return "system/dept/form";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dept:find')")
    @PostMapping("/list")
    public PageData list(SysDept dept){
        return deptService.findPageList(dept);
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dept:find')")
    @PostMapping("/tree")
    public ResultData tree(){
        LoginUser user = SecurityUtil.getLoginUser();
        SysDept dept = new SysDept();
        if (!SecurityUtil.isAdmin(user.getUserId()) && !user.getHasAllDeptScope()){
            dept.setUserDeptScope(user.getDeptScope());
        }
        return new ResultData(TreeUtil.getChild(deptService.findList(dept),"deptId"));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dept:find')")
    @PostMapping("/info/{id}")
    public ResultData info(@PathVariable(value = "id") Long id){
        return new ResultData(deptService.detail(id));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dept:add')")
    @Log(moduleName = "部门管理", OperatorType = OperatorType.ADD)
    @PostMapping("/add")
    public ResultData add(@Validated(ModelEntity.add.class) SysDept dept){
        deptService.add(dept);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dept:edit')")
    @Log(moduleName = "部门管理", OperatorType = OperatorType.EDIT)
    @PostMapping("/edit")
    public ResultData edit(@Validated(ModelEntity.edit.class) SysDept dept){
        deptService.edit(dept);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dept:del')")
    @Log(moduleName = "部门管理", OperatorType = OperatorType.DEL)
    @PostMapping("/del")
    public ResultData del(@Validated(ModelEntity.del.class) SysDept dept){
        deptService.del(dept);
        return new ResultData();
    }
}
