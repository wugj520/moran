package mobai.moran.system.controller;

import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysNotice;
import mobai.moran.system.service.SysNoticeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 通知公告
 * @author mobai
 */
@Controller
@RequestMapping("/system/notice")
public class SysNoticeController {

    @Resource
    SysNoticeService noticeService;

    @GetMapping("/index")
    public String index(){
        return "system/notice/index";
    }

    @GetMapping("/form")
    public String form(){
        return "system/notice/form";
    }

    @GetMapping("/view")
    public String view(){
        return "system/notice/view";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:notice:find')")
    @PostMapping("/list")
    public PageData list(SysNotice notice){
        return noticeService.findPageList(notice);
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:notice:find')")
    @PostMapping("/info/{id}")
    public ResultData info(@PathVariable(value = "id") Long id){
        return new ResultData(noticeService.detail(id));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:notice:add')")
    @Log(moduleName = "通知公告管理", OperatorType = OperatorType.ADD)
    @PostMapping("/add")
    public ResultData add(@Validated(ModelEntity.add.class) SysNotice notice){
        noticeService.add(notice);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:notice:edit')")
    @Log(moduleName = "通知公告管理", OperatorType = OperatorType.EDIT)
    @PostMapping("/edit")
    public ResultData edit(@Validated(ModelEntity.edit.class) SysNotice notice){
        noticeService.edit(notice);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:notice:del')")
    @Log(moduleName = "通知公告管理", OperatorType = OperatorType.DEL)
    @PostMapping("/del")
    public ResultData del(@Validated(ModelEntity.del.class) SysNotice notice){
        noticeService.del(notice);
        return new ResultData();
    }
}
