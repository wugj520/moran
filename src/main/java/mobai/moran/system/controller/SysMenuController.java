package mobai.moran.system.controller;

import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.util.TreeUtil;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysMenu;
import mobai.moran.system.service.SysMenuService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 菜单管理
 * @author mobai
 */
@Controller
@RequestMapping("/system/menu")
public class SysMenuController {

    @Resource
    SysMenuService menuService;

    @GetMapping("/index")
    public String index(){
        return "system/menu/index";
    }
    @GetMapping("/form")
    public String form(){
        return "system/menu/form";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:menu:find')")
    @PostMapping("/list")
    public ResultData list(SysMenu pojo){
        return new ResultData(menuService.findList(pojo));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:menu:find')")
    @PostMapping("/tree")
    public ResultData tree(SysMenu pojo){
        return new ResultData(TreeUtil.getChild(menuService.findList(pojo),"menuId"));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:menu:find')")
    @PostMapping("/info/{id}")
    public ResultData info(@PathVariable(value = "id") Long id){
        return new ResultData(menuService.detail(id));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:menu:add')")
    @Log(moduleName = "菜单管理", OperatorType = OperatorType.ADD)
    @PostMapping("/add")
    public ResultData add(@Validated(ModelEntity.add.class) SysMenu pojo){
        menuService.add(pojo);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:menu:edit')")
    @Log(moduleName = "菜单管理", OperatorType = OperatorType.EDIT)
    @PostMapping("/edit")
    public ResultData edit(@Validated(ModelEntity.edit.class) SysMenu pojo){
        menuService.edit(pojo);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dept:del')")
    @Log(moduleName = "菜单管理", OperatorType = OperatorType.DEL)
    @PostMapping("/del")
    public ResultData del(@Validated(ModelEntity.del.class) SysMenu pojo){
        menuService.del(pojo);
        return new ResultData();
    }
}
