package mobai.moran.system.controller;

import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysLoginLog;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.system.service.SysLoginLogService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 登录日志
 * @author mobai
 */
@Controller
@RequestMapping("/system/log/login")
public class SysLoginLogController {

    @Resource
    private SysLoginLogService loginLogService;

    @GetMapping("/index")
    public String index(){
        return "system/log/login_index";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:login:find')")
    @PostMapping("/list")
    public PageData list(SysLoginLog log){
        return loginLogService.findPageList(log);
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:login:unlock')")
    @Log(moduleName = "登录日志", OperatorType = OperatorType.DEL)
    @PostMapping("/unlock/{username}")
    public ResultData unlockUser(@PathVariable(value = "username") String username){
        SecurityUtil.delPwdError(username);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:login:del')")
    @Log(moduleName = "登录日志", OperatorType = OperatorType.DEL)
    @PostMapping("/del")
    public ResultData del(SysLoginLog log){
        loginLogService.del(log);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:login:del')")
    @Log(moduleName = "登录日志", OperatorType = OperatorType.CLEAN)
    @PostMapping("/clean")
    public ResultData clean(){
        loginLogService.clean();
        return new ResultData();
    }
}
