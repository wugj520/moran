package mobai.moran.system.controller;

import mobai.moran.common.base.vo.LoginRequest;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.cache.redis.RedisUtil;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.common.util.ObjectUtil;
import mobai.moran.common.util.StringUtil;
import mobai.moran.common.util.TreeUtil;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysDept;
import mobai.moran.system.entity.SysUser;
import mobai.moran.system.security.service.LoginService;
import mobai.moran.system.service.SysDeptService;
import mobai.moran.system.service.SysMenuService;
import mobai.moran.system.service.SysUserService;
import mobai.moran.system.util.ConfigUtil;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.system.entity.vo.LoginUser;
import mobai.moran.system.util.DictUtil;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author mobai
 */
@Controller
public class LoginController {

    @Resource
    LoginService loginService;

    @Resource
    private SysMenuService menuService;

    @Resource
    SysUserService userService;
    @Resource
    SysDeptService deptService;

    /** 登录页 */
    @GetMapping("/login")
    public String login(){
        boolean hasLogin = SecurityUtil.hasLogin();
        if (hasLogin) {
            return "redirect:index";
        }
        return "login";
    }

    /** 主页 */
    @GetMapping("/index")
    public String index(){
        if (!SecurityUtil.hasLogin()) {
            return "redirect:login";
        }
        return "index";
    }

    @GetMapping("/home")
    public String home(){
        return "home";
    }
    @GetMapping("/pwdView")
    public String pwdView(){
        return "other/pwd_form";
    }

    @ResponseBody
    @PostMapping("/login")
    public ResultData login(@Validated LoginRequest login){
        if (SecurityUtil.loginIsBan(login.getUsername())){
            throw new ServiceException("密码错误次数过多，账号暂时被锁定，请一小时以后再试");
        }
        // 验证码验证
        if (!ObjectUtil.hasBlank(login.getVerKey(),login.getVerCode())){
            if (ObjectUtil.isBlank(login.getVerCode())){
                return new ResultData(233,"请输入验证码");
            }
            String captchaCode = RedisUtil.get(Constants.CAPTCHA_CODE_KEY + login.getVerKey()).toString();
            RedisUtil.remove(Constants.CAPTCHA_CODE_KEY + login.getVerKey());
            if (ObjectUtil.isBlank(captchaCode)){
                return new ResultData(233,"验证码已失效");
            }
            if (captchaCode.equals(login.getVerCode())){
                return new ResultData(233,"验证码错误");
            }
        }
        String token = loginService.login(login);
        return new ResultData(token);
    }

    @ResponseBody
    @RequestMapping("index/init")
    public ResultData init(){
        LoginUser user = SecurityUtil.getLoginUser();
        Map<String,Object> data = new HashMap<>();
        //
        data.put("homePath", "home");
        // 获取用户菜单
        data.put("menuList",menuService.selectMenuTreeByUserId(user.getUserId()));
        return new ResultData(data);
    }

    @ResponseBody
    @RequestMapping("cache")
    public ResultData cache(){
        LoginUser user = SecurityUtil.getLoginUser();
        Map<String, Object> data = new HashMap<>();
        data.put("loginUser", user);
        data.put("dict", DictUtil.getDictList());
        data.put("dictTree", DictUtil.getDictTree());
        SysDept dept = new SysDept();
        List<Map<String, Object>> deptAllList = deptService.findList(dept);
        data.put("dept", deptAllList);
        List<Map<String, Object>> deptAllList2 = deptService.findList(dept);
        List<Map<String, Object>> deptAllTree = TreeUtil.getChild(deptAllList2,"deptId");
        data.put("deptTree", deptAllTree);
        if (SecurityUtil.isAdmin(user.getUserId()) || user.getHasAllDeptScope()){
            data.put("deptScopeTree", deptAllTree);
        }else {
            dept.setUserDeptScope(user.getDeptScope());
            data.put("deptScopeTree", TreeUtil.getChild(deptService.findList(dept),"deptId"));
        }
        return new ResultData(data);
    }

    @ResponseBody
    @Log(moduleName = "密码修改", OperatorType = OperatorType.EDIT)
    @RequestMapping("updatePwd")
    public ResultData updatePwd(@RequestParam(value = "newPassword") String newPassword, @RequestParam(value = "password") String password){
        String pwdLv = ConfigUtil.get("user_password_level");
        if (SecurityUtil.pwdSafeLv(newPassword) < Integer.parseInt(pwdLv)){
            return new ResultData(222,"密码强度不够，请使用数字、小写字母、大写字母、特殊符号、中文进行组合");
        }
        if (newPassword.length() > 16) {
            return new ResultData(222,"密码长度不能超过16位数");
        }
        if (newPassword.matches(StringUtil.EMPTY_STR_REX)) {
            return new ResultData(222,"密码不能包含空格");
        }

        LoginUser loginUser = SecurityUtil.getLoginUser();
        SysUser user = userService.getById(loginUser.getUserId());
        if (!SecurityUtil.matchesPassword(password,user.getPassword())){
            return new ResultData(222,"密码错误！！！无法修改");
        }
        user.setPassword(SecurityUtil.encryptPassword(newPassword));
        user.setLastPwdUpdateTime(new Date());
        user.updateById();
        SecurityUtil.loginOut();
        return new ResultData(401,"修改成功");
    }
}
