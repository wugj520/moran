package mobai.moran.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysUserGroup;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.system.service.SysUserGroupService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 用户组
 * @author mobai
 */
@Controller
@RequestMapping("/system/userGroup")
public class SysUserGroupController {

    @Resource
    SysUserGroupService userGroupService;

    @GetMapping("/form")
    public String form(){
        return "system/userGroup/form";
    }

    @ResponseBody
    @PostMapping("/list")
    public PageData list(SysUserGroup userGroup){
        userGroup.setCreateUser(SecurityUtil.getLoginUser().getUserId());
        return userGroupService.findPageList(userGroup);
    }

    @ResponseBody
    @PostMapping("/info/{id}")
    public ResultData info(@PathVariable(value = "id") Long id){
        return new ResultData(userGroupService.detail(id));
    }

    @ResponseBody
    @Log(moduleName = "用户组管理", OperatorType = OperatorType.ADD)
    @PostMapping("/add")
    public ResultData add(@Validated(ModelEntity.add.class) SysUserGroup userGroup){
        userGroupService.add(userGroup);
        return new ResultData();
    }

    @ResponseBody
    @Log(moduleName = "用户组管理", OperatorType = OperatorType.EDIT)
    @PostMapping("/edit")
    public ResultData edit(@Validated(ModelEntity.edit.class) SysUserGroup userGroup){
        check(userGroup.getUserGroupId());
        userGroupService.edit(userGroup);
        return new ResultData();
    }

    @ResponseBody
    @Log(moduleName = "用户组管理", OperatorType = OperatorType.DEL)
    @PostMapping("/del")
    public ResultData del(@Validated(ModelEntity.del.class) SysUserGroup userGroup){
        check(userGroup.getUserGroupId());
        userGroupService.del(userGroup);
        return new ResultData();
    }

    /**
     * 检验用户是否有权限操作该用户组
     * @param userGroupId 用户组ID
     */
    private void check(Long userGroupId){
        QueryWrapper<SysUserGroup> qw = new QueryWrapper<>();
        qw.eq("create_user",SecurityUtil.getLoginUser().getUserId());
        qw.eq("user_group_id",userGroupId);
        if (userGroupService.count(qw) != 1){
            throw new ServiceException("异常操作，没有权限操作该用户组");
        }
    }
}
