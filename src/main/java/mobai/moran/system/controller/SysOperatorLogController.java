package mobai.moran.system.controller;

import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysOperatorLog;
import mobai.moran.system.service.SysOperatorLogService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 操作日志
 * @author mobai
 */
@Controller
@RequestMapping("/system/log/operator")
public class SysOperatorLogController {

    @Resource
    private SysOperatorLogService operatorLogService;

    @GetMapping("/index")
    public String index(){
        return "system/log/operator_index";
    }
    @GetMapping("/view")
    public String view(){
        return "system/log/operator_view";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:operator:find')")
    @PostMapping("/list")
    public PageData list(SysOperatorLog log){
        return operatorLogService.findPageList(log);
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:operator:find')")
    @PostMapping("/info/{id}")
    public ResultData info(@PathVariable(value = "id") Long id){
        return new ResultData(operatorLogService.detail(id));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:operator:del')")
    @Log(moduleName = "操作日志", OperatorType = OperatorType.DEL)
    @PostMapping("/del")
    public ResultData del(SysOperatorLog log){
        operatorLogService.del(log);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:operator:del')")
    @Log(moduleName = "操作日志", OperatorType = OperatorType.CLEAN)
    @PostMapping("/clean")
    public ResultData clean(){
        operatorLogService.clean();
        return new ResultData();
    }
}
