package mobai.moran.system.controller;

import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.exception.ServiceException;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysDept;
import mobai.moran.system.entity.SysUser;
import mobai.moran.system.security.util.SecurityUtil;
import mobai.moran.system.service.SysDeptService;
import mobai.moran.system.service.SysUserService;
import mobai.moran.system.util.ConfigUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * 用户管理
 * @author mobai
 */
@Controller
@RequestMapping("/system/user/")
public class SysUserController {

    @Resource
    SysUserService userService;
    @Resource
    SysDeptService deptService;

    @GetMapping("/index")
    public String index(){
        return "system/user/index";
    }
    @GetMapping("/form")
    public String form(){
        return "system/user/form";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:user:find')")
    @PostMapping("list")
    public PageData list(SysUser user){
        return userService.findPageList(user);
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:user:find')")
    @PostMapping("info/{id}")
    public ResultData info(@PathVariable(value = "id") Long id){
        Map<String,Object> user = userService.detail(id);
        if (!user.isEmpty()){
            Set<Long> roleIds = userService.getRoleIdsByUserId(id);
            user.put("roleIds",roleIds.toArray(roleIds.toArray(new Long[0])));
        }
        return new ResultData(user);
    }

    @ResponseBody
    @PostMapping("deptUser")
    public ResultData deptUser(){
        SysDept sd = new SysDept();
        sd.setHasUser("1");
        List<Map<String,Object>> deptList = deptService.findList(sd);
        List<Map<String,Object>> result = new ArrayList<>();
        for (Map<String,Object> dept: deptList){
            Map<String,Object> d = new HashMap<>();
            d.put("name",dept.get("deptName"));
            d.put("value","-" + dept.get("deptId"));
            if ("0".equals(dept.get("deptStatus").toString())){
                d.put("disabled",true);
            }
            d.put("children",userService.getUserByDeptIdForSelect(Long.parseLong(dept.get("deptId").toString())));
            result.add(d);
        }
        return new ResultData(result);
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:user:add')")
    @Log(moduleName = "用户管理", OperatorType = OperatorType.ADD)
    @PostMapping("add")
    public ResultData add(@Validated(ModelEntity.add.class) SysUser user){
        userService.add(user);
        return new ResultData();
    }

    @ResponseBody
    @Log(moduleName = "用户管理", OperatorType = OperatorType.EDIT)
    @PreAuthorize("@ss.hasPermit('system:user:edit')")
    @PostMapping("edit")
    public ResultData edit(@Validated(ModelEntity.edit.class) SysUser user){
        userService.edit(user);
        return new ResultData();
    }

    @ResponseBody
    @Log(moduleName = "用户管理", OperatorType = OperatorType.EDIT)
    @PreAuthorize("@ss.hasPermit('system:user:reset')")
    @PostMapping("resetPwd")
    public ResultData resetPwd(@RequestParam(value = "id") Long id) {
        SysUser user = userService.getById(id);
        if (user == null){
            throw new ServiceException("重置密码失败");
        }
        user.setPassword(SecurityUtil.encryptPassword(ConfigUtil.get("user_init_password")));
        user.setLastPwdUpdateTime(new Date());
        user.updateById();
        return new ResultData();
    }

    @ResponseBody
    @Log(moduleName = "用户管理", OperatorType = OperatorType.DEL)
    @PreAuthorize("@ss.hasPermit('system:user:del')")
    @PostMapping("del")
    public ResultData del(@Validated(ModelEntity.del.class) SysUser user){
        userService.del(user);
        return new ResultData();
    }
}
