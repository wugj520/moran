package mobai.moran.system.controller;

import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.common.util.TreeUtil;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysDict;
import mobai.moran.system.entity.SysDictType;
import mobai.moran.system.service.SysDictService;
import mobai.moran.system.service.SysDictTypeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 字典管理
 * @author mobai
 */
@Controller
@RequestMapping("/system/dict")
public class SysDictController {

    @Resource
    SysDictTypeService dictTypeService;

    @Resource
    SysDictService dictService;

    @GetMapping("/index")
    public String index(){
        return "system/dict/index";
    }

    @GetMapping("/type/form")
    public String typeForm(){
        return "system/dict/type_form";
    }

    @GetMapping("/data/index")
    public String dataIndex(){
        return "system/dict/data_index";
    }

    @GetMapping("/data/form")
    public String dataForm(){
        return "system/dict/data_form";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:find')")
    @PostMapping("/type/list")
    public PageData typeList(SysDictType dictType){
        return dictTypeService.findPageList(dictType);
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:find')")
    @PostMapping("/data/list")
    public ResultData dataList(SysDict dict){
        return new ResultData(dictService.findList(dict));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:find')")
    @PostMapping("/data/tree")
    public ResultData dataTree(SysDict dict){
        return new ResultData(TreeUtil.getChild(dictService.findList(dict),"dictId"));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:find')")
    @PostMapping("/type/info/{id}")
    public ResultData typeInfo(@PathVariable(value = "id") Long id){
        return new ResultData(dictTypeService.detail(id));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:find')")
    @PostMapping("/data/info/{id}")
    public ResultData dataInfo(@PathVariable(value = "id") Long id){
        return new ResultData(dictService.detail(id));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:add')")
    @Log(moduleName = "字典类型管理", OperatorType = OperatorType.ADD)
    @PostMapping("/type/add")
    public ResultData typeAdd(@Validated(ModelEntity.add.class) SysDictType dictType){
        dictTypeService.add(dictType);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:add')")
    @Log(moduleName = "字典管理", OperatorType = OperatorType.ADD)
    @PostMapping("/data/add")
    public ResultData dataAdd(@Validated(ModelEntity.add.class) SysDict dict){
        dictService.add(dict);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:edit')")
    @Log(moduleName = "字典类型管理", OperatorType = OperatorType.EDIT)
    @PostMapping("/type/edit")
    public ResultData typeEdit(@Validated(ModelEntity.edit.class) SysDictType dictType){
        dictTypeService.edit(dictType);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:edit')")
    @Log(moduleName = "字典管理", OperatorType = OperatorType.EDIT)
    @PostMapping("/data/edit")
    public ResultData dataEdit(@Validated(ModelEntity.edit.class) SysDict dict){
        dictService.edit(dict);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:del')")
    @Log(moduleName = "字典类型管理", OperatorType = OperatorType.DEL)
    @PostMapping("/type/del")
    public ResultData typedDel(@Validated(ModelEntity.del.class) SysDictType dictType){
        dictTypeService.del(dictType);
        return new ResultData();
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:dict:del')")
    @Log(moduleName = "字典管理", OperatorType = OperatorType.DEL)
    @PostMapping("/data/del")
    public ResultData dataDel(@Validated(ModelEntity.del.class) SysDict dict){
        dictService.del(dict);
        return new ResultData();
    }
}
