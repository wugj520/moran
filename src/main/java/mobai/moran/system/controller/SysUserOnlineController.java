package mobai.moran.system.controller;

import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.vo.SystemUserOnline;
import mobai.moran.system.security.util.SecurityUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * 在线用户监控
 * @author mobai
 */
@Controller
@RequestMapping("/monitor/online")
public class SysUserOnlineController {

    @PreAuthorize("@ss.hasPermit('monitor:online:find')")
    @GetMapping("/index")
    public String index() {
        return "system/monitor/online_user_index";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('monitor:online:find')")
    @PostMapping("/list")
    public PageData list(){
        List<SystemUserOnline> userOnlineList = SecurityUtil.onlineUser();
        Collections.reverse(userOnlineList);

        PageData page = new PageData();
        page.setCode(0);
        page.setCount(userOnlineList.size());
        page.setData(userOnlineList);
        page.setMsg("查询成功");
        return page;
    }

    /**
     * 强退用户
     */
    @ResponseBody
    @PreAuthorize("@ss.hasPermit('monitor:online:forceLogout')")
    @Log(moduleName = "在线用户", OperatorType = OperatorType.FORCE)
    @PostMapping("/forceLogout/{tokenId}")
    public ResultData forceLogout(@PathVariable(value = "tokenId") String tokenId) {
        SecurityUtil.loginOut(tokenId);
        return new ResultData();
    }
}
