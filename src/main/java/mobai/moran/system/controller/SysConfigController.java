package mobai.moran.system.controller;

import mobai.moran.common.base.ModelEntity;
import mobai.moran.common.base.vo.PageData;
import mobai.moran.common.base.vo.ResultData;
import mobai.moran.system.annotation.Log;
import mobai.moran.system.constant.OperatorType;
import mobai.moran.system.entity.SysConfig;
import mobai.moran.system.service.SysConfigService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 参数配置
 * @author mobai
 */
@Controller
@RequestMapping("/system/config")
public class SysConfigController {

    @Resource
    SysConfigService configService;

    @GetMapping("/index")
    public String index(){
        return "system/config/index";
    }
    @GetMapping("/form")
    public String form(){
        return "system/config/form";
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:config:find')")
    @PostMapping("/list")
    public PageData list(SysConfig config){
        return configService.findPageList(config);
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:config:find')")
    @PostMapping("/info/{id}")
    public ResultData info(@PathVariable(value = "id") Long id){
        return new ResultData(configService.detail(id));
    }

    @ResponseBody
    @PreAuthorize("@ss.hasPermit('system:config:add')")
    @Log(moduleName = "参数管理", OperatorType = OperatorType.ADD)
    @PostMapping("/add")
    public ResultData add(@Validated(ModelEntity.add.class) SysConfig config){
        configService.add(config);
        return new ResultData();
    }

    @ResponseBody
    @Log(moduleName = "参数管理", OperatorType = OperatorType.EDIT)
    @PreAuthorize("@ss.hasPermit('system:config:edit')")
    @PostMapping("/edit")
    public ResultData edit(@Validated(ModelEntity.edit.class) SysConfig config){
        configService.edit(config);
        return new ResultData();
    }

    @ResponseBody
    @Log(moduleName = "参数管理", OperatorType = OperatorType.DEL)
    @PreAuthorize("@ss.hasPermit('system:config:del')")
    @PostMapping("/del")
    public ResultData del(@Validated(ModelEntity.del.class) SysConfig config){
        configService.del(config);
        return new ResultData();
    }
}
