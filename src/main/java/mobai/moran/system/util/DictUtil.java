package mobai.moran.system.util;

import mobai.moran.common.util.ObjectUtil;
import mobai.moran.common.util.TreeUtil;

import java.util.*;

/**
 * 字典管理,方便客户端获取，直接把字典缓存在内存
 * @author mobai
 */
public class DictUtil {

    /** 分隔符 */
    public static final String SEPARATOR = ",";

    /** 字典列表 */
    private static final Map<String,List<Map<String,Object>>> DICT_LIST = new HashMap<>();
    /** 字典树列表 */
    private static final Map<String,List<Map<String,Object>>> DICT_TREE = new HashMap<>();

    public static Map<String,List<Map<String,Object>>> getDictList(){
        return DICT_LIST;
    }

    public static Map<String,List<Map<String,Object>>> getDictTree(){
        return DICT_TREE;
    }

    /**
     * 设置字典缓存
     * @param key          标识
     * @param dictList 字典数据列表
     */
    public static void set(String key, List<Map<String,Object>> dictList){
        DICT_LIST.put(key, dictList);
        DICT_TREE.put(key, TreeUtil.getChild(ObjectUtil.deepCopy(dictList),"dictId"));
    }

    /**
     * 获取字典缓存
     * @param key 标识
     * @return 字典数据列表
     */
    public static List<Map<String,Object>> get(String key){
        if (DICT_LIST.containsKey(key)){
            return DICT_LIST.get(key);
        }
        return new ArrayList<>();
    }

    /**
     * 移除指定字典缓存
     * @param key   标识
     */
    public static void remove(String key){
        DICT_LIST.remove(key);
        DICT_TREE.remove(key);
    }

    /** 清空字典缓存 */
    public static void clear() {
        DICT_LIST.clear();
        DICT_TREE.clear();
    }

    /**
     * 根据字典类型和字典值获取字典标签
     *
     * @param dictType  字典类型
     * @param dictValue 字典值
     * @return 字典标签
     */
    public static String getDictName(String dictType, String dictValue) {
        return getDictName(dictType, dictValue, SEPARATOR);
    }

    /**
     * 根据字典类型和字典值获取字典标签
     *
     * @param dictType  字典类型
     * @param dictValue 字典值
     * @param separator 多个值的分隔符
     * @return 字典标签
     */
    public static String getDictName(String dictType, String dictValue, String separator) {
        List<Map<String,Object>> dictList = get(dictType);
        if (ObjectUtil.isEmpty(dictList)) {
            return "";
        }

        StringBuilder nameStr = new StringBuilder();

        assert dictList != null;
        for (Map<String,Object> dict : dictList) {
            if (!ObjectUtil.contains(dictValue, separator)){
                if (dictValue.equals(dict.get("dictValue").toString())) {
                    nameStr.append(dict.get("dictName").toString());
                }
            }else{
                for (String value : dictValue.split(separator)) {
                    if (value.equals(dict.get("dictValue").toString())) {
                        if (nameStr.length() > 0){
                            nameStr.append(separator).append(dict.get("dictName").toString());
                            break;
                        }
                    }
                }
            }
        }
        return nameStr.toString();
    }

    /**
     * 根据字典类型和字典标签获取字典值
     *
     * @param dictType  字典类型
     * @param dictLabel 字典标签
     * @return 字典值
     */
    public static String getDictValue(String dictType, String dictLabel) {
        return getDictValue(dictType, dictLabel, SEPARATOR);
    }

    /**
     * 根据字典类型和字典标签获取字典值
     *
     * @param dictType  字典类型
     * @param dictLabel 字典标签
     * @param separator 分隔符
     * @return 字典值
     */
    public static String getDictValue(String dictType, String dictLabel, String separator) {
        List<Map<String, Object>> dictList = get(dictType);
        if (ObjectUtil.isEmpty(dictList)) {
            return "";
        }
        assert dictList != null;

        StringBuilder valueStr = new StringBuilder();

        for (Map<String, Object> dict : dictList) {
            if (!ObjectUtil.contains(dictLabel, separator)) {
                // 不包含分隔符，直接获取
                if (dictLabel.equals(dict.get("dictName").toString())) {
                    valueStr.append(dict.get("dictValue").toString());
                }
            }else{
                for (String label : dictLabel.split(separator)) {
                    if (label.equals(dict.get("dictName").toString())){
                        if (valueStr.length() > 0){
                            valueStr.append(separator).append(dict.get("dictValue").toString());
                        }
                        break;
                    }
                }
            }
        }
        return valueStr.toString();
    }
}
