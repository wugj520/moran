package mobai.moran.system.util;

import eu.bitwalker.useragentutils.UserAgent;
import mobai.moran.common.base.vo.LoginRequest;
import mobai.moran.common.constant.Constants;
import mobai.moran.common.util.ServletUtil;
import mobai.moran.system.entity.SysLoginLog;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author mobai
 */
public class LogUtil {

    public static SysLoginLog createLoginLog(LoginRequest loginRequest, String status, String message, Object... args){
        SysLoginLog login = new SysLoginLog();
        login.setLoginUserName(loginRequest.getUsername());
        login.setLoginPassword(loginRequest.getPassword());
        login.setLoginMsg(message);
        login.setLoginStatus(status);
        return setReq(login);
    }

    public static SysLoginLog createLoginLog(String username, String status, String message, Object... args){
        SysLoginLog login = new SysLoginLog();
        login.setLoginUserName(username);
        login.setLoginMsg(message);
        login.setLoginStatus(status);
        return setReq(login);
    }

    public static SysLoginLog setReq(SysLoginLog login){
        HttpServletRequest request = ServletUtil.getRequest();
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        login.setLoginIp(ServletUtil.getRequestIp(request));
        login.setLoginLocation(ServletUtil.getRequestLocation(request));
        login.setLoginBrowser(userAgent.getBrowser().getName());
        login.setLoginOs(userAgent.getOperatingSystem().getName());
        login.setLoginTime(new Date());
        login.setDeleted(Constants.NO_DELETE);
        return login;
    }


}
