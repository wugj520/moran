package mobai.moran.system.util;

import mobai.moran.common.cache.memory.CacheUtil;

/**
 * @author mobai
 */
public class ConfigUtil {

    /** 参数管理 cache key */
    public static final String SYS_CONFIG_KEY = "SYS_CONFIG:";

    /**
     * 设置参数缓存
     * @param key   标识
     * @param val   参数值
     */
    public static void set(String key, String val){
        CacheUtil.put(getCacheKey(key), val);
    }

    /**
     * 获取参数值
     * @param key 标识
     * @return    参数值
     */
    public static String get(String key){
        Object val = CacheUtil.get(getCacheKey(key));
        return val == null ? "" : val.toString();
    }

    /**
     * 判断值是否存在
     * @param key 标识
     */
    public static boolean containsKey(String key){
        return CacheUtil.get(getCacheKey(key)) != null;
    }

    /**
     * 移除指定参数缓存
     * @param key   标识
     */
    public static void remove(String key){
        CacheUtil.remove(getCacheKey(key));
    }

    /** 清空字典缓存 */
    public static void clear() {
        CacheUtil.removeByPrefix(SYS_CONFIG_KEY + "*");
    }

    /**
     * 设置cache key
     * @param key 参数键
     * @return 缓存键key
     */
    public static String getCacheKey(String key) {
        return SYS_CONFIG_KEY + key;
    }
}
