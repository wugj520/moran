package mobai.moran.system.constant;

/**
 * 操作类型
 * @author mobai
 */
public class OperatorType {

    /** 其他 */
    public static final String OTHER = "0";
    /** 新增 */
    public static final String ADD = "1";
    /** 删除 */
    public static final String DEL = "2";
    /** 编辑 */
    public static final String EDIT = "3";
    /** 查询 */
    public static final String FIND = "4";
    /** 导出 */
    public static final String EXPORT = "5";
    /** 导入 */
    public static final String IMPORT = "6";
    /** 清空 */
    public static final String CLEAN = "7";
    /** 强退 */
    public static final String FORCE = "8";
}
